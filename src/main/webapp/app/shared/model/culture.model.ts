import { IPlantation } from '@/shared/model/plantation.model';
import { IPlante } from '@/shared/model/plante.model';

export interface ICulture {
  id?: number;
  nom?: string | null;
  dateDebut?: Date | null;
  dateFin?: Date | null;
  dateRecolte?: Date | null;
  plantation?: IPlantation | null;
  plante?: IPlante | null;
}

export class Culture implements ICulture {
  constructor(
    public id?: number,
    public nom?: string | null,
    public dateDebut?: Date | null,
    public dateFin?: Date | null,
    public dateRecolte?: Date | null,
    public plantation?: IPlantation | null,
    public plante?: IPlante | null
  ) {}
}
