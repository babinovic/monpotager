/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import CultureUpdateComponent from '@/entities/culture/culture-update.vue';
import CultureClass from '@/entities/culture/culture-update.component';
import CultureService from '@/entities/culture/culture.service';

import PlantationService from '@/entities/plantation/plantation.service';

import PlanteService from '@/entities/plante/plante.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Culture Management Update Component', () => {
    let wrapper: Wrapper<CultureClass>;
    let comp: CultureClass;
    let cultureServiceStub: SinonStubbedInstance<CultureService>;

    beforeEach(() => {
      cultureServiceStub = sinon.createStubInstance<CultureService>(CultureService);

      wrapper = shallowMount<CultureClass>(CultureUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          cultureService: () => cultureServiceStub,
          alertService: () => new AlertService(),

          plantationService: () =>
            sinon.createStubInstance<PlantationService>(PlantationService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          planteService: () =>
            sinon.createStubInstance<PlanteService>(PlanteService, {
              retrieve: sinon.stub().resolves({}),
            } as any),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.culture = entity;
        cultureServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(cultureServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.culture = entity;
        cultureServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(cultureServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundCulture = { id: 123 };
        cultureServiceStub.find.resolves(foundCulture);
        cultureServiceStub.retrieve.resolves([foundCulture]);

        // WHEN
        comp.beforeRouteEnter({ params: { cultureId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.culture).toBe(foundCulture);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
