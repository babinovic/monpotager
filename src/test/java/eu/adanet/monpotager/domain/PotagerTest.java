package eu.adanet.monpotager.domain;

import static org.assertj.core.api.Assertions.assertThat;

import eu.adanet.monpotager.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PotagerTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Potager.class);
        Potager potager1 = new Potager();
        potager1.setId(1L);
        Potager potager2 = new Potager();
        potager2.setId(potager1.getId());
        assertThat(potager1).isEqualTo(potager2);
        potager2.setId(2L);
        assertThat(potager1).isNotEqualTo(potager2);
        potager1.setId(null);
        assertThat(potager1).isNotEqualTo(potager2);
    }
}
