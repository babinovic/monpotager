package eu.adanet.monpotager.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import eu.adanet.monpotager.IntegrationTest;
import eu.adanet.monpotager.domain.Parcelle;
import eu.adanet.monpotager.domain.enumeration.exposition;
import eu.adanet.monpotager.repository.ParcelleRepository;
import eu.adanet.monpotager.service.ParcelleService;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ParcelleResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class ParcelleResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final Float DEFAULT_LONGUEUR = 1F;
    private static final Float UPDATED_LONGUEUR = 2F;

    private static final Float DEFAULT_LARGEUR = 1F;
    private static final Float UPDATED_LARGEUR = 2F;

    private static final exposition DEFAULT_ENSOLEILLEMENT = exposition.OMBRE;
    private static final exposition UPDATED_ENSOLEILLEMENT = exposition.MI_OMBRE;

    private static final Integer DEFAULT_NB_PLANTATION_LONGUEUR = 1;
    private static final Integer UPDATED_NB_PLANTATION_LONGUEUR = 2;

    private static final Integer DEFAULT_NB_PLANTATION_LARGEUR = 1;
    private static final Integer UPDATED_NB_PLANTATION_LARGEUR = 2;

    private static final String ENTITY_API_URL = "/api/parcelles";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ParcelleRepository parcelleRepository;

    @Mock
    private ParcelleRepository parcelleRepositoryMock;

    @Mock
    private ParcelleService parcelleServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restParcelleMockMvc;

    private Parcelle parcelle;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Parcelle createEntity(EntityManager em) {
        Parcelle parcelle = new Parcelle()
            .nom(DEFAULT_NOM)
            .longueur(DEFAULT_LONGUEUR)
            .largeur(DEFAULT_LARGEUR)
            .ensoleillement(DEFAULT_ENSOLEILLEMENT)
            .nbPlantationLongueur(DEFAULT_NB_PLANTATION_LONGUEUR)
            .nbPlantationLargeur(DEFAULT_NB_PLANTATION_LARGEUR);
        return parcelle;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Parcelle createUpdatedEntity(EntityManager em) {
        Parcelle parcelle = new Parcelle()
            .nom(UPDATED_NOM)
            .longueur(UPDATED_LONGUEUR)
            .largeur(UPDATED_LARGEUR)
            .ensoleillement(UPDATED_ENSOLEILLEMENT)
            .nbPlantationLongueur(UPDATED_NB_PLANTATION_LONGUEUR)
            .nbPlantationLargeur(UPDATED_NB_PLANTATION_LARGEUR);
        return parcelle;
    }

    @BeforeEach
    public void initTest() {
        parcelle = createEntity(em);
    }

    @Test
    @Transactional
    void createParcelle() throws Exception {
        int databaseSizeBeforeCreate = parcelleRepository.findAll().size();
        // Create the Parcelle
        restParcelleMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(parcelle)))
            .andExpect(status().isCreated());

        // Validate the Parcelle in the database
        List<Parcelle> parcelleList = parcelleRepository.findAll();
        assertThat(parcelleList).hasSize(databaseSizeBeforeCreate + 1);
        Parcelle testParcelle = parcelleList.get(parcelleList.size() - 1);
        assertThat(testParcelle.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testParcelle.getLongueur()).isEqualTo(DEFAULT_LONGUEUR);
        assertThat(testParcelle.getLargeur()).isEqualTo(DEFAULT_LARGEUR);
        assertThat(testParcelle.getEnsoleillement()).isEqualTo(DEFAULT_ENSOLEILLEMENT);
        assertThat(testParcelle.getNbPlantationLongueur()).isEqualTo(DEFAULT_NB_PLANTATION_LONGUEUR);
        assertThat(testParcelle.getNbPlantationLargeur()).isEqualTo(DEFAULT_NB_PLANTATION_LARGEUR);
    }

    @Test
    @Transactional
    void createParcelleWithExistingId() throws Exception {
        // Create the Parcelle with an existing ID
        parcelle.setId(1L);

        int databaseSizeBeforeCreate = parcelleRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restParcelleMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(parcelle)))
            .andExpect(status().isBadRequest());

        // Validate the Parcelle in the database
        List<Parcelle> parcelleList = parcelleRepository.findAll();
        assertThat(parcelleList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllParcelles() throws Exception {
        // Initialize the database
        parcelleRepository.saveAndFlush(parcelle);

        // Get all the parcelleList
        restParcelleMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(parcelle.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].longueur").value(hasItem(DEFAULT_LONGUEUR.doubleValue())))
            .andExpect(jsonPath("$.[*].largeur").value(hasItem(DEFAULT_LARGEUR.doubleValue())))
            .andExpect(jsonPath("$.[*].ensoleillement").value(hasItem(DEFAULT_ENSOLEILLEMENT.toString())))
            .andExpect(jsonPath("$.[*].nbPlantationLongueur").value(hasItem(DEFAULT_NB_PLANTATION_LONGUEUR)))
            .andExpect(jsonPath("$.[*].nbPlantationLargeur").value(hasItem(DEFAULT_NB_PLANTATION_LARGEUR)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllParcellesWithEagerRelationshipsIsEnabled() throws Exception {
        when(parcelleServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restParcelleMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(parcelleServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllParcellesWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(parcelleServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restParcelleMockMvc.perform(get(ENTITY_API_URL + "?eagerload=false")).andExpect(status().isOk());
        verify(parcelleRepositoryMock, times(1)).findAll(any(Pageable.class));
    }

    @Test
    @Transactional
    void getParcelle() throws Exception {
        // Initialize the database
        parcelleRepository.saveAndFlush(parcelle);

        // Get the parcelle
        restParcelleMockMvc
            .perform(get(ENTITY_API_URL_ID, parcelle.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(parcelle.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.longueur").value(DEFAULT_LONGUEUR.doubleValue()))
            .andExpect(jsonPath("$.largeur").value(DEFAULT_LARGEUR.doubleValue()))
            .andExpect(jsonPath("$.ensoleillement").value(DEFAULT_ENSOLEILLEMENT.toString()))
            .andExpect(jsonPath("$.nbPlantationLongueur").value(DEFAULT_NB_PLANTATION_LONGUEUR))
            .andExpect(jsonPath("$.nbPlantationLargeur").value(DEFAULT_NB_PLANTATION_LARGEUR));
    }

    @Test
    @Transactional
    void getNonExistingParcelle() throws Exception {
        // Get the parcelle
        restParcelleMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingParcelle() throws Exception {
        // Initialize the database
        parcelleRepository.saveAndFlush(parcelle);

        int databaseSizeBeforeUpdate = parcelleRepository.findAll().size();

        // Update the parcelle
        Parcelle updatedParcelle = parcelleRepository.findById(parcelle.getId()).get();
        // Disconnect from session so that the updates on updatedParcelle are not directly saved in db
        em.detach(updatedParcelle);
        updatedParcelle
            .nom(UPDATED_NOM)
            .longueur(UPDATED_LONGUEUR)
            .largeur(UPDATED_LARGEUR)
            .ensoleillement(UPDATED_ENSOLEILLEMENT)
            .nbPlantationLongueur(UPDATED_NB_PLANTATION_LONGUEUR)
            .nbPlantationLargeur(UPDATED_NB_PLANTATION_LARGEUR);

        restParcelleMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedParcelle.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedParcelle))
            )
            .andExpect(status().isOk());

        // Validate the Parcelle in the database
        List<Parcelle> parcelleList = parcelleRepository.findAll();
        assertThat(parcelleList).hasSize(databaseSizeBeforeUpdate);
        Parcelle testParcelle = parcelleList.get(parcelleList.size() - 1);
        assertThat(testParcelle.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testParcelle.getLongueur()).isEqualTo(UPDATED_LONGUEUR);
        assertThat(testParcelle.getLargeur()).isEqualTo(UPDATED_LARGEUR);
        assertThat(testParcelle.getEnsoleillement()).isEqualTo(UPDATED_ENSOLEILLEMENT);
        assertThat(testParcelle.getNbPlantationLongueur()).isEqualTo(UPDATED_NB_PLANTATION_LONGUEUR);
        assertThat(testParcelle.getNbPlantationLargeur()).isEqualTo(UPDATED_NB_PLANTATION_LARGEUR);
    }

    @Test
    @Transactional
    void putNonExistingParcelle() throws Exception {
        int databaseSizeBeforeUpdate = parcelleRepository.findAll().size();
        parcelle.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restParcelleMockMvc
            .perform(
                put(ENTITY_API_URL_ID, parcelle.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(parcelle))
            )
            .andExpect(status().isBadRequest());

        // Validate the Parcelle in the database
        List<Parcelle> parcelleList = parcelleRepository.findAll();
        assertThat(parcelleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchParcelle() throws Exception {
        int databaseSizeBeforeUpdate = parcelleRepository.findAll().size();
        parcelle.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restParcelleMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(parcelle))
            )
            .andExpect(status().isBadRequest());

        // Validate the Parcelle in the database
        List<Parcelle> parcelleList = parcelleRepository.findAll();
        assertThat(parcelleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamParcelle() throws Exception {
        int databaseSizeBeforeUpdate = parcelleRepository.findAll().size();
        parcelle.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restParcelleMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(parcelle)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Parcelle in the database
        List<Parcelle> parcelleList = parcelleRepository.findAll();
        assertThat(parcelleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateParcelleWithPatch() throws Exception {
        // Initialize the database
        parcelleRepository.saveAndFlush(parcelle);

        int databaseSizeBeforeUpdate = parcelleRepository.findAll().size();

        // Update the parcelle using partial update
        Parcelle partialUpdatedParcelle = new Parcelle();
        partialUpdatedParcelle.setId(parcelle.getId());

        partialUpdatedParcelle.longueur(UPDATED_LONGUEUR).largeur(UPDATED_LARGEUR).nbPlantationLongueur(UPDATED_NB_PLANTATION_LONGUEUR);

        restParcelleMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedParcelle.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedParcelle))
            )
            .andExpect(status().isOk());

        // Validate the Parcelle in the database
        List<Parcelle> parcelleList = parcelleRepository.findAll();
        assertThat(parcelleList).hasSize(databaseSizeBeforeUpdate);
        Parcelle testParcelle = parcelleList.get(parcelleList.size() - 1);
        assertThat(testParcelle.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testParcelle.getLongueur()).isEqualTo(UPDATED_LONGUEUR);
        assertThat(testParcelle.getLargeur()).isEqualTo(UPDATED_LARGEUR);
        assertThat(testParcelle.getEnsoleillement()).isEqualTo(DEFAULT_ENSOLEILLEMENT);
        assertThat(testParcelle.getNbPlantationLongueur()).isEqualTo(UPDATED_NB_PLANTATION_LONGUEUR);
        assertThat(testParcelle.getNbPlantationLargeur()).isEqualTo(DEFAULT_NB_PLANTATION_LARGEUR);
    }

    @Test
    @Transactional
    void fullUpdateParcelleWithPatch() throws Exception {
        // Initialize the database
        parcelleRepository.saveAndFlush(parcelle);

        int databaseSizeBeforeUpdate = parcelleRepository.findAll().size();

        // Update the parcelle using partial update
        Parcelle partialUpdatedParcelle = new Parcelle();
        partialUpdatedParcelle.setId(parcelle.getId());

        partialUpdatedParcelle
            .nom(UPDATED_NOM)
            .longueur(UPDATED_LONGUEUR)
            .largeur(UPDATED_LARGEUR)
            .ensoleillement(UPDATED_ENSOLEILLEMENT)
            .nbPlantationLongueur(UPDATED_NB_PLANTATION_LONGUEUR)
            .nbPlantationLargeur(UPDATED_NB_PLANTATION_LARGEUR);

        restParcelleMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedParcelle.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedParcelle))
            )
            .andExpect(status().isOk());

        // Validate the Parcelle in the database
        List<Parcelle> parcelleList = parcelleRepository.findAll();
        assertThat(parcelleList).hasSize(databaseSizeBeforeUpdate);
        Parcelle testParcelle = parcelleList.get(parcelleList.size() - 1);
        assertThat(testParcelle.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testParcelle.getLongueur()).isEqualTo(UPDATED_LONGUEUR);
        assertThat(testParcelle.getLargeur()).isEqualTo(UPDATED_LARGEUR);
        assertThat(testParcelle.getEnsoleillement()).isEqualTo(UPDATED_ENSOLEILLEMENT);
        assertThat(testParcelle.getNbPlantationLongueur()).isEqualTo(UPDATED_NB_PLANTATION_LONGUEUR);
        assertThat(testParcelle.getNbPlantationLargeur()).isEqualTo(UPDATED_NB_PLANTATION_LARGEUR);
    }

    @Test
    @Transactional
    void patchNonExistingParcelle() throws Exception {
        int databaseSizeBeforeUpdate = parcelleRepository.findAll().size();
        parcelle.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restParcelleMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, parcelle.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(parcelle))
            )
            .andExpect(status().isBadRequest());

        // Validate the Parcelle in the database
        List<Parcelle> parcelleList = parcelleRepository.findAll();
        assertThat(parcelleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchParcelle() throws Exception {
        int databaseSizeBeforeUpdate = parcelleRepository.findAll().size();
        parcelle.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restParcelleMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(parcelle))
            )
            .andExpect(status().isBadRequest());

        // Validate the Parcelle in the database
        List<Parcelle> parcelleList = parcelleRepository.findAll();
        assertThat(parcelleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamParcelle() throws Exception {
        int databaseSizeBeforeUpdate = parcelleRepository.findAll().size();
        parcelle.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restParcelleMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(parcelle)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Parcelle in the database
        List<Parcelle> parcelleList = parcelleRepository.findAll();
        assertThat(parcelleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteParcelle() throws Exception {
        // Initialize the database
        parcelleRepository.saveAndFlush(parcelle);

        int databaseSizeBeforeDelete = parcelleRepository.findAll().size();

        // Delete the parcelle
        restParcelleMockMvc
            .perform(delete(ENTITY_API_URL_ID, parcelle.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Parcelle> parcelleList = parcelleRepository.findAll();
        assertThat(parcelleList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
