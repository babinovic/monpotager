import { Component, Vue, Inject } from 'vue-property-decorator';

import { ITache } from '@/shared/model/tache.model';
import TacheService from './tache.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class TacheDetails extends Vue {
  @Inject('tacheService') private tacheService: () => TacheService;
  @Inject('alertService') private alertService: () => AlertService;

  public tache: ITache = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.tacheId) {
        vm.retrieveTache(to.params.tacheId);
      }
    });
  }

  public retrieveTache(tacheId) {
    this.tacheService()
      .find(tacheId)
      .then(res => {
        this.tache = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
