package eu.adanet.monpotager.service;

import eu.adanet.monpotager.domain.Culture;
import eu.adanet.monpotager.domain.Plantation;
import eu.adanet.monpotager.repository.CultureRepository;
import eu.adanet.monpotager.repository.PlantationRepository;
import eu.adanet.monpotager.service.dto.PlantationCultureDTO;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Plantation}.
 */
@Service
@Transactional
public class PlantationService {

    private final Logger log = LoggerFactory.getLogger(PlantationService.class);

    private final PlantationRepository plantationRepository;
    private final CultureRepository cultureRepository;
    private final CultureService cultureService;

    public PlantationService(PlantationRepository plantationRepository, CultureService cultureService, CultureRepository cultureRepository) {
        this.plantationRepository = plantationRepository;
        this.cultureService = cultureService;
        this.cultureRepository = cultureRepository;
    }

    /**
     * Save a plantation.
     *
     * @param plantation the entity to save.
     * @return the persisted entity.
     */
    public Plantation save(Plantation plantation) {
        log.debug("Request to save Plantation : {}", plantation);
        return plantationRepository.save(plantation);
    }

    /**
     * Update a plantation.
     *
     * @param plantation the entity to save.
     * @return the persisted entity.
     */
    public Plantation update(Plantation plantation) {
        log.debug("Request to update Plantation : {}", plantation);
        return plantationRepository.save(plantation);
    }

    /**
     * Partially update a plantation.
     *
     * @param plantation the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Plantation> partialUpdate(Plantation plantation) {
        log.debug("Request to partially update Plantation : {}", plantation);

        return plantationRepository
            .findById(plantation.getId())
            .map(existingPlantation -> {
                if (plantation.getIndexParcelle() != null) {
                    existingPlantation.setIndexParcelle(plantation.getIndexParcelle());
                }

                return existingPlantation;
            })
            .map(plantationRepository::save);
    }

    /**
     * Get all the plantations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Plantation> findAll(Pageable pageable) {
        log.debug("Request to get all Plantations");
        return plantationRepository.findAll(pageable);
    }

    /**
     * Get all the plantations with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<Plantation> findAllWithEagerRelationships(Pageable pageable) {
        return plantationRepository.findAllWithEagerRelationships(pageable);
    }

    /**
     * Get one plantation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Plantation> findOne(Long id) {
        log.debug("Request to get Plantation : {}", id);
        return plantationRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the plantation by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Plantation : {}", id);
        // cascade delete culture
        for (Culture culture : cultureRepository.findAll()) {
            if (culture.getPlantation().getId().longValue() == id) {
                cultureRepository.delete(culture);
            }           
        }
        plantationRepository.deleteById(id);
    }

    /**
     * Renvoie les plantations avec les culures à une date donnée
     *
     * @param parcelleId ID de la parcelle
     * @param dateVisualisation dateVisualisation
     * @return the list of entities.
     */
    public List<PlantationCultureDTO> getAllPlantationsParcelle(Long parcelleId, Date dateVisualisation) {
        List<PlantationCultureDTO> list = new ArrayList<PlantationCultureDTO>();
        log.debug("Request to get all Plantations");
        List<Plantation> plantations = plantationRepository.findAll();
        for (Plantation plantation : plantations) {
            if (plantation.getParcelle().getId().longValue() == parcelleId) {
                list.add(new PlantationCultureDTO(plantation, cultureService.getCultureActive(plantation, dateVisualisation)));
            }
        }
        return list;
    }
}
