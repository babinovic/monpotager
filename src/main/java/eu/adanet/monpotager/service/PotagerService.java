package eu.adanet.monpotager.service;

import eu.adanet.monpotager.domain.Parcelle;
import eu.adanet.monpotager.domain.Potager;
import eu.adanet.monpotager.domain.Tache;
import eu.adanet.monpotager.repository.ParcelleRepository;
import eu.adanet.monpotager.repository.PotagerRepository;
import eu.adanet.monpotager.repository.TacheRepository;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Potager}.
 */
@Service
@Transactional
public class PotagerService {

    private final Logger log = LoggerFactory.getLogger(PotagerService.class);

    private final PotagerRepository potagerRepository;
    private final ParcelleRepository parcelleRepository;
    private final TacheRepository tacheRepository;

    public PotagerService(PotagerRepository potagerRepository, ParcelleRepository parcelleRepository, TacheRepository tacheRepository) {
        this.potagerRepository = potagerRepository;
        this.parcelleRepository = parcelleRepository;
        this.tacheRepository = tacheRepository;
    }

    /**
     * Save a potager.
     *
     * @param potager the entity to save.
     * @return the persisted entity.
     */
    public Potager save(Potager potager) {
        log.debug("Request to save Potager : {}", potager);
        return potagerRepository.save(potager);
    }

    /**
     * Update a potager.
     *
     * @param potager the entity to save.
     * @return the persisted entity.
     */
    public Potager update(Potager potager) {
        log.debug("Request to update Potager : {}", potager);
        return potagerRepository.save(potager);
    }

    /**
     * Partially update a potager.
     *
     * @param potager the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Potager> partialUpdate(Potager potager) {
        log.debug("Request to partially update Potager : {}", potager);

        return potagerRepository
            .findById(potager.getId())
            .map(existingPotager -> {
                if (potager.getNom() != null) {
                    existingPotager.setNom(potager.getNom());
                }
                if (potager.getLongitude() != null) {
                    existingPotager.setLongitude(potager.getLongitude());
                }
                if (potager.getLatitude() != null) {
                    existingPotager.setLatitude(potager.getLatitude());
                }

                return existingPotager;
            })
            .map(potagerRepository::save);
    }

    /**
     * Get all the potagers.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Potager> findAll() {
        log.debug("Request to get all Potagers");
        return potagerRepository.findAll();
    }

    /**
     * Get one potager by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Potager> findOne(Long id) {
        log.debug("Request to get Potager : {}", id);
        return potagerRepository.findById(id);
    }

    /**
     * Delete the potager by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Potager : {}", id);
        // cascade delete parcelles
        for (Parcelle parcelle : parcelleRepository.findAll()) {
            if (parcelle.getPotager().getId().longValue() == id) {
                parcelleRepository.delete(parcelle);
            }         
        }
        // cascade delete taches
        for (Tache tache : tacheRepository.findAll()) {
            if (tache.getPotager().getId().longValue() == id) {
                tacheRepository.delete(tache);
            }     
        }
        potagerRepository.deleteById(id);
    }
}
