package eu.adanet.monpotager.service;

import eu.adanet.monpotager.domain.Culture;
import eu.adanet.monpotager.domain.Plante;
import eu.adanet.monpotager.repository.CultureRepository;
import eu.adanet.monpotager.repository.PlanteRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Plante}.
 */
@Service
@Transactional
public class PlanteService {

    private final Logger log = LoggerFactory.getLogger(PlanteService.class);

    private final PlanteRepository planteRepository;
    private final CultureRepository cultureRepository;

    public PlanteService(PlanteRepository planteRepository, CultureRepository cultureRepository) {
        this.planteRepository = planteRepository;
        this.cultureRepository = cultureRepository;
    }

    /**
     * Save a plante.
     *
     * @param plante the entity to save.
     * @return the persisted entity.
     */
    public Plante save(Plante plante) {
        log.debug("Request to save Plante : {}", plante);
        return planteRepository.save(plante);
    }

    /**
     * Update a plante.
     *
     * @param plante the entity to save.
     * @return the persisted entity.
     */
    public Plante update(Plante plante) {
        log.debug("Request to update Plante : {}", plante);
        return planteRepository.save(plante);
    }

    /**
     * Partially update a plante.
     *
     * @param plante the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Plante> partialUpdate(Plante plante) {
        log.debug("Request to partially update Plante : {}", plante);

        return planteRepository
            .findById(plante.getId())
            .map(existingPlante -> {
                if (plante.getNom() != null) {
                    existingPlante.setNom(plante.getNom());
                }
                if (plante.getVariete() != null) {
                    existingPlante.setVariete(plante.getVariete());
                }
                if (plante.getType() != null) {
                    existingPlante.setType(plante.getType());
                }
                if (plante.getEspacement() != null) {
                    existingPlante.setEspacement(plante.getEspacement());
                }
                if (plante.getEspacementRang() != null) {
                    existingPlante.setEspacementRang(plante.getEspacementRang());
                }
                if (plante.getEnsoleillement() != null) {
                    existingPlante.setEnsoleillement(plante.getEnsoleillement());
                }
                if (plante.getArrosage() != null) {
                    existingPlante.setArrosage(plante.getArrosage());
                }
                if (plante.getDebutPlantation() != null) {
                    existingPlante.setDebutPlantation(plante.getDebutPlantation());
                }
                if (plante.getFinPlantation() != null) {
                    existingPlante.setFinPlantation(plante.getFinPlantation());
                }
                if (plante.getDebutRecolte() != null) {
                    existingPlante.setDebutRecolte(plante.getDebutRecolte());
                }
                if (plante.getFinRecolte() != null) {
                    existingPlante.setFinRecolte(plante.getFinRecolte());
                }
                if (plante.getDebutSemis() != null) {
                    existingPlante.setDebutSemis(plante.getDebutSemis());
                }
                if (plante.getFinSemis() != null) {
                    existingPlante.setFinSemis(plante.getFinSemis());
                }
                if (plante.getCategorie() != null) {
                    existingPlante.setCategorie(plante.getCategorie());
                }
                if (plante.getImage() != null) {
                    existingPlante.setImage(plante.getImage());
                }

                return existingPlante;
            })
            .map(planteRepository::save);
    }

    /**
     * Get all the plantes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Plante> findAll(Pageable pageable) {
        log.debug("Request to get all Plantes");
        return planteRepository.findAll(pageable);
    }

    /**
     * Get one plante by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Plante> findOne(Long id) {
        log.debug("Request to get Plante : {}", id);
        return planteRepository.findById(id);
    }

    /**
     * Delete the plante by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Plante : {}", id);
        // cascade delete cultures
        for (Culture culture : cultureRepository.findAll()) {
            if (culture.getPlante().getId().longValue() == id) {
                cultureRepository.delete(culture);
            }        
        }
        planteRepository.deleteById(id);
    }
}
