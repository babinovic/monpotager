package eu.adanet.monpotager.TacheAuto;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import eu.adanet.monpotager.domain.Tache;
import eu.adanet.monpotager.service.TacheService;

@Service
public class TacheAutoProcess {
    private final Logger log = LoggerFactory.getLogger(TacheAutoProcess.class);
    @Autowired
    private TacheService tacheService;

    @Async
    public void conseilsAsync(List<Tache> taches)  {
        try {
            // on crée les taches des conseils
            log.debug("execution conseilsAsync=" + taches);
            for (Tache tache : taches) {
                Tache tachecreee = tacheService.save(tache);
                log.debug("Tache créée=" + tachecreee);      
            }
            log.debug("finished");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
