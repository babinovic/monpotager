package eu.adanet.monpotager.service.dto;

import eu.adanet.monpotager.domain.Culture;
import eu.adanet.monpotager.domain.Plantation;
import eu.adanet.monpotager.domain.Plante;

import java.time.LocalDate;

/**
 * A DTO representing a user, with his authorities.
 */
public class PlantationCultureDTO extends Plantation {

    private Long cultureId;
    private String nomCulture;
    private LocalDate dateDebut;
    private LocalDate dateFin;
    private LocalDate dateRecolte;
    private Plante plante;
    private Plantation plantation;

    public PlantationCultureDTO(Plantation plantation, Culture culture) {
        this.id(plantation.getId());
        this.dateDebut = culture.getDateDebut();
        this.dateFin = culture.getDateFin();
        this.dateRecolte = culture.getDateRecolte();
        this.indexParcelle(plantation.getIndexParcelle());
        this.parcelle(plantation.getParcelle());
        this.cultureId = culture.getId();
        this.nomCulture = culture.getNom();
        this.plante = culture.getPlante();
        this.plantation = plantation;
    }

    public Long getCultureId() {
        return this.cultureId;
    }

    public void setCultureId(Long cultureId) {
        this.cultureId = cultureId;
    }

    public String getNomCulture() {
        return this.nomCulture;
    }

    public void setNomCulture(String nomCulture) {
        this.nomCulture = nomCulture;
    }

    public Plante getPlante() {
        return this.plante;
    }

    public void setPlante(Plante plante) {
        this.plante = plante;
    }

    public LocalDate getDateDebut() {
        return this.dateDebut;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return this.dateFin;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public LocalDate getDateRecolte() {
        return this.dateRecolte;
    }

    public void setDateRecolte(LocalDate dateRecolte) {
        this.dateRecolte = dateRecolte;
    }

    public Plantation getPlantation() {
        return this.plantation;
    }

    public void setPlantation(Plantation plantation) {
        this.plantation = plantation;
    }
}
