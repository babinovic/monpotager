/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import CultureDetailComponent from '@/entities/culture/culture-details.vue';
import CultureClass from '@/entities/culture/culture-details.component';
import CultureService from '@/entities/culture/culture.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Culture Management Detail Component', () => {
    let wrapper: Wrapper<CultureClass>;
    let comp: CultureClass;
    let cultureServiceStub: SinonStubbedInstance<CultureService>;

    beforeEach(() => {
      cultureServiceStub = sinon.createStubInstance<CultureService>(CultureService);

      wrapper = shallowMount<CultureClass>(CultureDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { cultureService: () => cultureServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundCulture = { id: 123 };
        cultureServiceStub.find.resolves(foundCulture);

        // WHEN
        comp.retrieveCulture(123);
        await comp.$nextTick();

        // THEN
        expect(comp.culture).toBe(foundCulture);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundCulture = { id: 123 };
        cultureServiceStub.find.resolves(foundCulture);

        // WHEN
        comp.beforeRouteEnter({ params: { cultureId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.culture).toBe(foundCulture);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
