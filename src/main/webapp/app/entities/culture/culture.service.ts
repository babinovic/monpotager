import axios from 'axios';

import buildPaginationQueryOpts from '@/shared/sort/sorts';

import { ICulture } from '@/shared/model/culture.model';

const baseApiUrl = 'api/cultures';
const arracherApiUrl = 'api/cultures/arracher';
const recolterApiUrl = 'api/cultures/recolter';
const planterApiUrl = 'api/cultures/planter'

export default class CultureService {

  public find(id: number): Promise<ICulture> {
    return new Promise<ICulture>((resolve, reject) => {
      axios
        .get(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public retrieve(paginationQuery?: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(baseApiUrl + `?${buildPaginationQueryOpts(paginationQuery)}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public delete(id: number): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .delete(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public create(entity: ICulture): Promise<ICulture> {
    return new Promise<ICulture>((resolve, reject) => {
      axios
        .post(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public update(entity: ICulture): Promise<ICulture> {
    return new Promise<ICulture>((resolve, reject) => {
      axios
        .put(`${baseApiUrl}/${entity.id}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public partialUpdate(entity: ICulture): Promise<ICulture> {
    return new Promise<ICulture>((resolve, reject) => {
      axios
        .patch(`${baseApiUrl}/${entity.id}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public arracher(cultureId: number): Promise<ICulture> {
    return new Promise<ICulture>((resolve, reject) => {
      axios
        .get(`${arracherApiUrl}/${cultureId}`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public recolter(cultureId: number): Promise<ICulture> {
    return new Promise<ICulture>((resolve, reject) => {
      axios
        .get(`${recolterApiUrl}/${cultureId}`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public planter(plantationId: number, planteId: number): Promise<ICulture> {
    return new Promise<ICulture>((resolve, reject) => {
      axios
        .get(planterApiUrl, {params: {plantationId: plantationId, planteId: planteId}})
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}
