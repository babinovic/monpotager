package eu.adanet.monpotager.repository;

import eu.adanet.monpotager.domain.Conseil;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Conseil entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConseilRepository extends JpaRepository<Conseil, Long>, JpaSpecificationExecutor<Conseil> {}
