package eu.adanet.monpotager.web.rest;

import eu.adanet.monpotager.domain.Potager;
import eu.adanet.monpotager.repository.PotagerRepository;
import eu.adanet.monpotager.service.PotagerService;
import eu.adanet.monpotager.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link eu.adanet.monpotager.domain.Potager}.
 */
@RestController
@RequestMapping("/api")
public class PotagerResource {

    private final Logger log = LoggerFactory.getLogger(PotagerResource.class);

    private static final String ENTITY_NAME = "potager";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PotagerService potagerService;

    private final PotagerRepository potagerRepository;

    public PotagerResource(PotagerService potagerService, PotagerRepository potagerRepository) {
        this.potagerService = potagerService;
        this.potagerRepository = potagerRepository;
    }

    /**
     * {@code POST  /potagers} : Create a new potager.
     *
     * @param potager the potager to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new potager, or with status {@code 400 (Bad Request)} if the potager has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/potagers")
    public ResponseEntity<Potager> createPotager(@RequestBody Potager potager) throws URISyntaxException {
        log.debug("REST request to save Potager : {}", potager);
        if (potager.getId() != null) {
            throw new BadRequestAlertException("A new potager cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Potager result = potagerService.save(potager);
        return ResponseEntity
            .created(new URI("/api/potagers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /potagers/:id} : Updates an existing potager.
     *
     * @param id the id of the potager to save.
     * @param potager the potager to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated potager,
     * or with status {@code 400 (Bad Request)} if the potager is not valid,
     * or with status {@code 500 (Internal Server Error)} if the potager couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/potagers/{id}")
    public ResponseEntity<Potager> updatePotager(@PathVariable(value = "id", required = false) final Long id, @RequestBody Potager potager)
        throws URISyntaxException {
        log.debug("REST request to update Potager : {}, {}", id, potager);
        if (potager.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, potager.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!potagerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Potager result = potagerService.update(potager);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, potager.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /potagers/:id} : Partial updates given fields of an existing potager, field will ignore if it is null
     *
     * @param id the id of the potager to save.
     * @param potager the potager to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated potager,
     * or with status {@code 400 (Bad Request)} if the potager is not valid,
     * or with status {@code 404 (Not Found)} if the potager is not found,
     * or with status {@code 500 (Internal Server Error)} if the potager couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/potagers/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Potager> partialUpdatePotager(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Potager potager
    ) throws URISyntaxException {
        log.debug("REST request to partial update Potager partially : {}, {}", id, potager);
        if (potager.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, potager.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!potagerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Potager> result = potagerService.partialUpdate(potager);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, potager.getId().toString())
        );
    }

    /**
     * {@code GET  /potagers} : get all the potagers.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of potagers in body.
     */
    @GetMapping("/potagers")
    public List<Potager> getAllPotagers() {
        log.debug("REST request to get all Potagers");
        return potagerService.findAll();
    }

    /**
     * {@code GET  /potagers/:id} : get the "id" potager.
     *
     * @param id the id of the potager to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the potager, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/potagers/{id}")
    public ResponseEntity<Potager> getPotager(@PathVariable Long id) {
        log.debug("REST request to get Potager : {}", id);
        Optional<Potager> potager = potagerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(potager);
    }

    /**
     * {@code DELETE  /potagers/:id} : delete the "id" potager.
     *
     * @param id the id of the potager to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/potagers/{id}")
    public ResponseEntity<Void> deletePotager(@PathVariable Long id) {
        log.debug("REST request to delete Potager : {}", id);
        potagerService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
