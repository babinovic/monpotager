/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import PotagerComponent from '@/entities/potager/potager.vue';
import PotagerClass from '@/entities/potager/potager.component';
import PotagerService from '@/entities/potager/potager.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(ToastPlugin);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Potager Management Component', () => {
    let wrapper: Wrapper<PotagerClass>;
    let comp: PotagerClass;
    let potagerServiceStub: SinonStubbedInstance<PotagerService>;

    beforeEach(() => {
      potagerServiceStub = sinon.createStubInstance<PotagerService>(PotagerService);
      potagerServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<PotagerClass>(PotagerComponent, {
        store,
        i18n,
        localVue,
        stubs: { bModal: bModalStub as any },
        provide: {
          potagerService: () => potagerServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      potagerServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllPotagers();
      await comp.$nextTick();

      // THEN
      expect(potagerServiceStub.retrieve.called).toBeTruthy();
      expect(comp.potagers[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      potagerServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      expect(potagerServiceStub.retrieve.callCount).toEqual(1);

      comp.removePotager();
      await comp.$nextTick();

      // THEN
      expect(potagerServiceStub.delete.called).toBeTruthy();
      expect(potagerServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
