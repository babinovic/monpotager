export interface IPotager {
  id?: number;
  nom?: string | null;
  longitude?: string | null;
  latitude?: string | null;
}

export class Potager implements IPotager {
  constructor(public id?: number, public nom?: string | null, public longitude?: string | null, public latitude?: string | null) {}
}
