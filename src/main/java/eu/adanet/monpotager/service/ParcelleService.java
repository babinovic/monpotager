package eu.adanet.monpotager.service;

import eu.adanet.monpotager.domain.Parcelle;
import eu.adanet.monpotager.domain.Plantation;
import eu.adanet.monpotager.domain.Tache;
import eu.adanet.monpotager.repository.ParcelleRepository;
import eu.adanet.monpotager.repository.PlantationRepository;
import eu.adanet.monpotager.repository.TacheRepository;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Parcelle}.
 */
@Service
@Transactional
public class ParcelleService {

    private final Logger log = LoggerFactory.getLogger(ParcelleService.class);

    private final ParcelleRepository parcelleRepository;
    private final PlantationRepository plantationRepository;
    private final TacheRepository tacheRepository;

    public ParcelleService(ParcelleRepository parcelleRepository, PlantationRepository plantationRepository, TacheRepository tacheRepository) {
        this.parcelleRepository = parcelleRepository;
        this.plantationRepository = plantationRepository;
        this.tacheRepository = tacheRepository;
    }

    /**
     * Save a parcelle.
     *
     * @param parcelle the entity to save.
     * @return the persisted entity.
     */
    public Parcelle save(Parcelle parcelle) {
        log.debug("Request to save Parcelle : {}", parcelle);
        return parcelleRepository.save(parcelle);
    }

    /**
     * Update a parcelle.
     *
     * @param parcelle the entity to save.
     * @return the persisted entity.
     */
    public Parcelle update(Parcelle parcelle) {
        log.debug("Request to update Parcelle : {}", parcelle);
        return parcelleRepository.save(parcelle);
    }

    /**
     * Partially update a parcelle.
     *
     * @param parcelle the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Parcelle> partialUpdate(Parcelle parcelle) {
        log.debug("Request to partially update Parcelle : {}", parcelle);

        return parcelleRepository
            .findById(parcelle.getId())
            .map(existingParcelle -> {
                if (parcelle.getNom() != null) {
                    existingParcelle.setNom(parcelle.getNom());
                }
                if (parcelle.getLongueur() != null) {
                    existingParcelle.setLongueur(parcelle.getLongueur());
                }
                if (parcelle.getLargeur() != null) {
                    existingParcelle.setLargeur(parcelle.getLargeur());
                }
                if (parcelle.getEnsoleillement() != null) {
                    existingParcelle.setEnsoleillement(parcelle.getEnsoleillement());
                }
                if (parcelle.getNbPlantationLongueur() != null) {
                    existingParcelle.setNbPlantationLongueur(parcelle.getNbPlantationLongueur());
                }
                if (parcelle.getNbPlantationLargeur() != null) {
                    existingParcelle.setNbPlantationLargeur(parcelle.getNbPlantationLargeur());
                }

                return existingParcelle;
            })
            .map(parcelleRepository::save);
    }

    /**
     * Get all the parcelles.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Parcelle> findAll(Pageable pageable) {
        log.debug("Request to get all Parcelles");
        return parcelleRepository.findAll(pageable);
    }

    /**
     * Get all the parcelles with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<Parcelle> findAllWithEagerRelationships(Pageable pageable) {
        return parcelleRepository.findAllWithEagerRelationships(pageable);
    }

    /**
     * Get one parcelle by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Parcelle> findOne(Long id) {
        log.debug("Request to get Parcelle : {}", id);
        return parcelleRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the parcelle by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Parcelle : {}", id);
        // cascade delete plantations
        for (Plantation plantation : plantationRepository.findAll()) {
            if (plantation.getParcelle().getId().longValue() == id) {
                plantationRepository.delete(plantation);
            }           
        }
        // cascade delete taches
        for (Tache tache : tacheRepository.findAll()) {
            if (tache.getParcelle().getId().longValue() == id) {
                tacheRepository.delete(tache);
            }           
        }
        parcelleRepository.deleteById(id);
    }
}
