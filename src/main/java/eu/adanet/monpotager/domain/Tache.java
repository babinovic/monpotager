package eu.adanet.monpotager.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import eu.adanet.monpotager.domain.enumeration.Etat;
import eu.adanet.monpotager.domain.enumeration.echelleTache;
import eu.adanet.monpotager.domain.enumeration.typeTache;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;

/**
 * A Tache.
 */
@Entity
@Table(name = "tache")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Tache implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private typeTache type;

    @Enumerated(EnumType.STRING)
    @Column(name = "echelle")
    private echelleTache echelle;

    @Column(name = "date_debut")
    private LocalDate dateDebut;

    @Column(name = "date_fin")
    private LocalDate dateFin;

    @Enumerated(EnumType.STRING)
    @Column(name = "etat")
    private Etat etat;

    @ManyToOne
    private Potager potager;

    @ManyToOne
    @JsonIgnoreProperties(value = { "potager" }, allowSetters = true)
    private Parcelle parcelle;

    @ManyToOne
    private Conseil conseil;

    public Tache(String nom, typeTache type, echelleTache echelle, LocalDate dateDebut, LocalDate dateFin, Etat etat, Conseil conseil, Potager potager) {
        this.nom = nom;
        this.type = type;
        this.echelle = echelle;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.etat = etat;
        this.conseil = conseil;
        this.potager = potager;
    }

    public Tache() {
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Tache id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public Tache nom(String nom) {
        this.setNom(nom);
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public typeTache getType() {
        return this.type;
    }

    public Tache type(typeTache type) {
        this.setType(type);
        return this;
    }

    public void setType(typeTache type) {
        this.type = type;
    }

    public echelleTache getEchelle() {
        return this.echelle;
    }

    public Tache echelle(echelleTache echelle) {
        this.setEchelle(echelle);
        return this;
    }

    public void setEchelle(echelleTache echelle) {
        this.echelle = echelle;
    }

    public LocalDate getDateDebut() {
        return this.dateDebut;
    }

    public Tache dateDebut(LocalDate dateDebut) {
        this.setDateDebut(dateDebut);
        return this;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return this.dateFin;
    }

    public Tache dateFin(LocalDate dateFin) {
        this.setDateFin(dateFin);
        return this;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public Etat getEtat() {
        return this.etat;
    }

    public Tache etat(Etat etat) {
        this.setEtat(etat);
        return this;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public Potager getPotager() {
        return this.potager;
    }

    public void setPotager(Potager potager) {
        this.potager = potager;
    }

    public Tache potager(Potager potager) {
        this.setPotager(potager);
        return this;
    }

    public Parcelle getParcelle() {
        return this.parcelle;
    }

    public void setParcelle(Parcelle parcelle) {
        this.parcelle = parcelle;
    }

    public Tache parcelle(Parcelle parcelle) {
        this.setParcelle(parcelle);
        return this;
    }

    public Conseil getConseil() {
        return this.conseil;
    }

    public void setConseil(Conseil conseil) {
        this.conseil = conseil;
    }

    public Tache conseil(Conseil conseil) {
        this.setConseil(conseil);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Tache)) {
            return false;
        }
        return id != null && id.equals(((Tache) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Tache{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", type='" + getType() + "'" +
            ", echelle='" + getEchelle() + "'" +
            ", dateDebut='" + getDateDebut() + "'" +
            ", dateFin='" + getDateFin() + "'" +
            ", etat='" + getEtat() + "'" +
            "}";
    }
}
