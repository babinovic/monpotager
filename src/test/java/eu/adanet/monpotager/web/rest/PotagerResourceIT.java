package eu.adanet.monpotager.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import eu.adanet.monpotager.IntegrationTest;
import eu.adanet.monpotager.domain.Potager;
import eu.adanet.monpotager.repository.PotagerRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PotagerResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PotagerResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_LONGITUDE = "AAAAAAAAAA";
    private static final String UPDATED_LONGITUDE = "BBBBBBBBBB";

    private static final String DEFAULT_LATITUDE = "AAAAAAAAAA";
    private static final String UPDATED_LATITUDE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/potagers";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PotagerRepository potagerRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPotagerMockMvc;

    private Potager potager;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Potager createEntity(EntityManager em) {
        Potager potager = new Potager().nom(DEFAULT_NOM).longitude(DEFAULT_LONGITUDE).latitude(DEFAULT_LATITUDE);
        return potager;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Potager createUpdatedEntity(EntityManager em) {
        Potager potager = new Potager().nom(UPDATED_NOM).longitude(UPDATED_LONGITUDE).latitude(UPDATED_LATITUDE);
        return potager;
    }

    @BeforeEach
    public void initTest() {
        potager = createEntity(em);
    }

    @Test
    @Transactional
    void createPotager() throws Exception {
        int databaseSizeBeforeCreate = potagerRepository.findAll().size();
        // Create the Potager
        restPotagerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(potager)))
            .andExpect(status().isCreated());

        // Validate the Potager in the database
        List<Potager> potagerList = potagerRepository.findAll();
        assertThat(potagerList).hasSize(databaseSizeBeforeCreate + 1);
        Potager testPotager = potagerList.get(potagerList.size() - 1);
        assertThat(testPotager.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testPotager.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testPotager.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
    }

    @Test
    @Transactional
    void createPotagerWithExistingId() throws Exception {
        // Create the Potager with an existing ID
        potager.setId(1L);

        int databaseSizeBeforeCreate = potagerRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPotagerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(potager)))
            .andExpect(status().isBadRequest());

        // Validate the Potager in the database
        List<Potager> potagerList = potagerRepository.findAll();
        assertThat(potagerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllPotagers() throws Exception {
        // Initialize the database
        potagerRepository.saveAndFlush(potager);

        // Get all the potagerList
        restPotagerMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(potager.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE)))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE)));
    }

    @Test
    @Transactional
    void getPotager() throws Exception {
        // Initialize the database
        potagerRepository.saveAndFlush(potager);

        // Get the potager
        restPotagerMockMvc
            .perform(get(ENTITY_API_URL_ID, potager.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(potager.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE));
    }

    @Test
    @Transactional
    void getNonExistingPotager() throws Exception {
        // Get the potager
        restPotagerMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingPotager() throws Exception {
        // Initialize the database
        potagerRepository.saveAndFlush(potager);

        int databaseSizeBeforeUpdate = potagerRepository.findAll().size();

        // Update the potager
        Potager updatedPotager = potagerRepository.findById(potager.getId()).get();
        // Disconnect from session so that the updates on updatedPotager are not directly saved in db
        em.detach(updatedPotager);
        updatedPotager.nom(UPDATED_NOM).longitude(UPDATED_LONGITUDE).latitude(UPDATED_LATITUDE);

        restPotagerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedPotager.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedPotager))
            )
            .andExpect(status().isOk());

        // Validate the Potager in the database
        List<Potager> potagerList = potagerRepository.findAll();
        assertThat(potagerList).hasSize(databaseSizeBeforeUpdate);
        Potager testPotager = potagerList.get(potagerList.size() - 1);
        assertThat(testPotager.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testPotager.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testPotager.getLatitude()).isEqualTo(UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    void putNonExistingPotager() throws Exception {
        int databaseSizeBeforeUpdate = potagerRepository.findAll().size();
        potager.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPotagerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, potager.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(potager))
            )
            .andExpect(status().isBadRequest());

        // Validate the Potager in the database
        List<Potager> potagerList = potagerRepository.findAll();
        assertThat(potagerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPotager() throws Exception {
        int databaseSizeBeforeUpdate = potagerRepository.findAll().size();
        potager.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPotagerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(potager))
            )
            .andExpect(status().isBadRequest());

        // Validate the Potager in the database
        List<Potager> potagerList = potagerRepository.findAll();
        assertThat(potagerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPotager() throws Exception {
        int databaseSizeBeforeUpdate = potagerRepository.findAll().size();
        potager.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPotagerMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(potager)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Potager in the database
        List<Potager> potagerList = potagerRepository.findAll();
        assertThat(potagerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePotagerWithPatch() throws Exception {
        // Initialize the database
        potagerRepository.saveAndFlush(potager);

        int databaseSizeBeforeUpdate = potagerRepository.findAll().size();

        // Update the potager using partial update
        Potager partialUpdatedPotager = new Potager();
        partialUpdatedPotager.setId(potager.getId());

        partialUpdatedPotager.nom(UPDATED_NOM);

        restPotagerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPotager.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPotager))
            )
            .andExpect(status().isOk());

        // Validate the Potager in the database
        List<Potager> potagerList = potagerRepository.findAll();
        assertThat(potagerList).hasSize(databaseSizeBeforeUpdate);
        Potager testPotager = potagerList.get(potagerList.size() - 1);
        assertThat(testPotager.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testPotager.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testPotager.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
    }

    @Test
    @Transactional
    void fullUpdatePotagerWithPatch() throws Exception {
        // Initialize the database
        potagerRepository.saveAndFlush(potager);

        int databaseSizeBeforeUpdate = potagerRepository.findAll().size();

        // Update the potager using partial update
        Potager partialUpdatedPotager = new Potager();
        partialUpdatedPotager.setId(potager.getId());

        partialUpdatedPotager.nom(UPDATED_NOM).longitude(UPDATED_LONGITUDE).latitude(UPDATED_LATITUDE);

        restPotagerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPotager.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPotager))
            )
            .andExpect(status().isOk());

        // Validate the Potager in the database
        List<Potager> potagerList = potagerRepository.findAll();
        assertThat(potagerList).hasSize(databaseSizeBeforeUpdate);
        Potager testPotager = potagerList.get(potagerList.size() - 1);
        assertThat(testPotager.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testPotager.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testPotager.getLatitude()).isEqualTo(UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    void patchNonExistingPotager() throws Exception {
        int databaseSizeBeforeUpdate = potagerRepository.findAll().size();
        potager.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPotagerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, potager.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(potager))
            )
            .andExpect(status().isBadRequest());

        // Validate the Potager in the database
        List<Potager> potagerList = potagerRepository.findAll();
        assertThat(potagerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPotager() throws Exception {
        int databaseSizeBeforeUpdate = potagerRepository.findAll().size();
        potager.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPotagerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(potager))
            )
            .andExpect(status().isBadRequest());

        // Validate the Potager in the database
        List<Potager> potagerList = potagerRepository.findAll();
        assertThat(potagerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPotager() throws Exception {
        int databaseSizeBeforeUpdate = potagerRepository.findAll().size();
        potager.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPotagerMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(potager)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Potager in the database
        List<Potager> potagerList = potagerRepository.findAll();
        assertThat(potagerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePotager() throws Exception {
        // Initialize the database
        potagerRepository.saveAndFlush(potager);

        int databaseSizeBeforeDelete = potagerRepository.findAll().size();

        // Delete the potager
        restPotagerMockMvc
            .perform(delete(ENTITY_API_URL_ID, potager.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Potager> potagerList = potagerRepository.findAll();
        assertThat(potagerList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
