package eu.adanet.monpotager.web.rest;

import eu.adanet.monpotager.domain.Culture;
import eu.adanet.monpotager.domain.Plante;
import eu.adanet.monpotager.repository.CultureRepository;
import eu.adanet.monpotager.service.CultureQueryService;
import eu.adanet.monpotager.service.CultureService;
import eu.adanet.monpotager.service.PlantationService;
import eu.adanet.monpotager.service.PlanteService;
import eu.adanet.monpotager.service.criteria.CultureCriteria;
import eu.adanet.monpotager.service.criteria.DataConflit;
import eu.adanet.monpotager.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link eu.adanet.monpotager.domain.Culture}.
 */
@RestController
@RequestMapping("/api")
public class CultureResource {

    private final Logger log = LoggerFactory.getLogger(CultureResource.class);

    private static final String ENTITY_NAME = "culture";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CultureService cultureService;

    private final CultureRepository cultureRepository;

    private final CultureQueryService cultureQueryService;
    private final PlantationService plantationService;
    private final PlanteService planteService;

    public CultureResource(CultureService cultureService, CultureRepository cultureRepository, CultureQueryService cultureQueryService, PlantationService plantationService, PlanteService planteService) {
        this.cultureService = cultureService;
        this.cultureRepository = cultureRepository;
        this.cultureQueryService = cultureQueryService;
        this.plantationService = plantationService;
        this.planteService = planteService;
    }

    /**
     * {@code POST  /cultures} : Create a new culture.
     *
     * @param culture the culture to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new culture, or with status {@code 400 (Bad Request)} if the culture has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cultures")
    public ResponseEntity<Culture> createCulture(@RequestBody Culture culture) throws URISyntaxException {
        log.debug("REST request to save Culture : {}", culture);
        if (culture.getId() != null) {
            throw new BadRequestAlertException("A new culture cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DataConflit conflit= this.existeConflit(culture);
        if (conflit.getNom() != null && conflit.getNom() != "") {
            String message = "La culture de " + conflit.getNom() + " commence le " + conflit.getDateDebut().toString() + " et se termine le " + conflit.getDateFin().toString() + ".Ces dates entrent en conflit avec ce que vous souhaitez creer. Changez d'emplacement.";
            throw new BadRequestAlertException(message, ENTITY_NAME, "conflitDate");
        }
        Culture result = cultureService.save(culture);
        return ResponseEntity
            .created(new URI("/api/cultures/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    // on vérifie qu'il n'y a pas de confilts pour créer la culture (date debut et fin)
    private DataConflit existeConflit(Culture culture) {
        DataConflit conflit = new DataConflit();
        CultureCriteria criteres = new CultureCriteria();
        LongFilter plantationFilter = new LongFilter();
        plantationFilter.setEquals(culture.getPlantation().getId());
        criteres.setPlantationId(plantationFilter);
        List<Culture> plantationCultures = cultureQueryService.findByCriteria(criteres);
        for (Culture culturePlantation : plantationCultures) {
            // pour chaque culture, on regarde si cela crée un conflit avec la culture à créer
            if ((culture.getDateDebut().isBefore(culturePlantation.getDateFin()) && culture.getDateDebut().isAfter(culturePlantation.getDateDebut())) ||
            (culture.getDateFin().isBefore(culturePlantation.getDateFin()) && culture.getDateFin().isAfter(culturePlantation.getDateDebut())) ||
            (culturePlantation.getDateDebut().isBefore(culture.getDateDebut()) && culturePlantation.getDateFin().isAfter(culture.getDateFin()))) {
                conflit.setNom(culturePlantation.getNom());
                conflit.setDateDebut(culturePlantation.getDateDebut());
                conflit.setDateFin(culturePlantation.getDateFin());
            }
        }
        return conflit;
    }

    /**
     * {@code PUT  /cultures/:id} : Updates an existing culture.
     *
     * @param id the id of the culture to save.
     * @param culture the culture to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated culture,
     * or with status {@code 400 (Bad Request)} if the culture is not valid,
     * or with status {@code 500 (Internal Server Error)} if the culture couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cultures/{id}")
    public ResponseEntity<Culture> updateCulture(@PathVariable(value = "id", required = false) final Long id, @RequestBody Culture culture)
        throws URISyntaxException {
        log.debug("REST request to update Culture : {}, {}", id, culture);
        if (culture.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, culture.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!cultureRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Culture result = cultureService.update(culture);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, culture.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /cultures/:id} : Partial updates given fields of an existing culture, field will ignore if it is null
     *
     * @param id the id of the culture to save.
     * @param culture the culture to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated culture,
     * or with status {@code 400 (Bad Request)} if the culture is not valid,
     * or with status {@code 404 (Not Found)} if the culture is not found,
     * or with status {@code 500 (Internal Server Error)} if the culture couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/cultures/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Culture> partialUpdateCulture(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Culture culture
    ) throws URISyntaxException {
        log.debug("REST request to partial update Culture partially : {}, {}", id, culture);
        if (culture.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, culture.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!cultureRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Culture> result = cultureService.partialUpdate(culture);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, culture.getId().toString())
        );
    }

    /**
     * {@code GET  /cultures} : get all the cultures.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cultures in body.
     */
    @GetMapping("/cultures")
    public ResponseEntity<List<Culture>> getAllCultures(
        CultureCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get Cultures by criteria: {}", criteria);
        Page<Culture> page = cultureQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /cultures/count} : count all the cultures.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/cultures/count")
    public ResponseEntity<Long> countCultures(CultureCriteria criteria) {
        log.debug("REST request to count Cultures by criteria: {}", criteria);
        return ResponseEntity.ok().body(cultureQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /cultures/:id} : get the "id" culture.
     *
     * @param id the id of the culture to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the culture, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cultures/{id}")
    public ResponseEntity<Culture> getCulture(@PathVariable Long id) {
        log.debug("REST request to get Culture : {}", id);
        Optional<Culture> culture = cultureService.findOne(id);
        return ResponseUtil.wrapOrNotFound(culture);
    }

    /**
     * {@code DELETE  /cultures/:id} : delete the "id" culture.
     *
     * @param id the id of the culture to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cultures/{id}")
    public ResponseEntity<Void> deleteCulture(@PathVariable Long id) {
        log.debug("REST request to delete Culture : {}", id);
        cultureService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code GET  /cultures/arracher/:id} : arracher la culture -> set Date de Fin mais pas Date de Récolte 
     *
     * @param id the id of the culture to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the culture, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cultures/arracher/{id}")
    public ResponseEntity<Culture> arracherCulture(@PathVariable Long id) {
        log.debug("REST request to get Culture : {}", id);        
        Culture result = cultureService.update(this.getCultureArrachee(cultureService.findOne(id)));  
        return ResponseEntity.ok().body(result);
    }

     /**
     * {@code GET  /cultures/recolter/:id} : recolter la culture -> set Date de Fin et Date de Récolte 
     *
     * @param id the id of the culture to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the culture, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cultures/recolter/{id}")
    public ResponseEntity<Culture> recolterCulture(@PathVariable Long id) {
        log.debug("REST request to get Culture : {}", id);        
        Culture result = cultureService.update(this.getCultureRecoltee(cultureService.findOne(id)));    
        return ResponseEntity.ok().body(result);
    }

    /**
     * {@code GET  /cultures/recolter/:id} : planter la culture -> créer culture
     *
     * @param id the id of the culture to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the culture, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cultures/planter/{id}")
    public ResponseEntity<Culture> planterCulture(
        @RequestParam(required = true) Long plantationId,
        @RequestParam(required = true) Long planteId,
        @RequestParam(required = true) Long dateDebut,
        @RequestParam(required = true) Long dateFin
    ) 
    {
        log.debug("REST request pour planter une Culture : {}", plantationId);
        Culture culture = new Culture();
        Plante plante = planteService.findOne(planteId).get();
        culture.dateDebut(Instant.ofEpochMilli(dateDebut).atZone(ZoneId.systemDefault()).toLocalDate());
        culture.dateFin(Instant.ofEpochMilli(dateFin).atZone(ZoneId.systemDefault()).toLocalDate());
        culture.plante(plante);
        culture.plantation(plantationService.findOne(plantationId).get());
        culture.nom(plante.getCategorie().toString() + "de " + plante.getNom() + " - " + plante.getVariete());
        Culture result = cultureService.save(culture);
        return ResponseEntity.ok().body(result);
    }
    

    // renvoie la culture modifiée
    private Culture getCultureArrachee(Optional<Culture> cultureOptional) {
        Culture cultureArrachee = cultureOptional.get();
        cultureArrachee.setDateFin(LocalDate.now());
        return cultureArrachee;
    }

    // renvoie la culture modifiée
    private Culture getCultureRecoltee(Optional<Culture> cultureOptional) {
        Culture cultureArrachee = cultureOptional.get();
        cultureArrachee.setDateFin(LocalDate.now());
        cultureArrachee.setDateRecolte(LocalDate.now());
        return cultureArrachee;
    }
}
