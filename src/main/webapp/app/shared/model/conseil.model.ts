export interface IConseil {
  id?: number;
  dateDebutPeriode?: Date | null;
  dateFinPeriode?: Date | null;
}

export class Conseil implements IConseil {
  constructor(public id?: number, public dateDebutPeriode?: Date | null, public dateFinPeriode?: Date | null) {}
}
