package eu.adanet.monpotager.domain.enumeration;

/**
 * The echelleTache enumeration.
 */
public enum echelleTache {
    POTAGER,
    PARCELLE,
    PLANTATION,
}
