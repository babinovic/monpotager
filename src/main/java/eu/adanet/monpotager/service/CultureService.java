package eu.adanet.monpotager.service;

import eu.adanet.monpotager.domain.Culture;
import eu.adanet.monpotager.domain.Plantation;
import eu.adanet.monpotager.repository.CultureRepository;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Culture}.
 */
@Service
@Transactional
public class CultureService {

    private final Logger log = LoggerFactory.getLogger(CultureService.class);

    private final CultureRepository cultureRepository;

    public CultureService(CultureRepository cultureRepository) {
        this.cultureRepository = cultureRepository;
    }

    /**
     * Save a culture.
     *
     * @param culture the entity to save.
     * @return the persisted entity.
     */
    public Culture save(Culture culture) {
        log.debug("Request to save Culture : {}", culture);
        return cultureRepository.save(culture);
    }

    /**
     * Update a culture.
     *
     * @param culture the entity to save.
     * @return the persisted entity.
     */
    public Culture update(Culture culture) {
        log.debug("Request to update Culture : {}", culture);
        return cultureRepository.save(culture);
    }

    /**
     * Partially update a culture.
     *
     * @param culture the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Culture> partialUpdate(Culture culture) {
        log.debug("Request to partially update Culture : {}", culture);

        return cultureRepository
            .findById(culture.getId())
            .map(existingCulture -> {
                if (culture.getNom() != null) {
                    existingCulture.setNom(culture.getNom());
                }
                if (culture.getDateDebut() != null) {
                    existingCulture.setDateDebut(culture.getDateDebut());
                }
                if (culture.getDateFin() != null) {
                    existingCulture.setDateFin(culture.getDateFin());
                }
                if (culture.getDateRecolte() != null) {
                    existingCulture.setDateRecolte(culture.getDateRecolte());
                }

                return existingCulture;
            })
            .map(cultureRepository::save);
    }

    /**
     * Get all the cultures.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Culture> findAll(Pageable pageable) {
        log.debug("Request to get all Cultures");
        return cultureRepository.findAll(pageable);
    }

    /**
     * Get all the cultures with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<Culture> findAllWithEagerRelationships(Pageable pageable) {
        return cultureRepository.findAllWithEagerRelationships(pageable);
    }

    /**
     * Get one culture by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Culture> findOne(Long id) {
        log.debug("Request to get Culture : {}", id);
        return cultureRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the culture by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Culture : {}", id);
        cultureRepository.deleteById(id);
    }

     /**
     * Renvoie la culture active d'une plantation
     *
     * @param plantation la plantation
     * @param dateVisualisation la date de visualisation
     */
    public Culture getCultureActive(Plantation plantation, Date dateVisualisation) {
        Culture cultureActive = cultureRepository.findActive(plantation.getId(), dateVisualisation != null ? dateVisualisation : new Date(System.currentTimeMillis()));
        if (cultureActive == null) {
            cultureActive = new Culture("Vide", plantation);
        }
        return cultureActive;
    }
}
