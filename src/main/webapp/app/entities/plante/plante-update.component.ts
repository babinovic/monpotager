import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import { IPlante, Plante } from '@/shared/model/plante.model';
import PlanteService from './plante.service';
import { typePlante } from '@/shared/model/enumerations/type-plante.model';
import { exposition } from '@/shared/model/enumerations/exposition.model';
import { frequenceArrosage } from '@/shared/model/enumerations/frequence-arrosage.model';
import { mois } from '@/shared/model/enumerations/mois.model';
import { categoriePlante } from '@/shared/model/enumerations/categorie-plante.model';

const validations: any = {
  plante: {
    nom: {},
    variete: {},
    type: {},
    espacement: {},
    espacementRang: {},
    ensoleillement: {},
    arrosage: {},
    debutPlantation: {},
    finPlantation: {},
    debutRecolte: {},
    finRecolte: {},
    debutSemis: {},
    finSemis: {},
    categorie: {},
    image: {},
  },
};

@Component({
  validations,
})
export default class PlanteUpdate extends Vue {
  @Inject('planteService') private planteService: () => PlanteService;
  @Inject('alertService') private alertService: () => AlertService;

  public plante: IPlante = new Plante();
  public typePlanteValues: string[] = Object.keys(typePlante);
  public expositionValues: string[] = Object.keys(exposition);
  public frequenceArrosageValues: string[] = Object.keys(frequenceArrosage);
  public moisValues: string[] = Object.keys(mois);
  public categoriePlanteValues: string[] = Object.keys(categoriePlante);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.planteId) {
        vm.retrievePlante(to.params.planteId);
      }
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.plante.id) {
      this.planteService()
        .update(this.plante)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('monpotagerApp.plante.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.planteService()
        .create(this.plante)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('monpotagerApp.plante.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrievePlante(planteId): void {
    this.planteService()
      .find(planteId)
      .then(res => {
        this.plante = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
