import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import { IConseil, Conseil } from '@/shared/model/conseil.model';
import ConseilService from './conseil.service';

const validations: any = {
  conseil: {
    dateDebutPeriode: {},
    dateFinPeriode: {},
  },
};

@Component({
  validations,
})
export default class ConseilUpdate extends Vue {
  @Inject('conseilService') private conseilService: () => ConseilService;
  @Inject('alertService') private alertService: () => AlertService;

  public conseil: IConseil = new Conseil();
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.conseilId) {
        vm.retrieveConseil(to.params.conseilId);
      }
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.conseil.id) {
      this.conseilService()
        .update(this.conseil)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('monpotagerApp.conseil.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.conseilService()
        .create(this.conseil)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('monpotagerApp.conseil.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveConseil(conseilId): void {
    this.conseilService()
      .find(conseilId)
      .then(res => {
        this.conseil = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
