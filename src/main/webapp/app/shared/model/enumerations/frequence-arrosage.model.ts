export enum frequenceArrosage {
  SANS = 'SANS',

  LEGER = 'LEGER',

  MOYEN = 'MOYEN',

  REGULIER = 'REGULIER',
}
