package eu.adanet.monpotager.domain;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;

/**
 * A Conseil.
 */
@Entity
@Table(name = "conseil")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Conseil implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "date_debut_periode")
    private LocalDate dateDebutPeriode;

    @Column(name = "date_fin_periode")
    private LocalDate dateFinPeriode;
    
    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Conseil(LocalDate dateDebutPeriode, LocalDate dateFinPeriode) {
        this.dateDebutPeriode = dateDebutPeriode;
        this.dateFinPeriode = dateFinPeriode;
    }

    public Conseil() {
    }

    public Long getId() {
        return this.id;
    }

    public Conseil id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateDebutPeriode() {
        return this.dateDebutPeriode;
    }

    public Conseil dateDebutPeriode(LocalDate dateDebutPeriode) {
        this.setDateDebutPeriode(dateDebutPeriode);
        return this;
    }

    public void setDateDebutPeriode(LocalDate dateDebutPeriode) {
        this.dateDebutPeriode = dateDebutPeriode;
    }

    public LocalDate getDateFinPeriode() {
        return this.dateFinPeriode;
    }

    public Conseil dateFinPeriode(LocalDate dateFinPeriode) {
        this.setDateFinPeriode(dateFinPeriode);
        return this;
    }

    public void setDateFinPeriode(LocalDate dateFinPeriode) {
        this.dateFinPeriode = dateFinPeriode;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Conseil)) {
            return false;
        }
        return id != null && id.equals(((Conseil) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Conseil{" +
            "id=" + getId() +
            ", dateDebutPeriode='" + getDateDebutPeriode() + "'" +
            ", dateFinPeriode='" + getDateFinPeriode() + "'" +
            "}";
    }
}
