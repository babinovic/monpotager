package eu.adanet.monpotager.repository;

import eu.adanet.monpotager.domain.Potager;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Potager entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PotagerRepository extends JpaRepository<Potager, Long> {}
