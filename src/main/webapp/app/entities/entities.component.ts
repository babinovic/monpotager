import { Component, Provide, Vue } from 'vue-property-decorator';

import UserService from '@/entities/user/user.service';
import PotagerService from './potager/potager.service';
import ParcelleService from './parcelle/parcelle.service';
import PlanteService from './plante/plante.service';
import PlantationService from './plantation/plantation.service';
import TacheService from './tache/tache.service';
import CultureService from './culture/culture.service';
import ConseilService from './conseil/conseil.service';
// jhipster-needle-add-entity-service-to-entities-component-import - JHipster will import entities services here

@Component
export default class Entities extends Vue {
  @Provide('userService') private userService = () => new UserService();
  @Provide('potagerService') private potagerService = () => new PotagerService();
  @Provide('parcelleService') private parcelleService = () => new ParcelleService();
  @Provide('planteService') private planteService = () => new PlanteService();
  @Provide('plantationService') private plantationService = () => new PlantationService();
  @Provide('tacheService') private tacheService = () => new TacheService();
  @Provide('cultureService') private cultureService = () => new CultureService();
  @Provide('conseilService') private conseilService = () => new ConseilService();
  // jhipster-needle-add-entity-service-to-entities-component - JHipster will import entities services here
}
