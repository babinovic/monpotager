import { IParcelle } from '@/shared/model/parcelle.model';

export interface IPlantation {
  id?: number;
  indexParcelle?: number | null;
  parcelle?: IParcelle | null;
}

export class Plantation implements IPlantation {
  constructor(public id?: number, public indexParcelle?: number | null, public parcelle?: IParcelle | null) {}
}
