package eu.adanet.monpotager.web.rest;

import eu.adanet.monpotager.domain.Plantation;
import eu.adanet.monpotager.repository.PlantationRepository;
import eu.adanet.monpotager.service.PlantationService;
import eu.adanet.monpotager.service.dto.PlantationCultureDTO;
import eu.adanet.monpotager.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link eu.adanet.monpotager.domain.Plantation}.
 */
@RestController
@RequestMapping("/api")
public class PlantationResource {

    private final Logger log = LoggerFactory.getLogger(PlantationResource.class);

    private static final String ENTITY_NAME = "plantation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PlantationService plantationService;

    private final PlantationRepository plantationRepository;

    public PlantationResource(PlantationService plantationService, PlantationRepository plantationRepository) {
        this.plantationService = plantationService;
        this.plantationRepository = plantationRepository;
    }

    /**
     * {@code POST  /plantations} : Create a new plantation.
     *
     * @param plantation the plantation to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new plantation, or with status {@code 400 (Bad Request)} if the plantation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/plantations")
    public ResponseEntity<Plantation> createPlantation(@RequestBody Plantation plantation) throws URISyntaxException {
        log.debug("REST request to save Plantation : {}", plantation);
        if (plantation.getId() != null) {
            throw new BadRequestAlertException("A new plantation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Plantation result = plantationService.save(plantation);
        return ResponseEntity
            .created(new URI("/api/plantations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /plantations/:id} : Updates an existing plantation.
     *
     * @param id the id of the plantation to save.
     * @param plantation the plantation to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated plantation,
     * or with status {@code 400 (Bad Request)} if the plantation is not valid,
     * or with status {@code 500 (Internal Server Error)} if the plantation couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/plantations/{id}")
    public ResponseEntity<Plantation> updatePlantation(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Plantation plantation
    ) throws URISyntaxException {
        log.debug("REST request to update Plantation : {}, {}", id, plantation);
        if (plantation.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, plantation.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!plantationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Plantation result = plantationService.update(plantation);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, plantation.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /plantations/:id} : Partial updates given fields of an existing plantation, field will ignore if it is null
     *
     * @param id the id of the plantation to save.
     * @param plantation the plantation to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated plantation,
     * or with status {@code 400 (Bad Request)} if the plantation is not valid,
     * or with status {@code 404 (Not Found)} if the plantation is not found,
     * or with status {@code 500 (Internal Server Error)} if the plantation couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/plantations/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Plantation> partialUpdatePlantation(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Plantation plantation
    ) throws URISyntaxException {
        log.debug("REST request to partial update Plantation partially : {}, {}", id, plantation);
        if (plantation.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, plantation.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!plantationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Plantation> result = plantationService.partialUpdate(plantation);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, plantation.getId().toString())
        );
    }

    /**
     * {@code GET  /plantations} : get all the plantations.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of plantations in body.
     */
    @GetMapping("/plantations")
    public ResponseEntity<List<Plantation>> getAllPlantations(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        @RequestParam(required = false, defaultValue = "false") boolean eagerload
    ) {
        log.debug("REST request to get a page of Plantations");
        Page<Plantation> page;
        if (eagerload) {
            page = plantationService.findAllWithEagerRelationships(pageable);
        } else {
            page = plantationService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /plantations} : get all the plantations pour une parcelle à une date donnée
     *
     * @param parcelleId 
     * @param dateVisualisation
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of plantations in body.
     */
    @GetMapping("/plantationsParcelle")
    public ResponseEntity<List<PlantationCultureDTO>> getAllPlantationsParcelle(
        @RequestParam(required = true) Long parcelleId,
        @RequestParam(required = false) Long dateVisualisation
    ) {
        log.debug("REST request to get a page of Plantations par parcelle, date=" + dateVisualisation.toString());
        List<PlantationCultureDTO> list = plantationService.getAllPlantationsParcelle(parcelleId, new Date(dateVisualisation));
        return ResponseEntity.ok().body(list);
    }

    /**
     * {@code GET  /plantations/:id} : get the "id" plantation.
     *
     * @param id the id of the plantation to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the plantation, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/plantations/{id}")
    public ResponseEntity<Plantation> getPlantation(@PathVariable Long id) {
        log.debug("REST request to get Plantation : {}", id);
        Optional<Plantation> plantation = plantationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(plantation);
    }

    /**
     * {@code DELETE  /plantations/:id} : delete the "id" plantation.
     *
     * @param id the id of the plantation to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/plantations/{id}")
    public ResponseEntity<Void> deletePlantation(@PathVariable Long id) {
        log.debug("REST request to delete Plantation : {}", id);
        plantationService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
