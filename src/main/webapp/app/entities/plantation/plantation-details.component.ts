import { Component, Vue, Inject } from 'vue-property-decorator';

import { IPlantation } from '@/shared/model/plantation.model';
import PlantationService from './plantation.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class PlantationDetails extends Vue {
  @Inject('plantationService') private plantationService: () => PlantationService;
  @Inject('alertService') private alertService: () => AlertService;

  public plantation: IPlantation = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.plantationId) {
        vm.retrievePlantation(to.params.plantationId);
      }
    });
  }

  public retrievePlantation(plantationId) {
    this.plantationService()
      .find(plantationId)
      .then(res => {
        this.plantation = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
