/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import PlantationDetailComponent from '@/entities/plantation/plantation-details.vue';
import PlantationClass from '@/entities/plantation/plantation-details.component';
import PlantationService from '@/entities/plantation/plantation.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Plantation Management Detail Component', () => {
    let wrapper: Wrapper<PlantationClass>;
    let comp: PlantationClass;
    let plantationServiceStub: SinonStubbedInstance<PlantationService>;

    beforeEach(() => {
      plantationServiceStub = sinon.createStubInstance<PlantationService>(PlantationService);

      wrapper = shallowMount<PlantationClass>(PlantationDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { plantationService: () => plantationServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundPlantation = { id: 123 };
        plantationServiceStub.find.resolves(foundPlantation);

        // WHEN
        comp.retrievePlantation(123);
        await comp.$nextTick();

        // THEN
        expect(comp.plantation).toBe(foundPlantation);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundPlantation = { id: 123 };
        plantationServiceStub.find.resolves(foundPlantation);

        // WHEN
        comp.beforeRouteEnter({ params: { plantationId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.plantation).toBe(foundPlantation);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
