package eu.adanet.monpotager.repository;

import eu.adanet.monpotager.domain.Plantation;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Plantation entity.
 */
@Repository
public interface PlantationRepository extends JpaRepository<Plantation, Long> {
    default Optional<Plantation> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<Plantation> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<Plantation> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct plantation from Plantation plantation left join fetch plantation.parcelle",
        countQuery = "select count(distinct plantation) from Plantation plantation"
    )
    Page<Plantation> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct plantation from Plantation plantation left join fetch plantation.parcelle")
    List<Plantation> findAllWithToOneRelationships();

    @Query("select plantation from Plantation plantation left join fetch plantation.parcelle where plantation.id =:id")
    Optional<Plantation> findOneWithToOneRelationships(@Param("id") Long id);
}
