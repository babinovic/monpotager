import { Authority } from '@/shared/security/authority';
/* tslint:disable */
// prettier-ignore
const Entities = () => import('@/entities/entities.vue');

const Potager = () => import('@/entities/potager/potager.vue');
const PotagerUpdate = () => import('@/entities/potager/potager-update.vue');
const PotagerDetails = () => import('@/entities/potager/potager-details.vue');

const Parcelle = () => import('@/entities/parcelle/parcelle.vue');
const ParcelleUpdate = () => import('@/entities/parcelle/parcelle-update.vue');
const ParcelleDetails = () => import('@/entities/parcelle/parcelle-details.vue');

const Plante = () => import('@/entities/plante/plante.vue');
const PlanteUpdate = () => import('@/entities/plante/plante-update.vue');
const PlanteDetails = () => import('@/entities/plante/plante-details.vue');

const Plantation = () => import('@/entities/plantation/plantation.vue');
const PlantationUpdate = () => import('@/entities/plantation/plantation-update.vue');
const PlantationDetails = () => import('@/entities/plantation/plantation-details.vue');

const Tache = () => import('@/entities/tache/tache.vue');
const TacheUpdate = () => import('@/entities/tache/tache-update.vue');
const TacheDetails = () => import('@/entities/tache/tache-details.vue');

// prettier-ignore
const Culture = () => import('@/entities/culture/culture.vue');
// prettier-ignore
const CultureUpdate = () => import('@/entities/culture/culture-update.vue');
// prettier-ignore
const CultureDetails = () => import('@/entities/culture/culture-details.vue');
// prettier-ignore
const Conseil = () => import('@/entities/conseil/conseil.vue');
// prettier-ignore
const ConseilUpdate = () => import('@/entities/conseil/conseil-update.vue');
// prettier-ignore
const ConseilDetails = () => import('@/entities/conseil/conseil-details.vue');
// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

export default {
  path: '/',
  component: Entities,
  children: [
    {
      path: 'potager',
      name: 'Potager',
      component: Potager,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'potager/new',
      name: 'PotagerCreate',
      component: PotagerUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'potager/:potagerId/edit',
      name: 'PotagerEdit',
      component: PotagerUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'potager/:potagerId/view',
      name: 'PotagerView',
      component: PotagerDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'parcelle',
      name: 'Parcelle',
      component: Parcelle,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'parcelle/new',
      name: 'ParcelleCreate',
      component: ParcelleUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'parcelle/:parcelleId/edit',
      name: 'ParcelleEdit',
      component: ParcelleUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'parcelle/:parcelleId/view',
      name: 'ParcelleView',
      component: ParcelleDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'plante',
      name: 'Plante',
      component: Plante,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'plante/new',
      name: 'PlanteCreate',
      component: PlanteUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'plante/:planteId/edit',
      name: 'PlanteEdit',
      component: PlanteUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'plante/:planteId/view',
      name: 'PlanteView',
      component: PlanteDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'plantation',
      name: 'Plantation',
      component: Plantation,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'plantation/new',
      name: 'PlantationCreate',
      component: PlantationUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'plantation/:plantationId/edit',
      name: 'PlantationEdit',
      component: PlantationUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'plantation/:plantationId/view',
      name: 'PlantationView',
      component: PlantationDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'tache',
      name: 'Tache',
      component: Tache,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'tache/new',
      name: 'TacheCreate',
      component: TacheUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'tache/:tacheId/edit',
      name: 'TacheEdit',
      component: TacheUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'tache/:tacheId/view',
      name: 'TacheView',
      component: TacheDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'culture',
      name: 'Culture',
      component: Culture,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'culture/new',
      name: 'CultureCreate',
      component: CultureUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'culture/:cultureId/edit',
      name: 'CultureEdit',
      component: CultureUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'culture/:cultureId/view',
      name: 'CultureView',
      component: CultureDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'conseil',
      name: 'Conseil',
      component: Conseil,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'conseil/new',
      name: 'ConseilCreate',
      component: ConseilUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'conseil/:conseilId/edit',
      name: 'ConseilEdit',
      component: ConseilUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'conseil/:conseilId/view',
      name: 'ConseilView',
      component: ConseilDetails,
      meta: { authorities: [Authority.USER] },
    },
    // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
  ],
};
