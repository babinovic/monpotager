import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import PotagerService from '@/entities/potager/potager.service';
import { IPotager } from '@/shared/model/potager.model';

import ParcelleService from '@/entities/parcelle/parcelle.service';
import { IParcelle } from '@/shared/model/parcelle.model';

import ConseilService from '@/entities/conseil/conseil.service';
import { IConseil } from '@/shared/model/conseil.model';

import { ITache, Tache } from '@/shared/model/tache.model';
import TacheService from './tache.service';
import { typeTache } from '@/shared/model/enumerations/type-tache.model';
import { echelleTache } from '@/shared/model/enumerations/echelle-tache.model';
import { Etat } from '@/shared/model/enumerations/etat.model';

const validations: any = {
  tache: {
    nom: {},
    type: {},
    echelle: {},
    dateDebut: {},
    dateFin: {},
    etat: {},
  },
};

@Component({
  validations,
})
export default class TacheUpdate extends Vue {
  @Inject('tacheService') private tacheService: () => TacheService;
  @Inject('alertService') private alertService: () => AlertService;

  public tache: ITache = new Tache();

  @Inject('potagerService') private potagerService: () => PotagerService;

  public potagers: IPotager[] = [];

  @Inject('parcelleService') private parcelleService: () => ParcelleService;

  public parcelles: IParcelle[] = [];

  @Inject('conseilService') private conseilService: () => ConseilService;

  public conseils: IConseil[] = [];
  public typeTacheValues: string[] = Object.keys(typeTache);
  public echelleTacheValues: string[] = Object.keys(echelleTache);
  public etatValues: string[] = Object.keys(Etat);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.tacheId) {
        vm.retrieveTache(to.params.tacheId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.tache.id) {
      this.tacheService()
        .update(this.tache)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('monpotagerApp.tache.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.tacheService()
        .create(this.tache)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('monpotagerApp.tache.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveTache(tacheId): void {
    this.tacheService()
      .find(tacheId)
      .then(res => {
        this.tache = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.potagerService()
      .retrieve()
      .then(res => {
        this.potagers = res.data;
      });
    this.parcelleService()
      .retrieve()
      .then(res => {
        this.parcelles = res.data;
      });
    this.conseilService()
      .retrieve()
      .then(res => {
        this.conseils = res.data;
      });
  }
}
