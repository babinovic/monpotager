package eu.adanet.monpotager.domain;

import eu.adanet.monpotager.domain.enumeration.categoriePlante;
import eu.adanet.monpotager.domain.enumeration.exposition;
import eu.adanet.monpotager.domain.enumeration.frequenceArrosage;
import eu.adanet.monpotager.domain.enumeration.Mois;
import eu.adanet.monpotager.domain.enumeration.typePlante;
import java.io.Serializable;
import javax.persistence.*;

/**
 * A Plante.
 */
@Entity
@Table(name = "plante")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Plante implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "variete")
    private String variete;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private typePlante type;

    @Column(name = "espacement")
    private Float espacement;

    @Column(name = "espacement_rang")
    private Float espacementRang;

    @Enumerated(EnumType.STRING)
    @Column(name = "ensoleillement")
    private exposition ensoleillement;

    @Enumerated(EnumType.STRING)
    @Column(name = "arrosage")
    private frequenceArrosage arrosage;

    @Enumerated(EnumType.STRING)
    @Column(name = "debut_plantation")
    private Mois debutPlantation;

    @Enumerated(EnumType.STRING)
    @Column(name = "fin_plantation")
    private Mois finPlantation;

    @Enumerated(EnumType.STRING)
    @Column(name = "debut_recolte")
    private Mois debutRecolte;

    @Enumerated(EnumType.STRING)
    @Column(name = "fin_recolte")
    private Mois finRecolte;

    @Enumerated(EnumType.STRING)
    @Column(name = "debut_semis")
    private Mois debutSemis;

    @Enumerated(EnumType.STRING)
    @Column(name = "fin_semis")
    private Mois finSemis;

    @Enumerated(EnumType.STRING)
    @Column(name = "categorie")
    private categoriePlante categorie;

    @Column(name = "image")
    private String image;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Plante id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public Plante nom(String nom) {
        this.setNom(nom);
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getVariete() {
        return this.variete;
    }

    public Plante variete(String variete) {
        this.setVariete(variete);
        return this;
    }

    public void setVariete(String variete) {
        this.variete = variete;
    }

    public typePlante getType() {
        return this.type;
    }

    public Plante type(typePlante type) {
        this.setType(type);
        return this;
    }

    public void setType(typePlante type) {
        this.type = type;
    }

    public Float getEspacement() {
        return this.espacement;
    }

    public Plante espacement(Float espacement) {
        this.setEspacement(espacement);
        return this;
    }

    public void setEspacement(Float espacement) {
        this.espacement = espacement;
    }

    public Float getEspacementRang() {
        return this.espacementRang;
    }

    public Plante espacementRang(Float espacementRang) {
        this.setEspacementRang(espacementRang);
        return this;
    }

    public void setEspacementRang(Float espacementRang) {
        this.espacementRang = espacementRang;
    }

    public exposition getEnsoleillement() {
        return this.ensoleillement;
    }

    public Plante ensoleillement(exposition ensoleillement) {
        this.setEnsoleillement(ensoleillement);
        return this;
    }

    public void setEnsoleillement(exposition ensoleillement) {
        this.ensoleillement = ensoleillement;
    }

    public frequenceArrosage getArrosage() {
        return this.arrosage;
    }

    public Plante arrosage(frequenceArrosage arrosage) {
        this.setArrosage(arrosage);
        return this;
    }

    public void setArrosage(frequenceArrosage arrosage) {
        this.arrosage = arrosage;
    }

    public Mois getDebutPlantation() {
        return this.debutPlantation;
    }

    public Plante debutPlantation(Mois debutPlantation) {
        this.setDebutPlantation(debutPlantation);
        return this;
    }

    public void setDebutPlantation(Mois debutPlantation) {
        this.debutPlantation = debutPlantation;
    }

    public Mois getFinPlantation() {
        return this.finPlantation;
    }

    public Plante finPlantation(Mois finPlantation) {
        this.setFinPlantation(finPlantation);
        return this;
    }

    public void setFinPlantation(Mois finPlantation) {
        this.finPlantation = finPlantation;
    }

    public Mois getDebutRecolte() {
        return this.debutRecolte;
    }

    public Plante debutRecolte(Mois debutRecolte) {
        this.setDebutRecolte(debutRecolte);
        return this;
    }

    public void setDebutRecolte(Mois debutRecolte) {
        this.debutRecolte = debutRecolte;
    }

    public Mois getFinRecolte() {
        return this.finRecolte;
    }

    public Plante finRecolte(Mois finRecolte) {
        this.setFinRecolte(finRecolte);
        return this;
    }

    public void setFinRecolte(Mois finRecolte) {
        this.finRecolte = finRecolte;
    }

    public Mois getDebutSemis() {
        return this.debutSemis;
    }

    public Plante debutSemis(Mois debutSemis) {
        this.setDebutSemis(debutSemis);
        return this;
    }

    public void setDebutSemis(Mois debutSemis) {
        this.debutSemis = debutSemis;
    }

    public Mois getFinSemis() {
        return this.finSemis;
    }

    public Plante finSemis(Mois finSemis) {
        this.setFinSemis(finSemis);
        return this;
    }

    public void setFinSemis(Mois finSemis) {
        this.finSemis = finSemis;
    }

    public categoriePlante getCategorie() {
        return this.categorie;
    }

    public Plante categorie(categoriePlante categorie) {
        this.setCategorie(categorie);
        return this;
    }

    public void setCategorie(categoriePlante categorie) {
        this.categorie = categorie;
    }

    public String getImage() {
        return this.image;
    }

    public Plante image(String image) {
        this.setImage(image);
        return this;
    }

    public void setImage(String image) {
        this.image = image;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Plante)) {
            return false;
        }
        return id != null && id.equals(((Plante) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Plante{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", variete='" + getVariete() + "'" +
            ", type='" + getType() + "'" +
            ", espacement=" + getEspacement() +
            ", espacementRang=" + getEspacementRang() +
            ", ensoleillement='" + getEnsoleillement() + "'" +
            ", arrosage='" + getArrosage() + "'" +
            ", debutPlantation='" + getDebutPlantation() + "'" +
            ", finPlantation='" + getFinPlantation() + "'" +
            ", debutRecolte='" + getDebutRecolte() + "'" +
            ", finRecolte='" + getFinRecolte() + "'" +
            ", debutSemis='" + getDebutSemis() + "'" +
            ", finSemis='" + getFinSemis() + "'" +
            ", categorie='" + getCategorie() + "'" +
            ", image='" + getImage() + "'" +
            "}";
    }
}
