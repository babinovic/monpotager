import { IPlantation } from '../model/plantation.model';
import { IPlante } from '../model/plante.model';

export interface IEmplacementPlantation {
  nomPlante?: string | null;
  image?: string | null;
  x?: number | null;
  y?: number | null;
  w?: number | null;
  h?: number | null;
  i?: number | null;
  plantation?: IPlantation | null,
  plante?: IPlante | null;
  nomCulture?: string | null;
  dateDebut?: string | null;
  dateFin?: string | null;
  dateRecolte?: string | null;
}

export class EmplacementPlantation implements IEmplacementPlantation {
  constructor(
    public nomPlante?: string | null,
    public image?: string | null,
    public x?: number | null,
    public y?: number | null,
    public w?: number | null,
    public h?: number | null,
    public i?: number | null,
    public plantation?: IPlantation | null,
    public plante?: IPlante | null,
    public nomCulture?: string | null,
    public dateDebut?: string | null,
    public dateFin?: string | null,
    public dateRecolte?: string | null,
  ) {}
}
