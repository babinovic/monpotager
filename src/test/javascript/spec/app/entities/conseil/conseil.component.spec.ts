/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import ConseilComponent from '@/entities/conseil/conseil.vue';
import ConseilClass from '@/entities/conseil/conseil.component';
import ConseilService from '@/entities/conseil/conseil.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(ToastPlugin);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Conseil Management Component', () => {
    let wrapper: Wrapper<ConseilClass>;
    let comp: ConseilClass;
    let conseilServiceStub: SinonStubbedInstance<ConseilService>;

    beforeEach(() => {
      conseilServiceStub = sinon.createStubInstance<ConseilService>(ConseilService);
      conseilServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<ConseilClass>(ConseilComponent, {
        store,
        i18n,
        localVue,
        stubs: { bModal: bModalStub as any },
        provide: {
          conseilService: () => conseilServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      conseilServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllConseils();
      await comp.$nextTick();

      // THEN
      expect(conseilServiceStub.retrieve.called).toBeTruthy();
      expect(comp.conseils[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      conseilServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      expect(conseilServiceStub.retrieve.callCount).toEqual(1);

      comp.removeConseil();
      await comp.$nextTick();

      // THEN
      expect(conseilServiceStub.delete.called).toBeTruthy();
      expect(conseilServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
