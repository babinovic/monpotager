import { IPotager } from '@/shared/model/potager.model';

import { exposition } from '@/shared/model/enumerations/exposition.model';
export interface IParcelle {
  id?: number;
  nom?: string | null;
  longueur?: number | null;
  largeur?: number | null;
  ensoleillement?: exposition | null;
  nbPlantationLongueur?: number | null;
  nbPlantationLargeur?: number | null;
  potager?: IPotager | null;
}

export class Parcelle implements IParcelle {
  constructor(
    public id?: number,
    public nom?: string | null,
    public longueur?: number | null,
    public largeur?: number | null,
    public ensoleillement?: exposition | null,
    public nbPlantationLongueur?: number | null,
    public nbPlantationLargeur?: number | null,
    public potager?: IPotager | null
  ) {}
}
