/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import PotagerUpdateComponent from '@/entities/potager/potager-update.vue';
import PotagerClass from '@/entities/potager/potager-update.component';
import PotagerService from '@/entities/potager/potager.service';

import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Potager Management Update Component', () => {
    let wrapper: Wrapper<PotagerClass>;
    let comp: PotagerClass;
    let potagerServiceStub: SinonStubbedInstance<PotagerService>;

    beforeEach(() => {
      potagerServiceStub = sinon.createStubInstance<PotagerService>(PotagerService);

      wrapper = shallowMount<PotagerClass>(PotagerUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          potagerService: () => potagerServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.potager = entity;
        potagerServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(potagerServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.potager = entity;
        potagerServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(potagerServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundPotager = { id: 123 };
        potagerServiceStub.find.resolves(foundPotager);
        potagerServiceStub.retrieve.resolves([foundPotager]);

        // WHEN
        comp.beforeRouteEnter({ params: { potagerId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.potager).toBe(foundPotager);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
