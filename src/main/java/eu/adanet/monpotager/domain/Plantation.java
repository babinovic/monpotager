package eu.adanet.monpotager.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;

/**
 * A Plantation.
 */
@Entity
@Table(name = "plantation")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Plantation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "index_parcelle")
    private Integer indexParcelle;

    @ManyToOne
    @JsonIgnoreProperties(value = { "potager" }, allowSetters = true)
    private Parcelle parcelle;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Plantation id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIndexParcelle() {
        return this.indexParcelle;
    }

    public Plantation indexParcelle(Integer indexParcelle) {
        this.setIndexParcelle(indexParcelle);
        return this;
    }

    public void setIndexParcelle(Integer indexParcelle) {
        this.indexParcelle = indexParcelle;
    }

    public Parcelle getParcelle() {
        return this.parcelle;
    }

    public void setParcelle(Parcelle parcelle) {
        this.parcelle = parcelle;
    }

    public Plantation parcelle(Parcelle parcelle) {
        this.setParcelle(parcelle);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Plantation)) {
            return false;
        }
        return id != null && id.equals(((Plantation) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Plantation{" +
            "id=" + getId() +
            ", indexParcelle=" + getIndexParcelle() +
            "}";
    }
}
