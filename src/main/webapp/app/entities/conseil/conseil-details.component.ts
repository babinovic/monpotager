import { Component, Vue, Inject } from 'vue-property-decorator';

import { IConseil } from '@/shared/model/conseil.model';
import ConseilService from './conseil.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class ConseilDetails extends Vue {
  @Inject('conseilService') private conseilService: () => ConseilService;
  @Inject('alertService') private alertService: () => AlertService;

  public conseil: IConseil = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.conseilId) {
        vm.retrieveConseil(to.params.conseilId);
      }
    });
  }

  public retrieveConseil(conseilId) {
    this.conseilService()
      .find(conseilId)
      .then(res => {
        this.conseil = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
