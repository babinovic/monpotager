package eu.adanet.monpotager.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import eu.adanet.monpotager.IntegrationTest;
import eu.adanet.monpotager.domain.Plante;
import eu.adanet.monpotager.domain.enumeration.categoriePlante;
import eu.adanet.monpotager.domain.enumeration.exposition;
import eu.adanet.monpotager.domain.enumeration.frequenceArrosage;
import eu.adanet.monpotager.domain.enumeration.Mois;
import eu.adanet.monpotager.domain.enumeration.typePlante;
import eu.adanet.monpotager.repository.PlanteRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PlanteResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PlanteResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_VARIETE = "AAAAAAAAAA";
    private static final String UPDATED_VARIETE = "BBBBBBBBBB";

    private static final typePlante DEFAULT_TYPE = typePlante.LEGUME;
    private static final typePlante UPDATED_TYPE = typePlante.FRUIT;

    private static final Float DEFAULT_ESPACEMENT = 1F;
    private static final Float UPDATED_ESPACEMENT = 2F;

    private static final Float DEFAULT_ESPACEMENT_RANG = 1F;
    private static final Float UPDATED_ESPACEMENT_RANG = 2F;

    private static final exposition DEFAULT_ENSOLEILLEMENT = exposition.OMBRE;
    private static final exposition UPDATED_ENSOLEILLEMENT = exposition.MI_OMBRE;

    private static final frequenceArrosage DEFAULT_ARROSAGE = frequenceArrosage.SANS;
    private static final frequenceArrosage UPDATED_ARROSAGE = frequenceArrosage.LEGER;

    private static final Mois DEFAULT_DEBUT_PLANTATION = Mois.JANVIER;
    private static final Mois UPDATED_DEBUT_PLANTATION = Mois.FEVRIER;

    private static final Mois DEFAULT_FIN_PLANTATION = Mois.JANVIER;
    private static final Mois UPDATED_FIN_PLANTATION = Mois.FEVRIER;

    private static final Mois DEFAULT_DEBUT_RECOLTE = Mois.JANVIER;
    private static final Mois UPDATED_DEBUT_RECOLTE = Mois.FEVRIER;

    private static final Mois DEFAULT_FIN_RECOLTE = Mois.JANVIER;
    private static final Mois UPDATED_FIN_RECOLTE = Mois.FEVRIER;

    private static final Mois DEFAULT_DEBUT_SEMIS = Mois.JANVIER;
    private static final Mois UPDATED_DEBUT_SEMIS = Mois.FEVRIER;

    private static final Mois DEFAULT_FIN_SEMIS = Mois.JANVIER;
    private static final Mois UPDATED_FIN_SEMIS = Mois.FEVRIER;

    private static final categoriePlante DEFAULT_CATEGORIE = categoriePlante.PLANT;
    private static final categoriePlante UPDATED_CATEGORIE = categoriePlante.SEMIS;

    private static final String DEFAULT_IMAGE = "AAAAAAAAAA";
    private static final String UPDATED_IMAGE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/plantes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PlanteRepository planteRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPlanteMockMvc;

    private Plante plante;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Plante createEntity(EntityManager em) {
        Plante plante = new Plante()
            .nom(DEFAULT_NOM)
            .variete(DEFAULT_VARIETE)
            .type(DEFAULT_TYPE)
            .espacement(DEFAULT_ESPACEMENT)
            .espacementRang(DEFAULT_ESPACEMENT_RANG)
            .ensoleillement(DEFAULT_ENSOLEILLEMENT)
            .arrosage(DEFAULT_ARROSAGE)
            .debutPlantation(DEFAULT_DEBUT_PLANTATION)
            .finPlantation(DEFAULT_FIN_PLANTATION)
            .debutRecolte(DEFAULT_DEBUT_RECOLTE)
            .finRecolte(DEFAULT_FIN_RECOLTE)
            .debutSemis(DEFAULT_DEBUT_SEMIS)
            .finSemis(DEFAULT_FIN_SEMIS)
            .categorie(DEFAULT_CATEGORIE)
            .image(DEFAULT_IMAGE);
        return plante;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Plante createUpdatedEntity(EntityManager em) {
        Plante plante = new Plante()
            .nom(UPDATED_NOM)
            .variete(UPDATED_VARIETE)
            .type(UPDATED_TYPE)
            .espacement(UPDATED_ESPACEMENT)
            .espacementRang(UPDATED_ESPACEMENT_RANG)
            .ensoleillement(UPDATED_ENSOLEILLEMENT)
            .arrosage(UPDATED_ARROSAGE)
            .debutPlantation(UPDATED_DEBUT_PLANTATION)
            .finPlantation(UPDATED_FIN_PLANTATION)
            .debutRecolte(UPDATED_DEBUT_RECOLTE)
            .finRecolte(UPDATED_FIN_RECOLTE)
            .debutSemis(UPDATED_DEBUT_SEMIS)
            .finSemis(UPDATED_FIN_SEMIS)
            .categorie(UPDATED_CATEGORIE)
            .image(UPDATED_IMAGE);
        return plante;
    }

    @BeforeEach
    public void initTest() {
        plante = createEntity(em);
    }

    @Test
    @Transactional
    void createPlante() throws Exception {
        int databaseSizeBeforeCreate = planteRepository.findAll().size();
        // Create the Plante
        restPlanteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(plante)))
            .andExpect(status().isCreated());

        // Validate the Plante in the database
        List<Plante> planteList = planteRepository.findAll();
        assertThat(planteList).hasSize(databaseSizeBeforeCreate + 1);
        Plante testPlante = planteList.get(planteList.size() - 1);
        assertThat(testPlante.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testPlante.getVariete()).isEqualTo(DEFAULT_VARIETE);
        assertThat(testPlante.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testPlante.getEspacement()).isEqualTo(DEFAULT_ESPACEMENT);
        assertThat(testPlante.getEspacementRang()).isEqualTo(DEFAULT_ESPACEMENT_RANG);
        assertThat(testPlante.getEnsoleillement()).isEqualTo(DEFAULT_ENSOLEILLEMENT);
        assertThat(testPlante.getArrosage()).isEqualTo(DEFAULT_ARROSAGE);
        assertThat(testPlante.getDebutPlantation()).isEqualTo(DEFAULT_DEBUT_PLANTATION);
        assertThat(testPlante.getFinPlantation()).isEqualTo(DEFAULT_FIN_PLANTATION);
        assertThat(testPlante.getDebutRecolte()).isEqualTo(DEFAULT_DEBUT_RECOLTE);
        assertThat(testPlante.getFinRecolte()).isEqualTo(DEFAULT_FIN_RECOLTE);
        assertThat(testPlante.getDebutSemis()).isEqualTo(DEFAULT_DEBUT_SEMIS);
        assertThat(testPlante.getFinSemis()).isEqualTo(DEFAULT_FIN_SEMIS);
        assertThat(testPlante.getCategorie()).isEqualTo(DEFAULT_CATEGORIE);
        assertThat(testPlante.getImage()).isEqualTo(DEFAULT_IMAGE);
    }

    @Test
    @Transactional
    void createPlanteWithExistingId() throws Exception {
        // Create the Plante with an existing ID
        plante.setId(1L);

        int databaseSizeBeforeCreate = planteRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPlanteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(plante)))
            .andExpect(status().isBadRequest());

        // Validate the Plante in the database
        List<Plante> planteList = planteRepository.findAll();
        assertThat(planteList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllPlantes() throws Exception {
        // Initialize the database
        planteRepository.saveAndFlush(plante);

        // Get all the planteList
        restPlanteMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(plante.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].variete").value(hasItem(DEFAULT_VARIETE)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].espacement").value(hasItem(DEFAULT_ESPACEMENT.doubleValue())))
            .andExpect(jsonPath("$.[*].espacementRang").value(hasItem(DEFAULT_ESPACEMENT_RANG.doubleValue())))
            .andExpect(jsonPath("$.[*].ensoleillement").value(hasItem(DEFAULT_ENSOLEILLEMENT.toString())))
            .andExpect(jsonPath("$.[*].arrosage").value(hasItem(DEFAULT_ARROSAGE.toString())))
            .andExpect(jsonPath("$.[*].debutPlantation").value(hasItem(DEFAULT_DEBUT_PLANTATION.toString())))
            .andExpect(jsonPath("$.[*].finPlantation").value(hasItem(DEFAULT_FIN_PLANTATION.toString())))
            .andExpect(jsonPath("$.[*].debutRecolte").value(hasItem(DEFAULT_DEBUT_RECOLTE.toString())))
            .andExpect(jsonPath("$.[*].finRecolte").value(hasItem(DEFAULT_FIN_RECOLTE.toString())))
            .andExpect(jsonPath("$.[*].debutSemis").value(hasItem(DEFAULT_DEBUT_SEMIS.toString())))
            .andExpect(jsonPath("$.[*].finSemis").value(hasItem(DEFAULT_FIN_SEMIS.toString())))
            .andExpect(jsonPath("$.[*].categorie").value(hasItem(DEFAULT_CATEGORIE.toString())))
            .andExpect(jsonPath("$.[*].image").value(hasItem(DEFAULT_IMAGE)));
    }

    @Test
    @Transactional
    void getPlante() throws Exception {
        // Initialize the database
        planteRepository.saveAndFlush(plante);

        // Get the plante
        restPlanteMockMvc
            .perform(get(ENTITY_API_URL_ID, plante.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(plante.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.variete").value(DEFAULT_VARIETE))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.espacement").value(DEFAULT_ESPACEMENT.doubleValue()))
            .andExpect(jsonPath("$.espacementRang").value(DEFAULT_ESPACEMENT_RANG.doubleValue()))
            .andExpect(jsonPath("$.ensoleillement").value(DEFAULT_ENSOLEILLEMENT.toString()))
            .andExpect(jsonPath("$.arrosage").value(DEFAULT_ARROSAGE.toString()))
            .andExpect(jsonPath("$.debutPlantation").value(DEFAULT_DEBUT_PLANTATION.toString()))
            .andExpect(jsonPath("$.finPlantation").value(DEFAULT_FIN_PLANTATION.toString()))
            .andExpect(jsonPath("$.debutRecolte").value(DEFAULT_DEBUT_RECOLTE.toString()))
            .andExpect(jsonPath("$.finRecolte").value(DEFAULT_FIN_RECOLTE.toString()))
            .andExpect(jsonPath("$.debutSemis").value(DEFAULT_DEBUT_SEMIS.toString()))
            .andExpect(jsonPath("$.finSemis").value(DEFAULT_FIN_SEMIS.toString()))
            .andExpect(jsonPath("$.categorie").value(DEFAULT_CATEGORIE.toString()))
            .andExpect(jsonPath("$.image").value(DEFAULT_IMAGE));
    }

    @Test
    @Transactional
    void getNonExistingPlante() throws Exception {
        // Get the plante
        restPlanteMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingPlante() throws Exception {
        // Initialize the database
        planteRepository.saveAndFlush(plante);

        int databaseSizeBeforeUpdate = planteRepository.findAll().size();

        // Update the plante
        Plante updatedPlante = planteRepository.findById(plante.getId()).get();
        // Disconnect from session so that the updates on updatedPlante are not directly saved in db
        em.detach(updatedPlante);
        updatedPlante
            .nom(UPDATED_NOM)
            .variete(UPDATED_VARIETE)
            .type(UPDATED_TYPE)
            .espacement(UPDATED_ESPACEMENT)
            .espacementRang(UPDATED_ESPACEMENT_RANG)
            .ensoleillement(UPDATED_ENSOLEILLEMENT)
            .arrosage(UPDATED_ARROSAGE)
            .debutPlantation(UPDATED_DEBUT_PLANTATION)
            .finPlantation(UPDATED_FIN_PLANTATION)
            .debutRecolte(UPDATED_DEBUT_RECOLTE)
            .finRecolte(UPDATED_FIN_RECOLTE)
            .debutSemis(UPDATED_DEBUT_SEMIS)
            .finSemis(UPDATED_FIN_SEMIS)
            .categorie(UPDATED_CATEGORIE)
            .image(UPDATED_IMAGE);

        restPlanteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedPlante.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedPlante))
            )
            .andExpect(status().isOk());

        // Validate the Plante in the database
        List<Plante> planteList = planteRepository.findAll();
        assertThat(planteList).hasSize(databaseSizeBeforeUpdate);
        Plante testPlante = planteList.get(planteList.size() - 1);
        assertThat(testPlante.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testPlante.getVariete()).isEqualTo(UPDATED_VARIETE);
        assertThat(testPlante.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testPlante.getEspacement()).isEqualTo(UPDATED_ESPACEMENT);
        assertThat(testPlante.getEspacementRang()).isEqualTo(UPDATED_ESPACEMENT_RANG);
        assertThat(testPlante.getEnsoleillement()).isEqualTo(UPDATED_ENSOLEILLEMENT);
        assertThat(testPlante.getArrosage()).isEqualTo(UPDATED_ARROSAGE);
        assertThat(testPlante.getDebutPlantation()).isEqualTo(UPDATED_DEBUT_PLANTATION);
        assertThat(testPlante.getFinPlantation()).isEqualTo(UPDATED_FIN_PLANTATION);
        assertThat(testPlante.getDebutRecolte()).isEqualTo(UPDATED_DEBUT_RECOLTE);
        assertThat(testPlante.getFinRecolte()).isEqualTo(UPDATED_FIN_RECOLTE);
        assertThat(testPlante.getDebutSemis()).isEqualTo(UPDATED_DEBUT_SEMIS);
        assertThat(testPlante.getFinSemis()).isEqualTo(UPDATED_FIN_SEMIS);
        assertThat(testPlante.getCategorie()).isEqualTo(UPDATED_CATEGORIE);
        assertThat(testPlante.getImage()).isEqualTo(UPDATED_IMAGE);
    }

    @Test
    @Transactional
    void putNonExistingPlante() throws Exception {
        int databaseSizeBeforeUpdate = planteRepository.findAll().size();
        plante.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPlanteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, plante.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(plante))
            )
            .andExpect(status().isBadRequest());

        // Validate the Plante in the database
        List<Plante> planteList = planteRepository.findAll();
        assertThat(planteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPlante() throws Exception {
        int databaseSizeBeforeUpdate = planteRepository.findAll().size();
        plante.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPlanteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(plante))
            )
            .andExpect(status().isBadRequest());

        // Validate the Plante in the database
        List<Plante> planteList = planteRepository.findAll();
        assertThat(planteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPlante() throws Exception {
        int databaseSizeBeforeUpdate = planteRepository.findAll().size();
        plante.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPlanteMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(plante)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Plante in the database
        List<Plante> planteList = planteRepository.findAll();
        assertThat(planteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePlanteWithPatch() throws Exception {
        // Initialize the database
        planteRepository.saveAndFlush(plante);

        int databaseSizeBeforeUpdate = planteRepository.findAll().size();

        // Update the plante using partial update
        Plante partialUpdatedPlante = new Plante();
        partialUpdatedPlante.setId(plante.getId());

        partialUpdatedPlante
            .type(UPDATED_TYPE)
            .espacementRang(UPDATED_ESPACEMENT_RANG)
            .ensoleillement(UPDATED_ENSOLEILLEMENT)
            .arrosage(UPDATED_ARROSAGE)
            .debutPlantation(UPDATED_DEBUT_PLANTATION)
            .debutSemis(UPDATED_DEBUT_SEMIS)
            .finSemis(UPDATED_FIN_SEMIS);

        restPlanteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPlante.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPlante))
            )
            .andExpect(status().isOk());

        // Validate the Plante in the database
        List<Plante> planteList = planteRepository.findAll();
        assertThat(planteList).hasSize(databaseSizeBeforeUpdate);
        Plante testPlante = planteList.get(planteList.size() - 1);
        assertThat(testPlante.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testPlante.getVariete()).isEqualTo(DEFAULT_VARIETE);
        assertThat(testPlante.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testPlante.getEspacement()).isEqualTo(DEFAULT_ESPACEMENT);
        assertThat(testPlante.getEspacementRang()).isEqualTo(UPDATED_ESPACEMENT_RANG);
        assertThat(testPlante.getEnsoleillement()).isEqualTo(UPDATED_ENSOLEILLEMENT);
        assertThat(testPlante.getArrosage()).isEqualTo(UPDATED_ARROSAGE);
        assertThat(testPlante.getDebutPlantation()).isEqualTo(UPDATED_DEBUT_PLANTATION);
        assertThat(testPlante.getFinPlantation()).isEqualTo(DEFAULT_FIN_PLANTATION);
        assertThat(testPlante.getDebutRecolte()).isEqualTo(DEFAULT_DEBUT_RECOLTE);
        assertThat(testPlante.getFinRecolte()).isEqualTo(DEFAULT_FIN_RECOLTE);
        assertThat(testPlante.getDebutSemis()).isEqualTo(UPDATED_DEBUT_SEMIS);
        assertThat(testPlante.getFinSemis()).isEqualTo(UPDATED_FIN_SEMIS);
        assertThat(testPlante.getCategorie()).isEqualTo(DEFAULT_CATEGORIE);
        assertThat(testPlante.getImage()).isEqualTo(DEFAULT_IMAGE);
    }

    @Test
    @Transactional
    void fullUpdatePlanteWithPatch() throws Exception {
        // Initialize the database
        planteRepository.saveAndFlush(plante);

        int databaseSizeBeforeUpdate = planteRepository.findAll().size();

        // Update the plante using partial update
        Plante partialUpdatedPlante = new Plante();
        partialUpdatedPlante.setId(plante.getId());

        partialUpdatedPlante
            .nom(UPDATED_NOM)
            .variete(UPDATED_VARIETE)
            .type(UPDATED_TYPE)
            .espacement(UPDATED_ESPACEMENT)
            .espacementRang(UPDATED_ESPACEMENT_RANG)
            .ensoleillement(UPDATED_ENSOLEILLEMENT)
            .arrosage(UPDATED_ARROSAGE)
            .debutPlantation(UPDATED_DEBUT_PLANTATION)
            .finPlantation(UPDATED_FIN_PLANTATION)
            .debutRecolte(UPDATED_DEBUT_RECOLTE)
            .finRecolte(UPDATED_FIN_RECOLTE)
            .debutSemis(UPDATED_DEBUT_SEMIS)
            .finSemis(UPDATED_FIN_SEMIS)
            .categorie(UPDATED_CATEGORIE)
            .image(UPDATED_IMAGE);

        restPlanteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPlante.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPlante))
            )
            .andExpect(status().isOk());

        // Validate the Plante in the database
        List<Plante> planteList = planteRepository.findAll();
        assertThat(planteList).hasSize(databaseSizeBeforeUpdate);
        Plante testPlante = planteList.get(planteList.size() - 1);
        assertThat(testPlante.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testPlante.getVariete()).isEqualTo(UPDATED_VARIETE);
        assertThat(testPlante.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testPlante.getEspacement()).isEqualTo(UPDATED_ESPACEMENT);
        assertThat(testPlante.getEspacementRang()).isEqualTo(UPDATED_ESPACEMENT_RANG);
        assertThat(testPlante.getEnsoleillement()).isEqualTo(UPDATED_ENSOLEILLEMENT);
        assertThat(testPlante.getArrosage()).isEqualTo(UPDATED_ARROSAGE);
        assertThat(testPlante.getDebutPlantation()).isEqualTo(UPDATED_DEBUT_PLANTATION);
        assertThat(testPlante.getFinPlantation()).isEqualTo(UPDATED_FIN_PLANTATION);
        assertThat(testPlante.getDebutRecolte()).isEqualTo(UPDATED_DEBUT_RECOLTE);
        assertThat(testPlante.getFinRecolte()).isEqualTo(UPDATED_FIN_RECOLTE);
        assertThat(testPlante.getDebutSemis()).isEqualTo(UPDATED_DEBUT_SEMIS);
        assertThat(testPlante.getFinSemis()).isEqualTo(UPDATED_FIN_SEMIS);
        assertThat(testPlante.getCategorie()).isEqualTo(UPDATED_CATEGORIE);
        assertThat(testPlante.getImage()).isEqualTo(UPDATED_IMAGE);
    }

    @Test
    @Transactional
    void patchNonExistingPlante() throws Exception {
        int databaseSizeBeforeUpdate = planteRepository.findAll().size();
        plante.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPlanteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, plante.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(plante))
            )
            .andExpect(status().isBadRequest());

        // Validate the Plante in the database
        List<Plante> planteList = planteRepository.findAll();
        assertThat(planteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPlante() throws Exception {
        int databaseSizeBeforeUpdate = planteRepository.findAll().size();
        plante.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPlanteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(plante))
            )
            .andExpect(status().isBadRequest());

        // Validate the Plante in the database
        List<Plante> planteList = planteRepository.findAll();
        assertThat(planteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPlante() throws Exception {
        int databaseSizeBeforeUpdate = planteRepository.findAll().size();
        plante.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPlanteMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(plante)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Plante in the database
        List<Plante> planteList = planteRepository.findAll();
        assertThat(planteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePlante() throws Exception {
        // Initialize the database
        planteRepository.saveAndFlush(plante);

        int databaseSizeBeforeDelete = planteRepository.findAll().size();

        // Delete the plante
        restPlanteMockMvc
            .perform(delete(ENTITY_API_URL_ID, plante.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Plante> planteList = planteRepository.findAll();
        assertThat(planteList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
