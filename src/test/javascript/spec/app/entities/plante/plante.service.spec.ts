/* tslint:disable max-line-length */
import axios from 'axios';
import sinon from 'sinon';

import PlanteService from '@/entities/plante/plante.service';
import { Plante } from '@/shared/model/plante.model';
import { typePlante } from '@/shared/model/enumerations/type-plante.model';
import { exposition } from '@/shared/model/enumerations/exposition.model';
import { frequenceArrosage } from '@/shared/model/enumerations/frequence-arrosage.model';
import { mois } from '@/shared/model/enumerations/mois.model';
import { categoriePlante } from '@/shared/model/enumerations/categorie-plante.model';

const error = {
  response: {
    status: null,
    data: {
      type: null,
    },
  },
};

const axiosStub = {
  get: sinon.stub(axios, 'get'),
  post: sinon.stub(axios, 'post'),
  put: sinon.stub(axios, 'put'),
  patch: sinon.stub(axios, 'patch'),
  delete: sinon.stub(axios, 'delete'),
};

describe('Service Tests', () => {
  describe('Plante Service', () => {
    let service: PlanteService;
    let elemDefault;

    beforeEach(() => {
      service = new PlanteService();
      elemDefault = new Plante(
        123,
        'AAAAAAA',
        'AAAAAAA',
        typePlante.LEGUME,
        0,
        0,
        exposition.OMBRE,
        frequenceArrosage.SANS,
        mois.JANVIER,
        mois.JANVIER,
        mois.JANVIER,
        mois.JANVIER,
        mois.JANVIER,
        mois.JANVIER,
        categoriePlante.PLANT,
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign({}, elemDefault);
        axiosStub.get.resolves({ data: returnedFromService });

        return service.find(123).then(res => {
          expect(res).toMatchObject(elemDefault);
        });
      });

      it('should not find an element', async () => {
        axiosStub.get.rejects(error);
        return service
          .find(123)
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should create a Plante', async () => {
        const returnedFromService = Object.assign(
          {
            id: 123,
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);

        axiosStub.post.resolves({ data: returnedFromService });
        return service.create({}).then(res => {
          expect(res).toMatchObject(expected);
        });
      });

      it('should not create a Plante', async () => {
        axiosStub.post.rejects(error);

        return service
          .create({})
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should update a Plante', async () => {
        const returnedFromService = Object.assign(
          {
            nom: 'BBBBBB',
            variete: 'BBBBBB',
            type: 'BBBBBB',
            espacement: 1,
            espacementRang: 1,
            ensoleillement: 'BBBBBB',
            arrosage: 'BBBBBB',
            debutPlantation: 'BBBBBB',
            finPlantation: 'BBBBBB',
            debutRecolte: 'BBBBBB',
            finRecolte: 'BBBBBB',
            debutSemis: 'BBBBBB',
            finSemis: 'BBBBBB',
            categorie: 'BBBBBB',
            image: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);
        axiosStub.put.resolves({ data: returnedFromService });

        return service.update(expected).then(res => {
          expect(res).toMatchObject(expected);
        });
      });

      it('should not update a Plante', async () => {
        axiosStub.put.rejects(error);

        return service
          .update({})
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should partial update a Plante', async () => {
        const patchObject = Object.assign(
          {
            nom: 'BBBBBB',
            type: 'BBBBBB',
            espacement: 1,
            espacementRang: 1,
            ensoleillement: 'BBBBBB',
            arrosage: 'BBBBBB',
            finPlantation: 'BBBBBB',
            finSemis: 'BBBBBB',
            image: 'BBBBBB',
          },
          new Plante()
        );
        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);
        axiosStub.patch.resolves({ data: returnedFromService });

        return service.partialUpdate(patchObject).then(res => {
          expect(res).toMatchObject(expected);
        });
      });

      it('should not partial update a Plante', async () => {
        axiosStub.patch.rejects(error);

        return service
          .partialUpdate({})
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should return a list of Plante', async () => {
        const returnedFromService = Object.assign(
          {
            nom: 'BBBBBB',
            variete: 'BBBBBB',
            type: 'BBBBBB',
            espacement: 1,
            espacementRang: 1,
            ensoleillement: 'BBBBBB',
            arrosage: 'BBBBBB',
            debutPlantation: 'BBBBBB',
            finPlantation: 'BBBBBB',
            debutRecolte: 'BBBBBB',
            finRecolte: 'BBBBBB',
            debutSemis: 'BBBBBB',
            finSemis: 'BBBBBB',
            categorie: 'BBBBBB',
            image: 'BBBBBB',
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        axiosStub.get.resolves([returnedFromService]);
        return service.retrieve({ sort: {}, page: 0, size: 10 }).then(res => {
          expect(res).toContainEqual(expected);
        });
      });

      it('should not return a list of Plante', async () => {
        axiosStub.get.rejects(error);

        return service
          .retrieve()
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });

      it('should delete a Plante', async () => {
        axiosStub.delete.resolves({ ok: true });
        return service.delete(123).then(res => {
          expect(res.ok).toBeTruthy();
        });
      });

      it('should not delete a Plante', async () => {
        axiosStub.delete.rejects(error);

        return service
          .delete(123)
          .then()
          .catch(err => {
            expect(err).toMatchObject(error);
          });
      });
    });
  });
});
