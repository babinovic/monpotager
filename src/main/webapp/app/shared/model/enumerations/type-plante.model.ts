export enum typePlante {
  LEGUME = 'LEGUME',

  FRUIT = 'FRUIT',

  AROMATIQUE = 'AROMATIQUE',

  FLEURS = 'FLEURS',

  ARBUSTRE = 'ARBUSTRE',
}
