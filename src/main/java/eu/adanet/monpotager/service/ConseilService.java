package eu.adanet.monpotager.service;

import eu.adanet.monpotager.domain.Conseil;
import eu.adanet.monpotager.domain.Tache;
import eu.adanet.monpotager.repository.ConseilRepository;
import eu.adanet.monpotager.repository.TacheRepository;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Conseil}.
 */
@Service
@Transactional
public class ConseilService {

    private final Logger log = LoggerFactory.getLogger(ConseilService.class);

    private final ConseilRepository conseilRepository;
    private final TacheRepository tacheRepository;

    public ConseilService(ConseilRepository conseilRepository, TacheRepository tacheRepository) {
        this.conseilRepository = conseilRepository;
        this.tacheRepository = tacheRepository;
    }

    /**
     * Save a conseil.
     *
     * @param conseil the entity to save.
     * @return the persisted entity.
     */
    public Conseil save(Conseil conseil) {
        log.debug("Request to save Conseil : {}", conseil);
        return conseilRepository.save(conseil);
    }

    /**
     * Update a conseil.
     *
     * @param conseil the entity to save.
     * @return the persisted entity.
     */
    public Conseil update(Conseil conseil) {
        log.debug("Request to update Conseil : {}", conseil);
        return conseilRepository.save(conseil);
    }

    /**
     * Partially update a conseil.
     *
     * @param conseil the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Conseil> partialUpdate(Conseil conseil) {
        log.debug("Request to partially update Conseil : {}", conseil);

        return conseilRepository
            .findById(conseil.getId())
            .map(existingConseil -> {
                if (conseil.getDateDebutPeriode() != null) {
                    existingConseil.setDateDebutPeriode(conseil.getDateDebutPeriode());
                }
                if (conseil.getDateFinPeriode() != null) {
                    existingConseil.setDateFinPeriode(conseil.getDateFinPeriode());
                }

                return existingConseil;
            })
            .map(conseilRepository::save);
    }

    /**
     * Get all the conseils.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Conseil> findAll() {
        log.debug("Request to get all Conseils");
        return conseilRepository.findAll();
    }

    /**
     * Get one conseil by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Conseil> findOne(Long id) {
        log.debug("Request to get Conseil : {}", id);
        return conseilRepository.findById(id);
    }

    /**
     * Delete the conseil by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Conseil : {}", id);
         // cascade delete taches
         for (Tache tache : tacheRepository.findAllWithEagerRelationships()) {
            if (tache.getConseil().getId().longValue() == id) {
                tacheRepository.delete(tache);
            }           
        }
        conseilRepository.deleteById(id);
    }


    /**
     * Get dernier conseil non traité
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Conseil> getDernierConseil(Long id) {
        log.debug("Request to get Conseil : {}", id);
        return conseilRepository.findById(id);
    }
    
}
