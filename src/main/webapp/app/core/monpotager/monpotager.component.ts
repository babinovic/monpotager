import PotagerService from '@/entities/potager/potager.service';
import { IPotager } from '@/shared/model/potager.model';
import { IParcelle } from '@/shared/model/parcelle.model';
import ParcelleService from '@/entities/parcelle/parcelle.service';
import AlertService from '@/shared/alert/alert.service';
import TacheService from '@/entities/tache/tache.service';
import { ITache } from '@/shared/model/tache.model';
import { Etat } from '@/shared/model/enumerations/etat.model';

export default {  

  data() {
    return {
      potagerActif: null,
      potagers: [] as IPotager[],
      parcelles: [] as IParcelle[],
      allParcelles: [] as IParcelle[],
      taches: [] as ITache[],
      allTaches: [] as ITache[],
      propOrder: 'id',
      potagerService: new PotagerService(),
      parcelleService: new ParcelleService(),
      tacheService: new TacheService(),
      alertService: new AlertService(),
      fieldsParcelle: ['nom', 'longueur', 'largeur', 'ensoleillement'],
      fieldsTache: ['nom','type','actions']
    };
  },

  mounted() {
    this.getPotagers();
    this.getParcelles();
    this.getTaches();
  },

  methods: {
    // récupère liste des potagers
    getPotagers(): void {
      this.potagerService
        .retrieve()
        .then((response: {data: IPotager[];}) => {
          this.potagers = response.data; 
          this.potagerActif = response.data.length > 0 ? response.data[0].id : null;
        })
        .catch((error: { response: { toString: () => any; }; }) => {
          this.alertService.showError(this, error.response.toString());
        }
      )
    },

    // quand on change le potager actif
    changePotagerActif(): void {
      if (this.potagerActif && this.allParcelles && this.allTaches) {
        this.parcelles = this.filterParcelles();
        this.taches = this.filterTaches();
      }
    },

    // récupère liste des parcelles du potager choisi 
    getParcelles(): void {
      const paginationQuery = {
        page: -1,
        size: 1000,
        sort: this.sort()
      };

      this.parcelleService
        .retrieve(paginationQuery)
        .then((response: {data: IParcelle[];}) => {
          this.allParcelles = response.data;
          this.parcelles = this.filterParcelles();
        })
        .catch((error: { response: any; }) => {
          this.alertService.showError(this, error.response);
        });
    },

    // filtrer en fonction du potager actif
    filterParcelles(): IParcelle[] {
      let parcelles: IParcelle[] = [];
      this.allParcelles.forEach((item: IParcelle) => {
        if (item.potager.id == this.potagerActif) {
          parcelles.push(item);
        }
      });
      return parcelles;
    },

    // ouvre la page de détails d'une parcelle
    ouvreParcelle(item: IParcelle): void {
      this.$router.push({ name: 'MaParcelle', params: { parcelleId: item.id } });
    },

    // récupère liste des taches du potager choisi 
    getTaches(): void {
      const paginationQuery = {
        page: -1,
        size: 1000,
        sort: this.sort()
      };

      this.tacheService
        .retrieve(paginationQuery)
        .then((response: {data: ITache[];}) => {
          this.allTaches = response.data;
          this.taches = this.filterTaches();
        })
        .catch((error: { response: any; }) => {
          this.alertService.showError(this, error.response);
        });
    },

    // filtrer en fonction du potager actif
    filterTaches(): ITache[] {
      let taches: ITache[] = [];
      this.allTaches.forEach((item: ITache) => {
        if (item.potager && item.potager.id == this.potagerActif && item.etat == Etat.ACTIVE) {
          taches.push(item);
        }
      });
      return taches;
    },

    ackTache(tache: ITache): void {
      tache.etat = Etat.LUE;
      this.tacheService
      .partialUpdate(tache)
      .then((response: {data: ITache[];}) => {
        this.getTaches();
      })
      .catch((error: { response: any; }) => {
        this.alertService.showError(this, error.response);
      });
    },

    sort(): Array<any> {
      const result = [this.propOrder + ',asc'];
      if (this.propOrder !== 'id') {
        result.push('id');
      }
      return result;
    }
  }
}
