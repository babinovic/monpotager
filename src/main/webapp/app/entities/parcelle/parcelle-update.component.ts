import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import PotagerService from '@/entities/potager/potager.service';
import { IPotager } from '@/shared/model/potager.model';

import { IParcelle, Parcelle } from '@/shared/model/parcelle.model';
import ParcelleService from './parcelle.service';
import { exposition } from '@/shared/model/enumerations/exposition.model';

const validations: any = {
  parcelle: {
    nom: {},
    longueur: {},
    largeur: {},
    ensoleillement: {},
    nbPlantationLongueur: {},
    nbPlantationLargeur: {},
  },
};

@Component({
  validations,
})
export default class ParcelleUpdate extends Vue {
  @Inject('parcelleService') private parcelleService: () => ParcelleService;
  @Inject('alertService') private alertService: () => AlertService;

  public parcelle: IParcelle = new Parcelle();

  @Inject('potagerService') private potagerService: () => PotagerService;

  public potagers: IPotager[] = [];
  public expositionValues: string[] = Object.keys(exposition);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.parcelleId) {
        vm.retrieveParcelle(to.params.parcelleId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.parcelle.id) {
      this.parcelleService()
        .update(this.parcelle)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('monpotagerApp.parcelle.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.parcelleService()
        .create(this.parcelle)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('monpotagerApp.parcelle.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveParcelle(parcelleId): void {
    this.parcelleService()
      .find(parcelleId)
      .then(res => {
        this.parcelle = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.potagerService()
      .retrieve()
      .then(res => {
        this.potagers = res.data;
      });
  }
}
