package eu.adanet.monpotager.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link eu.adanet.monpotager.domain.Culture} entity. This class is used
 * in {@link eu.adanet.monpotager.web.rest.CultureResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /cultures?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CultureCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter nom;

    private LocalDateFilter dateDebut;

    private LocalDateFilter dateFin;

    private LocalDateFilter dateRecolte;

    private LongFilter plantationId;

    private LongFilter planteId;

    private Boolean distinct;

    public CultureCriteria() {}

    public CultureCriteria(CultureCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.nom = other.nom == null ? null : other.nom.copy();
        this.dateDebut = other.dateDebut == null ? null : other.dateDebut.copy();
        this.dateFin = other.dateFin == null ? null : other.dateFin.copy();
        this.dateRecolte = other.dateRecolte == null ? null : other.dateRecolte.copy();
        this.plantationId = other.plantationId == null ? null : other.plantationId.copy();
        this.planteId = other.planteId == null ? null : other.planteId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public CultureCriteria copy() {
        return new CultureCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNom() {
        return nom;
    }

    public StringFilter nom() {
        if (nom == null) {
            nom = new StringFilter();
        }
        return nom;
    }

    public void setNom(StringFilter nom) {
        this.nom = nom;
    }

    public LocalDateFilter getDateDebut() {
        return dateDebut;
    }

    public LocalDateFilter dateDebut() {
        if (dateDebut == null) {
            dateDebut = new LocalDateFilter();
        }
        return dateDebut;
    }

    public void setDateDebut(LocalDateFilter dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDateFilter getDateFin() {
        return dateFin;
    }

    public LocalDateFilter dateFin() {
        if (dateFin == null) {
            dateFin = new LocalDateFilter();
        }
        return dateFin;
    }

    public void setDateFin(LocalDateFilter dateFin) {
        this.dateFin = dateFin;
    }

    public LocalDateFilter getDateRecolte() {
        return dateRecolte;
    }

    public LocalDateFilter dateRecolte() {
        if (dateRecolte == null) {
            dateRecolte = new LocalDateFilter();
        }
        return dateRecolte;
    }

    public void setDateRecolte(LocalDateFilter dateRecolte) {
        this.dateRecolte = dateRecolte;
    }

    public LongFilter getPlantationId() {
        return plantationId;
    }

    public LongFilter plantationId() {
        if (plantationId == null) {
            plantationId = new LongFilter();
        }
        return plantationId;
    }

    public void setPlantationId(LongFilter plantationId) {
        this.plantationId = plantationId;
    }

    public LongFilter getPlanteId() {
        return planteId;
    }

    public LongFilter planteId() {
        if (planteId == null) {
            planteId = new LongFilter();
        }
        return planteId;
    }

    public void setPlanteId(LongFilter planteId) {
        this.planteId = planteId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CultureCriteria that = (CultureCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(nom, that.nom) &&
            Objects.equals(dateDebut, that.dateDebut) &&
            Objects.equals(dateFin, that.dateFin) &&
            Objects.equals(dateRecolte, that.dateRecolte) &&
            Objects.equals(plantationId, that.plantationId) &&
            Objects.equals(planteId, that.planteId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, dateDebut, dateFin, dateRecolte, plantationId, planteId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CultureCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (nom != null ? "nom=" + nom + ", " : "") +
            (dateDebut != null ? "dateDebut=" + dateDebut + ", " : "") +
            (dateFin != null ? "dateFin=" + dateFin + ", " : "") +
            (dateRecolte != null ? "dateRecolte=" + dateRecolte + ", " : "") +
            (plantationId != null ? "plantationId=" + plantationId + ", " : "") +
            (planteId != null ? "planteId=" + planteId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
