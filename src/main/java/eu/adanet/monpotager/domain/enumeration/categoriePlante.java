package eu.adanet.monpotager.domain.enumeration;

/**
 * The categoriePlante enumeration.
 */
public enum categoriePlante {
    PLANT,
    SEMIS,
}
