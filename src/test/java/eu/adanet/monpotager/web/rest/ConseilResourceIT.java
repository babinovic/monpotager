package eu.adanet.monpotager.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import eu.adanet.monpotager.IntegrationTest;
import eu.adanet.monpotager.domain.Conseil;
import eu.adanet.monpotager.repository.ConseilRepository;
import eu.adanet.monpotager.service.criteria.ConseilCriteria;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ConseilResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ConseilResourceIT {

    private static final LocalDate DEFAULT_DATE_DEBUT_PERIODE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_DEBUT_PERIODE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATE_DEBUT_PERIODE = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATE_FIN_PERIODE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_FIN_PERIODE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATE_FIN_PERIODE = LocalDate.ofEpochDay(-1L);

    private static final String ENTITY_API_URL = "/api/conseils";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ConseilRepository conseilRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restConseilMockMvc;

    private Conseil conseil;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Conseil createEntity(EntityManager em) {
        Conseil conseil = new Conseil().dateDebutPeriode(DEFAULT_DATE_DEBUT_PERIODE).dateFinPeriode(DEFAULT_DATE_FIN_PERIODE);
        return conseil;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Conseil createUpdatedEntity(EntityManager em) {
        Conseil conseil = new Conseil().dateDebutPeriode(UPDATED_DATE_DEBUT_PERIODE).dateFinPeriode(UPDATED_DATE_FIN_PERIODE);
        return conseil;
    }

    @BeforeEach
    public void initTest() {
        conseil = createEntity(em);
    }

    @Test
    @Transactional
    void createConseil() throws Exception {
        int databaseSizeBeforeCreate = conseilRepository.findAll().size();
        // Create the Conseil
        restConseilMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(conseil)))
            .andExpect(status().isCreated());

        // Validate the Conseil in the database
        List<Conseil> conseilList = conseilRepository.findAll();
        assertThat(conseilList).hasSize(databaseSizeBeforeCreate + 1);
        Conseil testConseil = conseilList.get(conseilList.size() - 1);
        assertThat(testConseil.getDateDebutPeriode()).isEqualTo(DEFAULT_DATE_DEBUT_PERIODE);
        assertThat(testConseil.getDateFinPeriode()).isEqualTo(DEFAULT_DATE_FIN_PERIODE);
    }

    @Test
    @Transactional
    void createConseilWithExistingId() throws Exception {
        // Create the Conseil with an existing ID
        conseil.setId(1L);

        int databaseSizeBeforeCreate = conseilRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restConseilMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(conseil)))
            .andExpect(status().isBadRequest());

        // Validate the Conseil in the database
        List<Conseil> conseilList = conseilRepository.findAll();
        assertThat(conseilList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllConseils() throws Exception {
        // Initialize the database
        conseilRepository.saveAndFlush(conseil);

        // Get all the conseilList
        restConseilMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(conseil.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateDebutPeriode").value(hasItem(DEFAULT_DATE_DEBUT_PERIODE.toString())))
            .andExpect(jsonPath("$.[*].dateFinPeriode").value(hasItem(DEFAULT_DATE_FIN_PERIODE.toString())));
    }

    @Test
    @Transactional
    void getConseil() throws Exception {
        // Initialize the database
        conseilRepository.saveAndFlush(conseil);

        // Get the conseil
        restConseilMockMvc
            .perform(get(ENTITY_API_URL_ID, conseil.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(conseil.getId().intValue()))
            .andExpect(jsonPath("$.dateDebutPeriode").value(DEFAULT_DATE_DEBUT_PERIODE.toString()))
            .andExpect(jsonPath("$.dateFinPeriode").value(DEFAULT_DATE_FIN_PERIODE.toString()));
    }

    @Test
    @Transactional
    void getConseilsByIdFiltering() throws Exception {
        // Initialize the database
        conseilRepository.saveAndFlush(conseil);

        Long id = conseil.getId();

        defaultConseilShouldBeFound("id.equals=" + id);
        defaultConseilShouldNotBeFound("id.notEquals=" + id);

        defaultConseilShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultConseilShouldNotBeFound("id.greaterThan=" + id);

        defaultConseilShouldBeFound("id.lessThanOrEqual=" + id);
        defaultConseilShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllConseilsByDateDebutPeriodeIsEqualToSomething() throws Exception {
        // Initialize the database
        conseilRepository.saveAndFlush(conseil);

        // Get all the conseilList where dateDebutPeriode equals to DEFAULT_DATE_DEBUT_PERIODE
        defaultConseilShouldBeFound("dateDebutPeriode.equals=" + DEFAULT_DATE_DEBUT_PERIODE);

        // Get all the conseilList where dateDebutPeriode equals to UPDATED_DATE_DEBUT_PERIODE
        defaultConseilShouldNotBeFound("dateDebutPeriode.equals=" + UPDATED_DATE_DEBUT_PERIODE);
    }

    @Test
    @Transactional
    void getAllConseilsByDateDebutPeriodeIsInShouldWork() throws Exception {
        // Initialize the database
        conseilRepository.saveAndFlush(conseil);

        // Get all the conseilList where dateDebutPeriode in DEFAULT_DATE_DEBUT_PERIODE or UPDATED_DATE_DEBUT_PERIODE
        defaultConseilShouldBeFound("dateDebutPeriode.in=" + DEFAULT_DATE_DEBUT_PERIODE + "," + UPDATED_DATE_DEBUT_PERIODE);

        // Get all the conseilList where dateDebutPeriode equals to UPDATED_DATE_DEBUT_PERIODE
        defaultConseilShouldNotBeFound("dateDebutPeriode.in=" + UPDATED_DATE_DEBUT_PERIODE);
    }

    @Test
    @Transactional
    void getAllConseilsByDateDebutPeriodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        conseilRepository.saveAndFlush(conseil);

        // Get all the conseilList where dateDebutPeriode is not null
        defaultConseilShouldBeFound("dateDebutPeriode.specified=true");

        // Get all the conseilList where dateDebutPeriode is null
        defaultConseilShouldNotBeFound("dateDebutPeriode.specified=false");
    }

    @Test
    @Transactional
    void getAllConseilsByDateDebutPeriodeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        conseilRepository.saveAndFlush(conseil);

        // Get all the conseilList where dateDebutPeriode is greater than or equal to DEFAULT_DATE_DEBUT_PERIODE
        defaultConseilShouldBeFound("dateDebutPeriode.greaterThanOrEqual=" + DEFAULT_DATE_DEBUT_PERIODE);

        // Get all the conseilList where dateDebutPeriode is greater than or equal to UPDATED_DATE_DEBUT_PERIODE
        defaultConseilShouldNotBeFound("dateDebutPeriode.greaterThanOrEqual=" + UPDATED_DATE_DEBUT_PERIODE);
    }

    @Test
    @Transactional
    void getAllConseilsByDateDebutPeriodeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        conseilRepository.saveAndFlush(conseil);

        // Get all the conseilList where dateDebutPeriode is less than or equal to DEFAULT_DATE_DEBUT_PERIODE
        defaultConseilShouldBeFound("dateDebutPeriode.lessThanOrEqual=" + DEFAULT_DATE_DEBUT_PERIODE);

        // Get all the conseilList where dateDebutPeriode is less than or equal to SMALLER_DATE_DEBUT_PERIODE
        defaultConseilShouldNotBeFound("dateDebutPeriode.lessThanOrEqual=" + SMALLER_DATE_DEBUT_PERIODE);
    }

    @Test
    @Transactional
    void getAllConseilsByDateDebutPeriodeIsLessThanSomething() throws Exception {
        // Initialize the database
        conseilRepository.saveAndFlush(conseil);

        // Get all the conseilList where dateDebutPeriode is less than DEFAULT_DATE_DEBUT_PERIODE
        defaultConseilShouldNotBeFound("dateDebutPeriode.lessThan=" + DEFAULT_DATE_DEBUT_PERIODE);

        // Get all the conseilList where dateDebutPeriode is less than UPDATED_DATE_DEBUT_PERIODE
        defaultConseilShouldBeFound("dateDebutPeriode.lessThan=" + UPDATED_DATE_DEBUT_PERIODE);
    }

    @Test
    @Transactional
    void getAllConseilsByDateDebutPeriodeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        conseilRepository.saveAndFlush(conseil);

        // Get all the conseilList where dateDebutPeriode is greater than DEFAULT_DATE_DEBUT_PERIODE
        defaultConseilShouldNotBeFound("dateDebutPeriode.greaterThan=" + DEFAULT_DATE_DEBUT_PERIODE);

        // Get all the conseilList where dateDebutPeriode is greater than SMALLER_DATE_DEBUT_PERIODE
        defaultConseilShouldBeFound("dateDebutPeriode.greaterThan=" + SMALLER_DATE_DEBUT_PERIODE);
    }

    @Test
    @Transactional
    void getAllConseilsByDateFinPeriodeIsEqualToSomething() throws Exception {
        // Initialize the database
        conseilRepository.saveAndFlush(conseil);

        // Get all the conseilList where dateFinPeriode equals to DEFAULT_DATE_FIN_PERIODE
        defaultConseilShouldBeFound("dateFinPeriode.equals=" + DEFAULT_DATE_FIN_PERIODE);

        // Get all the conseilList where dateFinPeriode equals to UPDATED_DATE_FIN_PERIODE
        defaultConseilShouldNotBeFound("dateFinPeriode.equals=" + UPDATED_DATE_FIN_PERIODE);
    }

    @Test
    @Transactional
    void getAllConseilsByDateFinPeriodeIsInShouldWork() throws Exception {
        // Initialize the database
        conseilRepository.saveAndFlush(conseil);

        // Get all the conseilList where dateFinPeriode in DEFAULT_DATE_FIN_PERIODE or UPDATED_DATE_FIN_PERIODE
        defaultConseilShouldBeFound("dateFinPeriode.in=" + DEFAULT_DATE_FIN_PERIODE + "," + UPDATED_DATE_FIN_PERIODE);

        // Get all the conseilList where dateFinPeriode equals to UPDATED_DATE_FIN_PERIODE
        defaultConseilShouldNotBeFound("dateFinPeriode.in=" + UPDATED_DATE_FIN_PERIODE);
    }

    @Test
    @Transactional
    void getAllConseilsByDateFinPeriodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        conseilRepository.saveAndFlush(conseil);

        // Get all the conseilList where dateFinPeriode is not null
        defaultConseilShouldBeFound("dateFinPeriode.specified=true");

        // Get all the conseilList where dateFinPeriode is null
        defaultConseilShouldNotBeFound("dateFinPeriode.specified=false");
    }

    @Test
    @Transactional
    void getAllConseilsByDateFinPeriodeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        conseilRepository.saveAndFlush(conseil);

        // Get all the conseilList where dateFinPeriode is greater than or equal to DEFAULT_DATE_FIN_PERIODE
        defaultConseilShouldBeFound("dateFinPeriode.greaterThanOrEqual=" + DEFAULT_DATE_FIN_PERIODE);

        // Get all the conseilList where dateFinPeriode is greater than or equal to UPDATED_DATE_FIN_PERIODE
        defaultConseilShouldNotBeFound("dateFinPeriode.greaterThanOrEqual=" + UPDATED_DATE_FIN_PERIODE);
    }

    @Test
    @Transactional
    void getAllConseilsByDateFinPeriodeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        conseilRepository.saveAndFlush(conseil);

        // Get all the conseilList where dateFinPeriode is less than or equal to DEFAULT_DATE_FIN_PERIODE
        defaultConseilShouldBeFound("dateFinPeriode.lessThanOrEqual=" + DEFAULT_DATE_FIN_PERIODE);

        // Get all the conseilList where dateFinPeriode is less than or equal to SMALLER_DATE_FIN_PERIODE
        defaultConseilShouldNotBeFound("dateFinPeriode.lessThanOrEqual=" + SMALLER_DATE_FIN_PERIODE);
    }

    @Test
    @Transactional
    void getAllConseilsByDateFinPeriodeIsLessThanSomething() throws Exception {
        // Initialize the database
        conseilRepository.saveAndFlush(conseil);

        // Get all the conseilList where dateFinPeriode is less than DEFAULT_DATE_FIN_PERIODE
        defaultConseilShouldNotBeFound("dateFinPeriode.lessThan=" + DEFAULT_DATE_FIN_PERIODE);

        // Get all the conseilList where dateFinPeriode is less than UPDATED_DATE_FIN_PERIODE
        defaultConseilShouldBeFound("dateFinPeriode.lessThan=" + UPDATED_DATE_FIN_PERIODE);
    }

    @Test
    @Transactional
    void getAllConseilsByDateFinPeriodeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        conseilRepository.saveAndFlush(conseil);

        // Get all the conseilList where dateFinPeriode is greater than DEFAULT_DATE_FIN_PERIODE
        defaultConseilShouldNotBeFound("dateFinPeriode.greaterThan=" + DEFAULT_DATE_FIN_PERIODE);

        // Get all the conseilList where dateFinPeriode is greater than SMALLER_DATE_FIN_PERIODE
        defaultConseilShouldBeFound("dateFinPeriode.greaterThan=" + SMALLER_DATE_FIN_PERIODE);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultConseilShouldBeFound(String filter) throws Exception {
        restConseilMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(conseil.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateDebutPeriode").value(hasItem(DEFAULT_DATE_DEBUT_PERIODE.toString())))
            .andExpect(jsonPath("$.[*].dateFinPeriode").value(hasItem(DEFAULT_DATE_FIN_PERIODE.toString())));

        // Check, that the count call also returns 1
        restConseilMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultConseilShouldNotBeFound(String filter) throws Exception {
        restConseilMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restConseilMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingConseil() throws Exception {
        // Get the conseil
        restConseilMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingConseil() throws Exception {
        // Initialize the database
        conseilRepository.saveAndFlush(conseil);

        int databaseSizeBeforeUpdate = conseilRepository.findAll().size();

        // Update the conseil
        Conseil updatedConseil = conseilRepository.findById(conseil.getId()).get();
        // Disconnect from session so that the updates on updatedConseil are not directly saved in db
        em.detach(updatedConseil);
        updatedConseil.dateDebutPeriode(UPDATED_DATE_DEBUT_PERIODE).dateFinPeriode(UPDATED_DATE_FIN_PERIODE);

        restConseilMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedConseil.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedConseil))
            )
            .andExpect(status().isOk());

        // Validate the Conseil in the database
        List<Conseil> conseilList = conseilRepository.findAll();
        assertThat(conseilList).hasSize(databaseSizeBeforeUpdate);
        Conseil testConseil = conseilList.get(conseilList.size() - 1);
        assertThat(testConseil.getDateDebutPeriode()).isEqualTo(UPDATED_DATE_DEBUT_PERIODE);
        assertThat(testConseil.getDateFinPeriode()).isEqualTo(UPDATED_DATE_FIN_PERIODE);
    }

    @Test
    @Transactional
    void putNonExistingConseil() throws Exception {
        int databaseSizeBeforeUpdate = conseilRepository.findAll().size();
        conseil.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restConseilMockMvc
            .perform(
                put(ENTITY_API_URL_ID, conseil.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(conseil))
            )
            .andExpect(status().isBadRequest());

        // Validate the Conseil in the database
        List<Conseil> conseilList = conseilRepository.findAll();
        assertThat(conseilList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchConseil() throws Exception {
        int databaseSizeBeforeUpdate = conseilRepository.findAll().size();
        conseil.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restConseilMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(conseil))
            )
            .andExpect(status().isBadRequest());

        // Validate the Conseil in the database
        List<Conseil> conseilList = conseilRepository.findAll();
        assertThat(conseilList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamConseil() throws Exception {
        int databaseSizeBeforeUpdate = conseilRepository.findAll().size();
        conseil.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restConseilMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(conseil)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Conseil in the database
        List<Conseil> conseilList = conseilRepository.findAll();
        assertThat(conseilList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateConseilWithPatch() throws Exception {
        // Initialize the database
        conseilRepository.saveAndFlush(conseil);

        int databaseSizeBeforeUpdate = conseilRepository.findAll().size();

        // Update the conseil using partial update
        Conseil partialUpdatedConseil = new Conseil();
        partialUpdatedConseil.setId(conseil.getId());

        partialUpdatedConseil.dateDebutPeriode(UPDATED_DATE_DEBUT_PERIODE).dateFinPeriode(UPDATED_DATE_FIN_PERIODE);

        restConseilMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedConseil.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedConseil))
            )
            .andExpect(status().isOk());

        // Validate the Conseil in the database
        List<Conseil> conseilList = conseilRepository.findAll();
        assertThat(conseilList).hasSize(databaseSizeBeforeUpdate);
        Conseil testConseil = conseilList.get(conseilList.size() - 1);
        assertThat(testConseil.getDateDebutPeriode()).isEqualTo(UPDATED_DATE_DEBUT_PERIODE);
        assertThat(testConseil.getDateFinPeriode()).isEqualTo(UPDATED_DATE_FIN_PERIODE);
    }

    @Test
    @Transactional
    void fullUpdateConseilWithPatch() throws Exception {
        // Initialize the database
        conseilRepository.saveAndFlush(conseil);

        int databaseSizeBeforeUpdate = conseilRepository.findAll().size();

        // Update the conseil using partial update
        Conseil partialUpdatedConseil = new Conseil();
        partialUpdatedConseil.setId(conseil.getId());

        partialUpdatedConseil.dateDebutPeriode(UPDATED_DATE_DEBUT_PERIODE).dateFinPeriode(UPDATED_DATE_FIN_PERIODE);

        restConseilMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedConseil.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedConseil))
            )
            .andExpect(status().isOk());

        // Validate the Conseil in the database
        List<Conseil> conseilList = conseilRepository.findAll();
        assertThat(conseilList).hasSize(databaseSizeBeforeUpdate);
        Conseil testConseil = conseilList.get(conseilList.size() - 1);
        assertThat(testConseil.getDateDebutPeriode()).isEqualTo(UPDATED_DATE_DEBUT_PERIODE);
        assertThat(testConseil.getDateFinPeriode()).isEqualTo(UPDATED_DATE_FIN_PERIODE);
    }

    @Test
    @Transactional
    void patchNonExistingConseil() throws Exception {
        int databaseSizeBeforeUpdate = conseilRepository.findAll().size();
        conseil.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restConseilMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, conseil.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(conseil))
            )
            .andExpect(status().isBadRequest());

        // Validate the Conseil in the database
        List<Conseil> conseilList = conseilRepository.findAll();
        assertThat(conseilList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchConseil() throws Exception {
        int databaseSizeBeforeUpdate = conseilRepository.findAll().size();
        conseil.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restConseilMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(conseil))
            )
            .andExpect(status().isBadRequest());

        // Validate the Conseil in the database
        List<Conseil> conseilList = conseilRepository.findAll();
        assertThat(conseilList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamConseil() throws Exception {
        int databaseSizeBeforeUpdate = conseilRepository.findAll().size();
        conseil.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restConseilMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(conseil)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Conseil in the database
        List<Conseil> conseilList = conseilRepository.findAll();
        assertThat(conseilList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteConseil() throws Exception {
        // Initialize the database
        conseilRepository.saveAndFlush(conseil);

        int databaseSizeBeforeDelete = conseilRepository.findAll().size();

        // Delete the conseil
        restConseilMockMvc
            .perform(delete(ENTITY_API_URL_ID, conseil.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Conseil> conseilList = conseilRepository.findAll();
        assertThat(conseilList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
