import { Component, Vue, Provide } from 'vue-property-decorator';

import { IParcelle } from '@/shared/model/parcelle.model';
import ParcelleService from '@/entities/parcelle/parcelle.service';
import AlertService from '@/shared/alert/alert.service';
import PlantationService from '@/entities/plantation/plantation.service';
import { IPlantationCulture } from '@/shared/dto/plantationculture.dto';
import { IEmplacementPlantation } from '@/shared/dto/emplacementplantation.dto';
import GridLayout from 'vue-grid-layout';
import CultureService from '@/entities/culture/culture.service';
import { ICulture } from '@/shared/model/culture.model';
import { IPlante } from '@/shared/model/plante.model';
import PlanteService from '@/entities/plante/plante.service';
import { Etat } from '@/shared/model/enumerations/etat.model';
import { ITache } from '@/shared/model/tache.model';
import TacheService from '@/entities/tache/tache.service';

@Component({
  components: {
    GridLayout: GridLayout.GridLayout,
    GridItem: GridLayout.GridItem
  },
})
export default class MaParcelle extends Vue {
  @Provide('parcelleService') public parcelleService = () => new ParcelleService();
  @Provide('plantationService') public plantationService = () => new PlantationService();
  @Provide('planteService') public planteService = () => new PlanteService();
  @Provide('cultureService') public cultureService = () => new CultureService();
  @Provide('alertService') public alertService = () => new AlertService();
  @Provide('tacheService') public tacheService = () => new TacheService();

  public parcelle: IParcelle = {};
  public plantes: IPlante[] = [];
  public plantationsCulture: IPlantationCulture[] = [];
  public casePlantations: IEmplacementPlantation[] = []
  public casePlantationOuverte: IEmplacementPlantation = {};
  public taches: ITache[] = [];
  public allTaches: ITache[] = [];
  public ouvreCasePlantation = false;
  public formPlanter = false;
  public propOrder = 'id';
  public nouvelleCulture: ICulture = {};
  public chargement = false;
  public dateVisualisation = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());

  data() {
    return {
      fieldsTache: ['nom','type','actions']
    };
  }

  beforeRouteEnter(to: { params: { parcelleId: any; }; }, from: any, next: (arg0: (vm: any) => void) => void) {
    next((vm: { retrieveParcelle: (arg0: any) => void; }) => {
      if (to.params.parcelleId) {
        vm.retrieveParcelle(to.params.parcelleId);
      }
    });
  }

  mounted() {
  }

  public retrieveParcelle(parcelleId: number) {
    this.parcelleService()
      .find(parcelleId)
      .then(res => {
        this.parcelle = res;
        this.getPlantationsParcelle();
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public retrieveAllPlantes(): void {
    const paginationQuery = {
      page: -1,
      size: 1000,
      sort: this.sort(),
    };
    this.planteService()
      .retrieve(paginationQuery)
      .then(
        res => {
          if (res.data && res.data.length > 0 && !(this.plantes.length>0) ) {
            for (let i = 0; i < res.data.length; i++) {
              this.plantes.push(res.data[i]);
              console.log('this.plantes=', this.plantes)
            }
          }
        },
        err => {
          this.alertService().showHttpError(this, err.response);
        }
      );
  }

  // récupère liste des parcelles du potager choisi 
  public getPlantationsParcelle(): void {
    let that = this;
    this.plantationService()
      .retrieveParcelle(this.parcelle.id, Date.parse(this.dateVisualisation.toString()))
      .then(res => {
        that.plantationsCulture = res.data;
        that.creeGrid();
        that.getTaches();
      })
      .catch((error: { response: any; }) => {
        console.log(error)
        this.alertService().showHttpError(this, error.response);
      });
  }

  public creeGrid(): void {
    let x = 0; let y=0; let indexCol = 1;
    this.casePlantations = [];
    this.plantationsCulture.forEach(plantation => {
      if (plantation.cultureId) {
        this.casePlantations.push({'nomPlante': plantation.nomCulture, 'image': plantation.plante.image, 
                            'x': x, 'y': y, 'w': 1, 'h': 1, 'i': plantation.cultureId, 'plantation': plantation, 'plante': plantation.plante, 
                            'nomCulture': plantation.nomCulture, 'dateDebut': plantation.dateDebut.toString(), 'dateFin': plantation.dateFin.toString(), 'dateRecolte': null});
      } else if (plantation) {
        this.casePlantations.push({'nomPlante': 'Vide', 'image': '../../../content/images/terre.webp', 
                            'x': x, 'y': y, 'w': 1, 'h': 1, 'i': plantation.cultureId, 'plantation': plantation, 'plante': plantation.plante,
                            'nomCulture': 'Emplacement vide', 'dateDebut': null, 'dateFin': null, 'dateRecolte': null});
      }
      if (indexCol + 1 > this.parcelle.nbPlantationLongueur) {
        indexCol = 1; 
        y++;
        x = 0;
      } else {
        indexCol++;
        x++;
      }
    });
  }

  // récupère liste des taches du potager choisi 
  getTaches(): void {
    const paginationQuery = {
      page: -1,
      size: 1000,
      sort: this.sort()
    };

    this.tacheService()
      .retrieve(paginationQuery)
      .then((response: {data: ITache[];}) => {
        this.allTaches = response.data;
        this.taches = this.filterTaches();
      })
      .catch((error: { response: any; }) => {
        this.alertService().showError(this, error.response);
      });
  }

  // filtrer en fonction de la parcelle
  filterTaches(): ITache[] {
    let taches: ITache[] = [];
    this.allTaches.forEach((item: ITache) => {
      if (item.parcelle && item.parcelle.id == this.parcelle.id && item.etat == Etat.ACTIVE) {
        taches.push(item);
      }
    });
    return taches;
  }

  ackTache(tache: ITache): void {
    console.log('ackTache=', JSON.stringify(tache));
    tache.etat = Etat.LUE;
    this.tacheService()
    .partialUpdate(tache)
    .then(() => {
      this.getTaches();
    })
    .catch((error: { response: any; }) => {
      this.alertService().showError(this, error.response);
    });
  }

  public voirPlanteCase(casePlantation: IEmplacementPlantation): void {
    if (casePlantation) {
      this.casePlantationOuverte = casePlantation;
      console.log('casePlantationOuverte=', JSON.stringify(this.casePlantationOuverte));
      this.ouvreCasePlantation = true;
      this.retrieveAllPlantes();
    } 
  }

  public arracher(): any {
    this.chargement = true;
    this.cultureService()
        .arracher(this.casePlantationOuverte.i)
        .then(() => {
          this.getPlantationsParcelle();
          this.chargement = false;
          this.ouvreCasePlantation = false;
        })
        .catch(error => {
          this.chargement = false;
          this.alertService().showHttpError(this, error.response);
        });
  }

  public recolter(): any {
    this.chargement = true;
    this.cultureService()
        .recolter(this.casePlantationOuverte.i)
        .then(() => {
          this.getPlantationsParcelle();
          this.chargement = false;
          this.ouvreCasePlantation = false;
        })
        .catch(error => {
          this.chargement = false;
          this.alertService().showHttpError(this, error.response);
        });
  }

  public confirmerPlantation(): any {
    let boutonSubmit = this.$refs.submit as HTMLElement;
    boutonSubmit.click();
  }

  public lancerPlantation(): any {
    this.nouvelleCulture.plantation = this.casePlantationOuverte.plantation;
    console.log('this.nouvelleCulture=', JSON.stringify(this.nouvelleCulture));
    this.chargement = true;
    this.cultureService()
        .create(this.nouvelleCulture)
        .then((param) => {
          this.ouvreCasePlantation = false;        
          this.formPlanter = false; 
          this.chargement = false;
          const message = this.$t('monpotagerApp.culture.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
          this.getPlantationsParcelle();          
        })
        .catch(error => {
          this.chargement = false;
          this.alertService().showHttpError(this, error.response);
        });
  }

  public retour() {
    this.$router.go(-1);
  }

  public sort(): Array<any> {
    const result = [this.propOrder + ',asc'];
    if (this.propOrder !== 'id') {
      result.push('id');
    }
    return result;
  }
}
