import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import PlantationService from '@/entities/plantation/plantation.service';
import { IPlantation } from '@/shared/model/plantation.model';

import PlanteService from '@/entities/plante/plante.service';
import { IPlante } from '@/shared/model/plante.model';

import { ICulture, Culture } from '@/shared/model/culture.model';
import CultureService from './culture.service';

const validations: any = {
  culture: {
    nom: {},
    dateDebut: {},
    dateFin: {},
    dateRecolte: {},
  },
};

@Component({
  validations,
})
export default class CultureUpdate extends Vue {
  @Inject('cultureService') private cultureService: () => CultureService;
  @Inject('alertService') private alertService: () => AlertService;

  public culture: ICulture = new Culture();

  @Inject('plantationService') private plantationService: () => PlantationService;

  public plantations: IPlantation[] = [];

  @Inject('planteService') private planteService: () => PlanteService;

  public plantes: IPlante[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.cultureId) {
        vm.retrieveCulture(to.params.cultureId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.culture.id) {
      this.cultureService()
        .update(this.culture)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('monpotagerApp.culture.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.cultureService()
        .create(this.culture)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('monpotagerApp.culture.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveCulture(cultureId): void {
    this.cultureService()
      .find(cultureId)
      .then(res => {
        this.culture = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.plantationService()
      .retrieve()
      .then(res => {
        this.plantations = res.data;
      });
    this.planteService()
      .retrieve()
      .then(res => {
        this.plantes = res.data;
      });
  }
}
