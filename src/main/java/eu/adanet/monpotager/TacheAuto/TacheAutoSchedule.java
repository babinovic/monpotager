package eu.adanet.monpotager.TacheAuto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import eu.adanet.monpotager.service.TacheService;

@Service
public class TacheAutoSchedule {
    private final Logger log = LoggerFactory.getLogger(TacheAutoSchedule.class);
    @Autowired
    TacheAutoProcess tacheAutoProcess;
    @Autowired
    private TacheService tacheService;

    @Scheduled(cron = "0 0 * * * *")
    public void consumeData() {
        log.debug("exection consumeData");
        tacheAutoProcess.conseilsAsync(tacheService.getTachesAutoMois());
    }
}
