/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import TacheDetailComponent from '@/entities/tache/tache-details.vue';
import TacheClass from '@/entities/tache/tache-details.component';
import TacheService from '@/entities/tache/tache.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Tache Management Detail Component', () => {
    let wrapper: Wrapper<TacheClass>;
    let comp: TacheClass;
    let tacheServiceStub: SinonStubbedInstance<TacheService>;

    beforeEach(() => {
      tacheServiceStub = sinon.createStubInstance<TacheService>(TacheService);

      wrapper = shallowMount<TacheClass>(TacheDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { tacheService: () => tacheServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundTache = { id: 123 };
        tacheServiceStub.find.resolves(foundTache);

        // WHEN
        comp.retrieveTache(123);
        await comp.$nextTick();

        // THEN
        expect(comp.tache).toBe(foundTache);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundTache = { id: 123 };
        tacheServiceStub.find.resolves(foundTache);

        // WHEN
        comp.beforeRouteEnter({ params: { tacheId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.tache).toBe(foundTache);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
