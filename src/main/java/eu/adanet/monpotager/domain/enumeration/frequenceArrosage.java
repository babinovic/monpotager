package eu.adanet.monpotager.domain.enumeration;

/**
 * The frequenceArrosage enumeration.
 */
public enum frequenceArrosage {
    SANS,
    LEGER,
    MOYEN,
    REGULIER,
}
