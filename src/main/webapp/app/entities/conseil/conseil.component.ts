import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IConseil } from '@/shared/model/conseil.model';

import ConseilService from './conseil.service';
import AlertService from '@/shared/alert/alert.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class Conseil extends Vue {
  @Inject('conseilService') private conseilService: () => ConseilService;
  @Inject('alertService') private alertService: () => AlertService;

  private removeId: number = null;

  public conseils: IConseil[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllConseils();
  }

  public clear(): void {
    this.retrieveAllConseils();
  }

  public retrieveAllConseils(): void {
    this.isFetching = true;
    this.conseilService()
      .retrieve()
      .then(
        res => {
          this.conseils = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
          this.alertService().showHttpError(this, err.response);
        }
      );
  }

  public handleSyncList(): void {
    this.clear();
  }

  public prepareRemove(instance: IConseil): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeConseil(): void {
    this.conseilService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('monpotagerApp.conseil.deleted', { param: this.removeId });
        this.$bvToast.toast(message.toString(), {
          toaster: 'b-toaster-top-center',
          title: 'Info',
          variant: 'danger',
          solid: true,
          autoHideDelay: 5000,
        });
        this.removeId = null;
        this.retrieveAllConseils();
        this.closeDialog();
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
