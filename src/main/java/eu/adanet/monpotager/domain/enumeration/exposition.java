package eu.adanet.monpotager.domain.enumeration;

/**
 * The exposition enumeration.
 */
public enum exposition {
    OMBRE,
    MI_OMBRE,
    SOLEIL,
}
