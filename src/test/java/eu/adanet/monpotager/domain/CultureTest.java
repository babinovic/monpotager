package eu.adanet.monpotager.domain;

import static org.assertj.core.api.Assertions.assertThat;

import eu.adanet.monpotager.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CultureTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Culture.class);
        Culture culture1 = new Culture();
        culture1.setId(1L);
        Culture culture2 = new Culture();
        culture2.setId(culture1.getId());
        assertThat(culture1).isEqualTo(culture2);
        culture2.setId(2L);
        assertThat(culture1).isNotEqualTo(culture2);
        culture1.setId(null);
        assertThat(culture1).isNotEqualTo(culture2);
    }
}
