package eu.adanet.monpotager.domain.enumeration;

/**
 * The Etat enumeration.
 */
public enum Etat {
    ACTIVE,
    LUE,
    REFUSEE,
}
