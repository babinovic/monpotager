package eu.adanet.monpotager.repository;

import eu.adanet.monpotager.domain.Culture;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Culture entity.
 */
@Repository
public interface CultureRepository extends JpaRepository<Culture, Long>, JpaSpecificationExecutor<Culture> {
    default Optional<Culture> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<Culture> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<Culture> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct culture from Culture culture left join fetch culture.plante",
        countQuery = "select count(distinct culture) from Culture culture"
    )
    Page<Culture> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct culture from Culture culture left join fetch culture.plante")
    List<Culture> findAllWithToOneRelationships();

    @Query("select culture from Culture culture left join fetch culture.plante where culture.id =:id")
    Optional<Culture> findOneWithToOneRelationships(@Param("id") Long id);

    /**
     * Renvoie la culture active d'une plantation
     */
    @Query(value="SELECT * FROM culture WHERE plantation_id =:plantationId AND :dateVisualisation >= date_debut AND :dateVisualisation < date_fin", nativeQuery = true)
    Culture findActive(@Param("plantationId")Long plantationId, @Param("dateVisualisation") Date dateVisualisation);
}
