package eu.adanet.monpotager.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link eu.adanet.monpotager.domain.Conseil} entity. This class is used
 * in {@link eu.adanet.monpotager.web.rest.ConseilResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /conseils?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ConseilCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LocalDateFilter dateDebutPeriode;

    private LocalDateFilter dateFinPeriode;

    private Boolean distinct;

    public ConseilCriteria() {}

    public ConseilCriteria(ConseilCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.dateDebutPeriode = other.dateDebutPeriode == null ? null : other.dateDebutPeriode.copy();
        this.dateFinPeriode = other.dateFinPeriode == null ? null : other.dateFinPeriode.copy();
        this.distinct = other.distinct;
    }

    @Override
    public ConseilCriteria copy() {
        return new ConseilCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LocalDateFilter getDateDebutPeriode() {
        return dateDebutPeriode;
    }

    public LocalDateFilter dateDebutPeriode() {
        if (dateDebutPeriode == null) {
            dateDebutPeriode = new LocalDateFilter();
        }
        return dateDebutPeriode;
    }

    public void setDateDebutPeriode(LocalDateFilter dateDebutPeriode) {
        this.dateDebutPeriode = dateDebutPeriode;
    }

    public LocalDateFilter getDateFinPeriode() {
        return dateFinPeriode;
    }

    public LocalDateFilter dateFinPeriode() {
        if (dateFinPeriode == null) {
            dateFinPeriode = new LocalDateFilter();
        }
        return dateFinPeriode;
    }

    public void setDateFinPeriode(LocalDateFilter dateFinPeriode) {
        this.dateFinPeriode = dateFinPeriode;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ConseilCriteria that = (ConseilCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(dateDebutPeriode, that.dateDebutPeriode) &&
            Objects.equals(dateFinPeriode, that.dateFinPeriode) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dateDebutPeriode, dateFinPeriode, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ConseilCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (dateDebutPeriode != null ? "dateDebutPeriode=" + dateDebutPeriode + ", " : "") +
            (dateFinPeriode != null ? "dateFinPeriode=" + dateFinPeriode + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
