/**
 * View Models used by Spring MVC REST controllers.
 */
package eu.adanet.monpotager.web.rest.vm;
