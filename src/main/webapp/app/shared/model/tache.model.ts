import { IPotager } from '@/shared/model/potager.model';
import { IParcelle } from '@/shared/model/parcelle.model';
import { IConseil } from '@/shared/model/conseil.model';

import { typeTache } from '@/shared/model/enumerations/type-tache.model';
import { echelleTache } from '@/shared/model/enumerations/echelle-tache.model';
import { Etat } from '@/shared/model/enumerations/etat.model';
export interface ITache {
  id?: number;
  nom?: string | null;
  type?: typeTache | null;
  echelle?: echelleTache | null;
  dateDebut?: Date | null;
  dateFin?: Date | null;
  etat?: Etat | null;
  potager?: IPotager | null;
  parcelle?: IParcelle | null;
  conseil?: IConseil | null;
}

export class Tache implements ITache {
  constructor(
    public id?: number,
    public nom?: string | null,
    public type?: typeTache | null,
    public echelle?: echelleTache | null,
    public dateDebut?: Date | null,
    public dateFin?: Date | null,
    public etat?: Etat | null,
    public potager?: IPotager | null,
    public parcelle?: IParcelle | null,
    public conseil?: IConseil | null
  ) {}
}
