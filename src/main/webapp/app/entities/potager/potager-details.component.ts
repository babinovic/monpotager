import { Component, Vue, Inject } from 'vue-property-decorator';

import { IPotager } from '@/shared/model/potager.model';
import PotagerService from './potager.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class PotagerDetails extends Vue {
  @Inject('potagerService') private potagerService: () => PotagerService;
  @Inject('alertService') private alertService: () => AlertService;

  public potager: IPotager = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.potagerId) {
        vm.retrievePotager(to.params.potagerId);
      }
    });
  }

  public retrievePotager(potagerId) {
    this.potagerService()
      .find(potagerId)
      .then(res => {
        this.potager = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
