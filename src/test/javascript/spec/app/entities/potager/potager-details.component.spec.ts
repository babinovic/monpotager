/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import PotagerDetailComponent from '@/entities/potager/potager-details.vue';
import PotagerClass from '@/entities/potager/potager-details.component';
import PotagerService from '@/entities/potager/potager.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Potager Management Detail Component', () => {
    let wrapper: Wrapper<PotagerClass>;
    let comp: PotagerClass;
    let potagerServiceStub: SinonStubbedInstance<PotagerService>;

    beforeEach(() => {
      potagerServiceStub = sinon.createStubInstance<PotagerService>(PotagerService);

      wrapper = shallowMount<PotagerClass>(PotagerDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { potagerService: () => potagerServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundPotager = { id: 123 };
        potagerServiceStub.find.resolves(foundPotager);

        // WHEN
        comp.retrievePotager(123);
        await comp.$nextTick();

        // THEN
        expect(comp.potager).toBe(foundPotager);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundPotager = { id: 123 };
        potagerServiceStub.find.resolves(foundPotager);

        // WHEN
        comp.beforeRouteEnter({ params: { potagerId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.potager).toBe(foundPotager);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
