package eu.adanet.monpotager.web.rest;

import eu.adanet.monpotager.domain.Parcelle;
import eu.adanet.monpotager.domain.Plantation;
import eu.adanet.monpotager.repository.ParcelleRepository;
import eu.adanet.monpotager.service.ParcelleService;
import eu.adanet.monpotager.service.PlantationService;
import eu.adanet.monpotager.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link eu.adanet.monpotager.domain.Parcelle}.
 */
@RestController
@RequestMapping("/api")
public class ParcelleResource {

    private final Logger log = LoggerFactory.getLogger(ParcelleResource.class);

    private static final String ENTITY_NAME = "parcelle";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ParcelleService parcelleService;

    private final ParcelleRepository parcelleRepository;

    private final PlantationService plantationService;

    final int ESPACEMENT = 30;
    final int ESPACEMENT_RANG = 50;

    public ParcelleResource(ParcelleService parcelleService, ParcelleRepository parcelleRepository, PlantationService plantationService) {
        this.parcelleService = parcelleService;
        this.plantationService = plantationService;
        this.parcelleRepository = parcelleRepository;
    }

    /**
     * {@code POST  /parcelles} : Create a new parcelle.
     *
     * @param parcelle the parcelle to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new parcelle, or with status {@code 400 (Bad Request)} if the parcelle has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/parcelles")
    public ResponseEntity<Parcelle> createParcelle(@RequestBody Parcelle parcelle) throws URISyntaxException {
        log.debug("REST request to save Parcelle : {}", parcelle);
        if (parcelle.getId() != null) {
            throw new BadRequestAlertException("A new parcelle cannot already have an ID", ENTITY_NAME, "idexists");
        }
        parcelle.setNbPlantationLongueur(getNbPlantationLongueur(parcelle));
        parcelle.setNbPlantationLargeur(getNbPlantationLargeur(parcelle));
        Parcelle result = parcelleService.save(parcelle);
        creerPlantations(result);

        return ResponseEntity
            .created(new URI("/api/parcelles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    // créer les emplacements de plantation d'une nouvelle parcelle
    private void creerPlantations(Parcelle parcelle) {
        int nbCellulesLongueur = getNbPlantationLongueur(parcelle);
        int nbCellulesLargeur =  getNbPlantationLargeur(parcelle);
        log.info("nbCellulesLongueur=" + nbCellulesLongueur + " - nbCellulesLargeur=" + nbCellulesLargeur);
        for (int index = 1; index <= nbCellulesLongueur*nbCellulesLargeur; index++) {
           log.info("cree plantation " + index);
            Plantation plantation = new Plantation();
            plantation.setParcelle(parcelle);
            plantation.setIndexParcelle(index);
            plantationService.save(plantation);
        }       
    }

    private int getNbPlantationLongueur(Parcelle parcelle) {
        return (int)(parcelle.getLongueur()*100) / ESPACEMENT;
    }

    private int getNbPlantationLargeur(Parcelle parcelle) {
        return (int)(parcelle.getLargeur()*100) / ESPACEMENT_RANG;
    }

    /**
     * {@code PUT  /parcelles/:id} : Updates an existing parcelle.
     *
     * @param id the id of the parcelle to save.
     * @param parcelle the parcelle to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated parcelle,
     * or with status {@code 400 (Bad Request)} if the parcelle is not valid,
     * or with status {@code 500 (Internal Server Error)} if the parcelle couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/parcelles/{id}")
    public ResponseEntity<Parcelle> updateParcelle(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Parcelle parcelle
    ) throws URISyntaxException {
        log.debug("REST request to update Parcelle : {}, {}", id, parcelle);
        if (parcelle.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, parcelle.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!parcelleRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Parcelle result = parcelleService.update(parcelle);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, parcelle.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /parcelles/:id} : Partial updates given fields of an existing parcelle, field will ignore if it is null
     *
     * @param id the id of the parcelle to save.
     * @param parcelle the parcelle to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated parcelle,
     * or with status {@code 400 (Bad Request)} if the parcelle is not valid,
     * or with status {@code 404 (Not Found)} if the parcelle is not found,
     * or with status {@code 500 (Internal Server Error)} if the parcelle couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/parcelles/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Parcelle> partialUpdateParcelle(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Parcelle parcelle
    ) throws URISyntaxException {
        log.debug("REST request to partial update Parcelle partially : {}, {}", id, parcelle);
        if (parcelle.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, parcelle.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!parcelleRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Parcelle> result = parcelleService.partialUpdate(parcelle);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, parcelle.getId().toString())
        );
    }

    /**
     * {@code GET  /parcelles} : get all the parcelles.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of parcelles in body.
     */
    @GetMapping("/parcelles")
    public ResponseEntity<List<Parcelle>> getAllParcelles(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        @RequestParam(required = false, defaultValue = "false") boolean eagerload
    ) {
        log.debug("REST request to get a page of Parcelles");
        Page<Parcelle> page;
        if (eagerload) {
            page = parcelleService.findAllWithEagerRelationships(pageable);
        } else {
            page = parcelleService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /parcelles/:id} : get the "id" parcelle.
     *
     * @param id the id of the parcelle to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the parcelle, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/parcelles/{id}")
    public ResponseEntity<Parcelle> getParcelle(@PathVariable Long id) {
        log.debug("REST request to get Parcelle : {}", id);
        Optional<Parcelle> parcelle = parcelleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(parcelle);
    }

    /**
     * {@code DELETE  /parcelles/:id} : delete the "id" parcelle.
     *
     * @param id the id of the parcelle to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/parcelles/{id}")
    public ResponseEntity<Void> deleteParcelle(@PathVariable Long id) {
        log.debug("REST request to delete Parcelle : {}", id);
        parcelleService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
