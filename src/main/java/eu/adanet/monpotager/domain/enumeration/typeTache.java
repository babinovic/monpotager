package eu.adanet.monpotager.domain.enumeration;

/**
 * The typeTache enumeration.
 */
public enum typeTache {
    SEMIS,
    PLANTATION,
    ARROSAGE,
    TRAITEMENT,
    ENTRETIEN,
    RECOLTE,
}
