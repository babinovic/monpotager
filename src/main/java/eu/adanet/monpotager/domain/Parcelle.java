package eu.adanet.monpotager.domain;

import eu.adanet.monpotager.domain.enumeration.exposition;
import java.io.Serializable;
import javax.persistence.*;

/**
 * A Parcelle.
 */
@Entity
@Table(name = "parcelle")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Parcelle implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "longueur")
    private Float longueur;

    @Column(name = "largeur")
    private Float largeur;

    @Enumerated(EnumType.STRING)
    @Column(name = "ensoleillement")
    private exposition ensoleillement;

    @Column(name = "nb_plantation_longueur")
    private Integer nbPlantationLongueur;

    @Column(name = "nb_plantation_largeur")
    private Integer nbPlantationLargeur;

    @ManyToOne
    private Potager potager;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Parcelle id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public Parcelle nom(String nom) {
        this.setNom(nom);
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Float getLongueur() {
        return this.longueur;
    }

    public Parcelle longueur(Float longueur) {
        this.setLongueur(longueur);
        return this;
    }

    public void setLongueur(Float longueur) {
        this.longueur = longueur;
    }

    public Float getLargeur() {
        return this.largeur;
    }

    public Parcelle largeur(Float largeur) {
        this.setLargeur(largeur);
        return this;
    }

    public void setLargeur(Float largeur) {
        this.largeur = largeur;
    }

    public exposition getEnsoleillement() {
        return this.ensoleillement;
    }

    public Parcelle ensoleillement(exposition ensoleillement) {
        this.setEnsoleillement(ensoleillement);
        return this;
    }

    public void setEnsoleillement(exposition ensoleillement) {
        this.ensoleillement = ensoleillement;
    }

    public Integer getNbPlantationLongueur() {
        return this.nbPlantationLongueur;
    }

    public Parcelle nbPlantationLongueur(Integer nbPlantationLongueur) {
        this.setNbPlantationLongueur(nbPlantationLongueur);
        return this;
    }

    public void setNbPlantationLongueur(Integer nbPlantationLongueur) {
        this.nbPlantationLongueur = nbPlantationLongueur;
    }

    public Integer getNbPlantationLargeur() {
        return this.nbPlantationLargeur;
    }

    public Parcelle nbPlantationLargeur(Integer nbPlantationLargeur) {
        this.setNbPlantationLargeur(nbPlantationLargeur);
        return this;
    }

    public void setNbPlantationLargeur(Integer nbPlantationLargeur) {
        this.nbPlantationLargeur = nbPlantationLargeur;
    }

    public Potager getPotager() {
        return this.potager;
    }

    public void setPotager(Potager potager) {
        this.potager = potager;
    }

    public Parcelle potager(Potager potager) {
        this.setPotager(potager);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Parcelle)) {
            return false;
        }
        return id != null && id.equals(((Parcelle) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Parcelle{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", longueur=" + getLongueur() +
            ", largeur=" + getLargeur() +
            ", ensoleillement='" + getEnsoleillement() + "'" +
            ", nbPlantationLongueur=" + getNbPlantationLongueur() +
            ", nbPlantationLargeur=" + getNbPlantationLargeur() +
            "}";
    }
}
