package eu.adanet.monpotager.service;

import eu.adanet.monpotager.domain.Conseil;
import eu.adanet.monpotager.domain.Plante;
import eu.adanet.monpotager.domain.Potager;
import eu.adanet.monpotager.domain.Tache;
import eu.adanet.monpotager.domain.enumeration.Etat;
import eu.adanet.monpotager.domain.enumeration.categoriePlante;
import eu.adanet.monpotager.domain.enumeration.echelleTache;
import eu.adanet.monpotager.domain.enumeration.Mois;
import eu.adanet.monpotager.domain.enumeration.typePlante;
import eu.adanet.monpotager.domain.enumeration.typeTache;
import eu.adanet.monpotager.repository.ConseilRepository;
import eu.adanet.monpotager.repository.TacheRepository;
import eu.adanet.monpotager.service.criteria.ConseilCriteria;
import tech.jhipster.service.filter.LocalDateFilter;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Tache}.
 */
@Service
@Transactional
public class TacheService {

    private final Logger log = LoggerFactory.getLogger(TacheService.class);

    private final TacheRepository tacheRepository;
    private final ConseilQueryService conseilQueryService;
    private final PlanteService planteService;
    private final ConseilService conseilService;
    private final PotagerService potagerService;

    public TacheService(TacheRepository tacheRepository, ConseilQueryService conseilQueryService, PlanteService planteService, ConseilService conseilService, PotagerService potagerService) {
        this.tacheRepository = tacheRepository;
        this.conseilQueryService = conseilQueryService;
        this.conseilService = conseilService;
        this.planteService = planteService;
        this.potagerService = potagerService;
    }

    /**
     * Save a tache.
     *
     * @param tache the entity to save.
     * @return the persisted entity.
     */
    public Tache save(Tache tache) {
        log.debug("Request to save Tache : {}", tache);
        return tacheRepository.save(tache);
    }

    /**
     * Update a tache.
     *
     * @param tache the entity to save.
     * @return the persisted entity.
     */
    public Tache update(Tache tache) {
        log.debug("Request to update Tache : {}", tache);
        return tacheRepository.save(tache);
    }

    /**
     * Partially update a tache.
     *
     * @param tache the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Tache> partialUpdate(Tache tache) {
        log.debug("Request to partially update Tache : {}", tache);

        return tacheRepository
            .findById(tache.getId())
            .map(existingTache -> {
                if (tache.getNom() != null) {
                    existingTache.setNom(tache.getNom());
                }
                if (tache.getType() != null) {
                    existingTache.setType(tache.getType());
                }
                if (tache.getEchelle() != null) {
                    existingTache.setEchelle(tache.getEchelle());
                }
                if (tache.getDateDebut() != null) {
                    existingTache.setDateDebut(tache.getDateDebut());
                }
                if (tache.getDateFin() != null) {
                    existingTache.setDateFin(tache.getDateFin());
                }
                if (tache.getEtat() != null) {
                    existingTache.setEtat(tache.getEtat());
                }

                return existingTache;
            })
            .map(tacheRepository::save);
    }

    /**
     * Get all the taches.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Tache> findAll(Pageable pageable) {
        log.debug("Request to get all Taches");
        return tacheRepository.findAll(pageable);
    }

    /**
     * Get all the taches with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<Tache> findAllWithEagerRelationships(Pageable pageable) {
        return tacheRepository.findAllWithEagerRelationships(pageable);
    }

    /**
     * Get one tache by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Tache> findOne(Long id) {
        log.debug("Request to get Tache : {}", id);
        return tacheRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the tache by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Tache : {}", id);
        tacheRepository.deleteById(id);
    }

    public List<Tache> getTachesAutoMois() {
        List<Tache> tachesConseils = new ArrayList<>();
        
        // si oui, liste vide
        // sinon
        if (conseilsMoisADonner()) {
            // on récupère la liste de tous les légumes
            List<Plante> plantes = planteService.findAll(Pageable.unpaged()).getContent();
            // on crée le conseil du mois pour pouvoir lier les taches
            Conseil conseilMois = conseilService.save(new Conseil(YearMonth.now().atDay(1), YearMonth.now().atEndOfMonth()));
            for (Plante plante : plantes) {
                // on compare les dates de plantation et de semis au mois en cours
                if (plante.getCategorie() == categoriePlante.SEMIS && plante.getDebutSemis() != null && plante.getDebutSemis().equals(getMois())) {
                    // on ajoute un conseil de semis pour chaque potager
                    for (Potager potager : potagerService.findAll()) {
                        tachesConseils.add(getTacheAuto("Suggestion: semer ", conseilMois, plante, potager));
                    }
                }
                if (plante.getCategorie() == categoriePlante.PLANT && plante.getDebutPlantation() != null && plante.getDebutPlantation().equals(getMois())) {
                    // on ajoute un conseil de plantation par potager
                    for (Potager potager : potagerService.findAll()) {
                        tachesConseils.add(getTacheAuto("Suggestion: planter ", conseilMois, plante, potager));
                    }
                }
            }            
        }       
        return tachesConseils;
    }

    private Tache getTacheAuto(String operation, Conseil conseilMois, Plante plante, Potager potager) {
        String nom = operation + plante.getNom() + " - " + plante.getVariete();
        typeTache type = plante.getCategorie() == categoriePlante.SEMIS ? typeTache.SEMIS : typeTache.PLANTATION;
        return new Tache(nom, type, echelleTache.POTAGER , LocalDate.now(), LocalDate.now().plusMonths(1), Etat.ACTIVE, conseilMois, potager);
    }

    public Boolean conseilsMoisADonner() {
        // on regarde si la date du jour est incluse dans la dernière période de conseil
        ConseilCriteria criteres = new ConseilCriteria();
        LocalDateFilter dateDebutPeriode = new LocalDateFilter();
        dateDebutPeriode.setLessThan(LocalDate.now());
        LocalDateFilter dateFinPeriode = new LocalDateFilter();
       
        dateFinPeriode.setGreaterThanOrEqual(LocalDate.now());
        criteres.setDateDebutPeriode(dateDebutPeriode);
        criteres.setDateFinPeriode(dateFinPeriode);
        List<Conseil> conseils = conseilQueryService.findByCriteria(criteres);
        return conseils.size() < 1;
    }

    private Mois getMois() {
        Date aujourdhui = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM");
        String moisActuel = dateFormat.format(aujourdhui);
        Mois mois = Mois.valueOf(moisActuel.toUpperCase());
        return mois;
    }
}
