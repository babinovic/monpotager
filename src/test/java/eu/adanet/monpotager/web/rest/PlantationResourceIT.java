package eu.adanet.monpotager.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import eu.adanet.monpotager.IntegrationTest;
import eu.adanet.monpotager.domain.Plantation;
import eu.adanet.monpotager.repository.PlantationRepository;
import eu.adanet.monpotager.service.PlantationService;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PlantationResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class PlantationResourceIT {

    private static final Integer DEFAULT_INDEX_PARCELLE = 1;
    private static final Integer UPDATED_INDEX_PARCELLE = 2;

    private static final String ENTITY_API_URL = "/api/plantations";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PlantationRepository plantationRepository;

    @Mock
    private PlantationRepository plantationRepositoryMock;

    @Mock
    private PlantationService plantationServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPlantationMockMvc;

    private Plantation plantation;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Plantation createEntity(EntityManager em) {
        Plantation plantation = new Plantation().indexParcelle(DEFAULT_INDEX_PARCELLE);
        return plantation;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Plantation createUpdatedEntity(EntityManager em) {
        Plantation plantation = new Plantation().indexParcelle(UPDATED_INDEX_PARCELLE);
        return plantation;
    }

    @BeforeEach
    public void initTest() {
        plantation = createEntity(em);
    }

    @Test
    @Transactional
    void createPlantation() throws Exception {
        int databaseSizeBeforeCreate = plantationRepository.findAll().size();
        // Create the Plantation
        restPlantationMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(plantation)))
            .andExpect(status().isCreated());

        // Validate the Plantation in the database
        List<Plantation> plantationList = plantationRepository.findAll();
        assertThat(plantationList).hasSize(databaseSizeBeforeCreate + 1);
        Plantation testPlantation = plantationList.get(plantationList.size() - 1);
        assertThat(testPlantation.getIndexParcelle()).isEqualTo(DEFAULT_INDEX_PARCELLE);
    }

    @Test
    @Transactional
    void createPlantationWithExistingId() throws Exception {
        // Create the Plantation with an existing ID
        plantation.setId(1L);

        int databaseSizeBeforeCreate = plantationRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPlantationMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(plantation)))
            .andExpect(status().isBadRequest());

        // Validate the Plantation in the database
        List<Plantation> plantationList = plantationRepository.findAll();
        assertThat(plantationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllPlantations() throws Exception {
        // Initialize the database
        plantationRepository.saveAndFlush(plantation);

        // Get all the plantationList
        restPlantationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(plantation.getId().intValue())))
            .andExpect(jsonPath("$.[*].indexParcelle").value(hasItem(DEFAULT_INDEX_PARCELLE)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllPlantationsWithEagerRelationshipsIsEnabled() throws Exception {
        when(plantationServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restPlantationMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(plantationServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllPlantationsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(plantationServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restPlantationMockMvc.perform(get(ENTITY_API_URL + "?eagerload=false")).andExpect(status().isOk());
        verify(plantationRepositoryMock, times(1)).findAll(any(Pageable.class));
    }

    @Test
    @Transactional
    void getPlantation() throws Exception {
        // Initialize the database
        plantationRepository.saveAndFlush(plantation);

        // Get the plantation
        restPlantationMockMvc
            .perform(get(ENTITY_API_URL_ID, plantation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(plantation.getId().intValue()))
            .andExpect(jsonPath("$.indexParcelle").value(DEFAULT_INDEX_PARCELLE));
    }

    @Test
    @Transactional
    void getNonExistingPlantation() throws Exception {
        // Get the plantation
        restPlantationMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingPlantation() throws Exception {
        // Initialize the database
        plantationRepository.saveAndFlush(plantation);

        int databaseSizeBeforeUpdate = plantationRepository.findAll().size();

        // Update the plantation
        Plantation updatedPlantation = plantationRepository.findById(plantation.getId()).get();
        // Disconnect from session so that the updates on updatedPlantation are not directly saved in db
        em.detach(updatedPlantation);
        updatedPlantation.indexParcelle(UPDATED_INDEX_PARCELLE);

        restPlantationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedPlantation.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedPlantation))
            )
            .andExpect(status().isOk());

        // Validate the Plantation in the database
        List<Plantation> plantationList = plantationRepository.findAll();
        assertThat(plantationList).hasSize(databaseSizeBeforeUpdate);
        Plantation testPlantation = plantationList.get(plantationList.size() - 1);
        assertThat(testPlantation.getIndexParcelle()).isEqualTo(UPDATED_INDEX_PARCELLE);
    }

    @Test
    @Transactional
    void putNonExistingPlantation() throws Exception {
        int databaseSizeBeforeUpdate = plantationRepository.findAll().size();
        plantation.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPlantationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, plantation.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(plantation))
            )
            .andExpect(status().isBadRequest());

        // Validate the Plantation in the database
        List<Plantation> plantationList = plantationRepository.findAll();
        assertThat(plantationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPlantation() throws Exception {
        int databaseSizeBeforeUpdate = plantationRepository.findAll().size();
        plantation.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPlantationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(plantation))
            )
            .andExpect(status().isBadRequest());

        // Validate the Plantation in the database
        List<Plantation> plantationList = plantationRepository.findAll();
        assertThat(plantationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPlantation() throws Exception {
        int databaseSizeBeforeUpdate = plantationRepository.findAll().size();
        plantation.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPlantationMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(plantation)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Plantation in the database
        List<Plantation> plantationList = plantationRepository.findAll();
        assertThat(plantationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePlantationWithPatch() throws Exception {
        // Initialize the database
        plantationRepository.saveAndFlush(plantation);

        int databaseSizeBeforeUpdate = plantationRepository.findAll().size();

        // Update the plantation using partial update
        Plantation partialUpdatedPlantation = new Plantation();
        partialUpdatedPlantation.setId(plantation.getId());

        partialUpdatedPlantation.indexParcelle(UPDATED_INDEX_PARCELLE);

        restPlantationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPlantation.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPlantation))
            )
            .andExpect(status().isOk());

        // Validate the Plantation in the database
        List<Plantation> plantationList = plantationRepository.findAll();
        assertThat(plantationList).hasSize(databaseSizeBeforeUpdate);
        Plantation testPlantation = plantationList.get(plantationList.size() - 1);
        assertThat(testPlantation.getIndexParcelle()).isEqualTo(UPDATED_INDEX_PARCELLE);
    }

    @Test
    @Transactional
    void fullUpdatePlantationWithPatch() throws Exception {
        // Initialize the database
        plantationRepository.saveAndFlush(plantation);

        int databaseSizeBeforeUpdate = plantationRepository.findAll().size();

        // Update the plantation using partial update
        Plantation partialUpdatedPlantation = new Plantation();
        partialUpdatedPlantation.setId(plantation.getId());

        partialUpdatedPlantation.indexParcelle(UPDATED_INDEX_PARCELLE);

        restPlantationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPlantation.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPlantation))
            )
            .andExpect(status().isOk());

        // Validate the Plantation in the database
        List<Plantation> plantationList = plantationRepository.findAll();
        assertThat(plantationList).hasSize(databaseSizeBeforeUpdate);
        Plantation testPlantation = plantationList.get(plantationList.size() - 1);
        assertThat(testPlantation.getIndexParcelle()).isEqualTo(UPDATED_INDEX_PARCELLE);
    }

    @Test
    @Transactional
    void patchNonExistingPlantation() throws Exception {
        int databaseSizeBeforeUpdate = plantationRepository.findAll().size();
        plantation.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPlantationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, plantation.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(plantation))
            )
            .andExpect(status().isBadRequest());

        // Validate the Plantation in the database
        List<Plantation> plantationList = plantationRepository.findAll();
        assertThat(plantationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPlantation() throws Exception {
        int databaseSizeBeforeUpdate = plantationRepository.findAll().size();
        plantation.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPlantationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(plantation))
            )
            .andExpect(status().isBadRequest());

        // Validate the Plantation in the database
        List<Plantation> plantationList = plantationRepository.findAll();
        assertThat(plantationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPlantation() throws Exception {
        int databaseSizeBeforeUpdate = plantationRepository.findAll().size();
        plantation.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPlantationMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(plantation))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Plantation in the database
        List<Plantation> plantationList = plantationRepository.findAll();
        assertThat(plantationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePlantation() throws Exception {
        // Initialize the database
        plantationRepository.saveAndFlush(plantation);

        int databaseSizeBeforeDelete = plantationRepository.findAll().size();

        // Delete the plantation
        restPlantationMockMvc
            .perform(delete(ENTITY_API_URL_ID, plantation.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Plantation> plantationList = plantationRepository.findAll();
        assertThat(plantationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
