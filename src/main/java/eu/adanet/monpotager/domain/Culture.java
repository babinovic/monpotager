package eu.adanet.monpotager.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;

/**
 * A Culture.
 */
@Entity
@Table(name = "culture")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Culture implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "date_debut")
    private LocalDate dateDebut;

    @Column(name = "date_fin")
    private LocalDate dateFin;

    @Column(name = "date_recolte")
    private LocalDate dateRecolte;

    @ManyToOne
    @JsonIgnoreProperties(value = { "parcelle" }, allowSetters = true)
    private Plantation plantation;

    @ManyToOne
    private Plante plante;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Culture(String nom,  Plantation plantation) {
        this.nom = nom;
        this.dateDebut = null;
        this.dateFin = null;
        this.plantation = plantation;
    }

    public Culture() {
    }

    public Long getId() {
        return this.id;
    }

    public Culture id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public Culture nom(String nom) {
        this.setNom(nom);
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalDate getDateDebut() {
        return this.dateDebut;
    }

    public Culture dateDebut(LocalDate dateDebut) {
        this.setDateDebut(dateDebut);
        return this;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return this.dateFin;
    }

    public Culture dateFin(LocalDate dateFin) {
        this.setDateFin(dateFin);
        return this;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public LocalDate getDateRecolte() {
        return this.dateRecolte;
    }

    public Culture dateRecolte(LocalDate dateRecolte) {
        this.setDateRecolte(dateRecolte);
        return this;
    }

    public void setDateRecolte(LocalDate dateRecolte) {
        this.dateRecolte = dateRecolte;
    }

    public Plantation getPlantation() {
        return this.plantation;
    }

    public void setPlantation(Plantation plantation) {
        this.plantation = plantation;
    }

    public Culture plantation(Plantation plantation) {
        this.setPlantation(plantation);
        return this;
    }

    public Plante getPlante() {
        return this.plante;
    }

    public void setPlante(Plante plante) {
        this.plante = plante;
    }

    public Culture plante(Plante plante) {
        this.setPlante(plante);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Culture)) {
            return false;
        }
        return id != null && id.equals(((Culture) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Culture{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", dateDebut='" + getDateDebut() + "'" +
            ", dateFin='" + getDateFin() + "'" +
            ", dateRecolte='" + getDateRecolte() + "'" +
            "}";
    }
}
