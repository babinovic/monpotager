/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import TacheUpdateComponent from '@/entities/tache/tache-update.vue';
import TacheClass from '@/entities/tache/tache-update.component';
import TacheService from '@/entities/tache/tache.service';

import PotagerService from '@/entities/potager/potager.service';

import ParcelleService from '@/entities/parcelle/parcelle.service';

import ConseilService from '@/entities/conseil/conseil.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Tache Management Update Component', () => {
    let wrapper: Wrapper<TacheClass>;
    let comp: TacheClass;
    let tacheServiceStub: SinonStubbedInstance<TacheService>;

    beforeEach(() => {
      tacheServiceStub = sinon.createStubInstance<TacheService>(TacheService);

      wrapper = shallowMount<TacheClass>(TacheUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          tacheService: () => tacheServiceStub,
          alertService: () => new AlertService(),

          potagerService: () =>
            sinon.createStubInstance<PotagerService>(PotagerService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          parcelleService: () =>
            sinon.createStubInstance<ParcelleService>(ParcelleService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          conseilService: () =>
            sinon.createStubInstance<ConseilService>(ConseilService, {
              retrieve: sinon.stub().resolves({}),
            } as any),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.tache = entity;
        tacheServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(tacheServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.tache = entity;
        tacheServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(tacheServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundTache = { id: 123 };
        tacheServiceStub.find.resolves(foundTache);
        tacheServiceStub.retrieve.resolves([foundTache]);

        // WHEN
        comp.beforeRouteEnter({ params: { tacheId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.tache).toBe(foundTache);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
