import { IParcelle } from '../model/parcelle.model';
import { IPlante } from '../model/plante.model';

export interface IPlantationCulture {
  id?: number;
  dateSemis?: string | null;
  indexParcelle?: number | null;
  parcelle?: IParcelle | null;
  cultureId?: number;
  nomCulture?: string | null;
  dateDebut?: string | null;
  dateFin?: string | null;
  dateRecolte?: string | null;
  plante?: IPlante | null;
}

export class PlantationService implements IPlantationCulture {
  constructor(
    public id?: number,
    public dateSemis?: string | null,
    public datePlantation?: string | null,
    public indexParcelle?: number | null,
    public parcelle?: IParcelle | null,
    public cultureId?: number,
    public nomCulture?: string | null,
    public dateDebut?: string | null,
    public dateFin?: string | null,
    public dateRecolte?: string | null,
    public plante?: IPlante | null
  ) {}
}
