package eu.adanet.monpotager.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import eu.adanet.monpotager.IntegrationTest;
import eu.adanet.monpotager.domain.Culture;
import eu.adanet.monpotager.domain.Plantation;
import eu.adanet.monpotager.domain.Plante;
import eu.adanet.monpotager.repository.CultureRepository;
import eu.adanet.monpotager.service.CultureService;
import eu.adanet.monpotager.service.criteria.CultureCriteria;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CultureResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class CultureResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_DEBUT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_DEBUT = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATE_DEBUT = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATE_FIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_FIN = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATE_FIN = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DATE_RECOLTE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_RECOLTE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATE_RECOLTE = LocalDate.ofEpochDay(-1L);

    private static final String ENTITY_API_URL = "/api/cultures";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CultureRepository cultureRepository;

    @Mock
    private CultureRepository cultureRepositoryMock;

    @Mock
    private CultureService cultureServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCultureMockMvc;

    private Culture culture;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Culture createEntity(EntityManager em) {
        Culture culture = new Culture()
            .nom(DEFAULT_NOM)
            .dateDebut(DEFAULT_DATE_DEBUT)
            .dateFin(DEFAULT_DATE_FIN)
            .dateRecolte(DEFAULT_DATE_RECOLTE);
        return culture;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Culture createUpdatedEntity(EntityManager em) {
        Culture culture = new Culture()
            .nom(UPDATED_NOM)
            .dateDebut(UPDATED_DATE_DEBUT)
            .dateFin(UPDATED_DATE_FIN)
            .dateRecolte(UPDATED_DATE_RECOLTE);
        return culture;
    }

    @BeforeEach
    public void initTest() {
        culture = createEntity(em);
    }

    @Test
    @Transactional
    void createCulture() throws Exception {
        int databaseSizeBeforeCreate = cultureRepository.findAll().size();
        // Create the Culture
        restCultureMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(culture)))
            .andExpect(status().isCreated());

        // Validate the Culture in the database
        List<Culture> cultureList = cultureRepository.findAll();
        assertThat(cultureList).hasSize(databaseSizeBeforeCreate + 1);
        Culture testCulture = cultureList.get(cultureList.size() - 1);
        assertThat(testCulture.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testCulture.getDateDebut()).isEqualTo(DEFAULT_DATE_DEBUT);
        assertThat(testCulture.getDateFin()).isEqualTo(DEFAULT_DATE_FIN);
        assertThat(testCulture.getDateRecolte()).isEqualTo(DEFAULT_DATE_RECOLTE);
    }

    @Test
    @Transactional
    void createCultureWithExistingId() throws Exception {
        // Create the Culture with an existing ID
        culture.setId(1L);

        int databaseSizeBeforeCreate = cultureRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCultureMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(culture)))
            .andExpect(status().isBadRequest());

        // Validate the Culture in the database
        List<Culture> cultureList = cultureRepository.findAll();
        assertThat(cultureList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCultures() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList
        restCultureMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(culture.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].dateDebut").value(hasItem(DEFAULT_DATE_DEBUT.toString())))
            .andExpect(jsonPath("$.[*].dateFin").value(hasItem(DEFAULT_DATE_FIN.toString())))
            .andExpect(jsonPath("$.[*].dateRecolte").value(hasItem(DEFAULT_DATE_RECOLTE.toString())));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllCulturesWithEagerRelationshipsIsEnabled() throws Exception {
        when(cultureServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restCultureMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(cultureServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllCulturesWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(cultureServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restCultureMockMvc.perform(get(ENTITY_API_URL + "?eagerload=false")).andExpect(status().isOk());
        verify(cultureRepositoryMock, times(1)).findAll(any(Pageable.class));
    }

    @Test
    @Transactional
    void getCulture() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get the culture
        restCultureMockMvc
            .perform(get(ENTITY_API_URL_ID, culture.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(culture.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.dateDebut").value(DEFAULT_DATE_DEBUT.toString()))
            .andExpect(jsonPath("$.dateFin").value(DEFAULT_DATE_FIN.toString()))
            .andExpect(jsonPath("$.dateRecolte").value(DEFAULT_DATE_RECOLTE.toString()));
    }

    @Test
    @Transactional
    void getCulturesByIdFiltering() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        Long id = culture.getId();

        defaultCultureShouldBeFound("id.equals=" + id);
        defaultCultureShouldNotBeFound("id.notEquals=" + id);

        defaultCultureShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCultureShouldNotBeFound("id.greaterThan=" + id);

        defaultCultureShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCultureShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllCulturesByNomIsEqualToSomething() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where nom equals to DEFAULT_NOM
        defaultCultureShouldBeFound("nom.equals=" + DEFAULT_NOM);

        // Get all the cultureList where nom equals to UPDATED_NOM
        defaultCultureShouldNotBeFound("nom.equals=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    void getAllCulturesByNomIsInShouldWork() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where nom in DEFAULT_NOM or UPDATED_NOM
        defaultCultureShouldBeFound("nom.in=" + DEFAULT_NOM + "," + UPDATED_NOM);

        // Get all the cultureList where nom equals to UPDATED_NOM
        defaultCultureShouldNotBeFound("nom.in=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    void getAllCulturesByNomIsNullOrNotNull() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where nom is not null
        defaultCultureShouldBeFound("nom.specified=true");

        // Get all the cultureList where nom is null
        defaultCultureShouldNotBeFound("nom.specified=false");
    }

    @Test
    @Transactional
    void getAllCulturesByNomContainsSomething() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where nom contains DEFAULT_NOM
        defaultCultureShouldBeFound("nom.contains=" + DEFAULT_NOM);

        // Get all the cultureList where nom contains UPDATED_NOM
        defaultCultureShouldNotBeFound("nom.contains=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    void getAllCulturesByNomNotContainsSomething() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where nom does not contain DEFAULT_NOM
        defaultCultureShouldNotBeFound("nom.doesNotContain=" + DEFAULT_NOM);

        // Get all the cultureList where nom does not contain UPDATED_NOM
        defaultCultureShouldBeFound("nom.doesNotContain=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    void getAllCulturesByDateDebutIsEqualToSomething() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where dateDebut equals to DEFAULT_DATE_DEBUT
        defaultCultureShouldBeFound("dateDebut.equals=" + DEFAULT_DATE_DEBUT);

        // Get all the cultureList where dateDebut equals to UPDATED_DATE_DEBUT
        defaultCultureShouldNotBeFound("dateDebut.equals=" + UPDATED_DATE_DEBUT);
    }

    @Test
    @Transactional
    void getAllCulturesByDateDebutIsInShouldWork() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where dateDebut in DEFAULT_DATE_DEBUT or UPDATED_DATE_DEBUT
        defaultCultureShouldBeFound("dateDebut.in=" + DEFAULT_DATE_DEBUT + "," + UPDATED_DATE_DEBUT);

        // Get all the cultureList where dateDebut equals to UPDATED_DATE_DEBUT
        defaultCultureShouldNotBeFound("dateDebut.in=" + UPDATED_DATE_DEBUT);
    }

    @Test
    @Transactional
    void getAllCulturesByDateDebutIsNullOrNotNull() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where dateDebut is not null
        defaultCultureShouldBeFound("dateDebut.specified=true");

        // Get all the cultureList where dateDebut is null
        defaultCultureShouldNotBeFound("dateDebut.specified=false");
    }

    @Test
    @Transactional
    void getAllCulturesByDateDebutIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where dateDebut is greater than or equal to DEFAULT_DATE_DEBUT
        defaultCultureShouldBeFound("dateDebut.greaterThanOrEqual=" + DEFAULT_DATE_DEBUT);

        // Get all the cultureList where dateDebut is greater than or equal to UPDATED_DATE_DEBUT
        defaultCultureShouldNotBeFound("dateDebut.greaterThanOrEqual=" + UPDATED_DATE_DEBUT);
    }

    @Test
    @Transactional
    void getAllCulturesByDateDebutIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where dateDebut is less than or equal to DEFAULT_DATE_DEBUT
        defaultCultureShouldBeFound("dateDebut.lessThanOrEqual=" + DEFAULT_DATE_DEBUT);

        // Get all the cultureList where dateDebut is less than or equal to SMALLER_DATE_DEBUT
        defaultCultureShouldNotBeFound("dateDebut.lessThanOrEqual=" + SMALLER_DATE_DEBUT);
    }

    @Test
    @Transactional
    void getAllCulturesByDateDebutIsLessThanSomething() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where dateDebut is less than DEFAULT_DATE_DEBUT
        defaultCultureShouldNotBeFound("dateDebut.lessThan=" + DEFAULT_DATE_DEBUT);

        // Get all the cultureList where dateDebut is less than UPDATED_DATE_DEBUT
        defaultCultureShouldBeFound("dateDebut.lessThan=" + UPDATED_DATE_DEBUT);
    }

    @Test
    @Transactional
    void getAllCulturesByDateDebutIsGreaterThanSomething() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where dateDebut is greater than DEFAULT_DATE_DEBUT
        defaultCultureShouldNotBeFound("dateDebut.greaterThan=" + DEFAULT_DATE_DEBUT);

        // Get all the cultureList where dateDebut is greater than SMALLER_DATE_DEBUT
        defaultCultureShouldBeFound("dateDebut.greaterThan=" + SMALLER_DATE_DEBUT);
    }

    @Test
    @Transactional
    void getAllCulturesByDateFinIsEqualToSomething() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where dateFin equals to DEFAULT_DATE_FIN
        defaultCultureShouldBeFound("dateFin.equals=" + DEFAULT_DATE_FIN);

        // Get all the cultureList where dateFin equals to UPDATED_DATE_FIN
        defaultCultureShouldNotBeFound("dateFin.equals=" + UPDATED_DATE_FIN);
    }

    @Test
    @Transactional
    void getAllCulturesByDateFinIsInShouldWork() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where dateFin in DEFAULT_DATE_FIN or UPDATED_DATE_FIN
        defaultCultureShouldBeFound("dateFin.in=" + DEFAULT_DATE_FIN + "," + UPDATED_DATE_FIN);

        // Get all the cultureList where dateFin equals to UPDATED_DATE_FIN
        defaultCultureShouldNotBeFound("dateFin.in=" + UPDATED_DATE_FIN);
    }

    @Test
    @Transactional
    void getAllCulturesByDateFinIsNullOrNotNull() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where dateFin is not null
        defaultCultureShouldBeFound("dateFin.specified=true");

        // Get all the cultureList where dateFin is null
        defaultCultureShouldNotBeFound("dateFin.specified=false");
    }

    @Test
    @Transactional
    void getAllCulturesByDateFinIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where dateFin is greater than or equal to DEFAULT_DATE_FIN
        defaultCultureShouldBeFound("dateFin.greaterThanOrEqual=" + DEFAULT_DATE_FIN);

        // Get all the cultureList where dateFin is greater than or equal to UPDATED_DATE_FIN
        defaultCultureShouldNotBeFound("dateFin.greaterThanOrEqual=" + UPDATED_DATE_FIN);
    }

    @Test
    @Transactional
    void getAllCulturesByDateFinIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where dateFin is less than or equal to DEFAULT_DATE_FIN
        defaultCultureShouldBeFound("dateFin.lessThanOrEqual=" + DEFAULT_DATE_FIN);

        // Get all the cultureList where dateFin is less than or equal to SMALLER_DATE_FIN
        defaultCultureShouldNotBeFound("dateFin.lessThanOrEqual=" + SMALLER_DATE_FIN);
    }

    @Test
    @Transactional
    void getAllCulturesByDateFinIsLessThanSomething() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where dateFin is less than DEFAULT_DATE_FIN
        defaultCultureShouldNotBeFound("dateFin.lessThan=" + DEFAULT_DATE_FIN);

        // Get all the cultureList where dateFin is less than UPDATED_DATE_FIN
        defaultCultureShouldBeFound("dateFin.lessThan=" + UPDATED_DATE_FIN);
    }

    @Test
    @Transactional
    void getAllCulturesByDateFinIsGreaterThanSomething() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where dateFin is greater than DEFAULT_DATE_FIN
        defaultCultureShouldNotBeFound("dateFin.greaterThan=" + DEFAULT_DATE_FIN);

        // Get all the cultureList where dateFin is greater than SMALLER_DATE_FIN
        defaultCultureShouldBeFound("dateFin.greaterThan=" + SMALLER_DATE_FIN);
    }

    @Test
    @Transactional
    void getAllCulturesByDateRecolteIsEqualToSomething() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where dateRecolte equals to DEFAULT_DATE_RECOLTE
        defaultCultureShouldBeFound("dateRecolte.equals=" + DEFAULT_DATE_RECOLTE);

        // Get all the cultureList where dateRecolte equals to UPDATED_DATE_RECOLTE
        defaultCultureShouldNotBeFound("dateRecolte.equals=" + UPDATED_DATE_RECOLTE);
    }

    @Test
    @Transactional
    void getAllCulturesByDateRecolteIsInShouldWork() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where dateRecolte in DEFAULT_DATE_RECOLTE or UPDATED_DATE_RECOLTE
        defaultCultureShouldBeFound("dateRecolte.in=" + DEFAULT_DATE_RECOLTE + "," + UPDATED_DATE_RECOLTE);

        // Get all the cultureList where dateRecolte equals to UPDATED_DATE_RECOLTE
        defaultCultureShouldNotBeFound("dateRecolte.in=" + UPDATED_DATE_RECOLTE);
    }

    @Test
    @Transactional
    void getAllCulturesByDateRecolteIsNullOrNotNull() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where dateRecolte is not null
        defaultCultureShouldBeFound("dateRecolte.specified=true");

        // Get all the cultureList where dateRecolte is null
        defaultCultureShouldNotBeFound("dateRecolte.specified=false");
    }

    @Test
    @Transactional
    void getAllCulturesByDateRecolteIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where dateRecolte is greater than or equal to DEFAULT_DATE_RECOLTE
        defaultCultureShouldBeFound("dateRecolte.greaterThanOrEqual=" + DEFAULT_DATE_RECOLTE);

        // Get all the cultureList where dateRecolte is greater than or equal to UPDATED_DATE_RECOLTE
        defaultCultureShouldNotBeFound("dateRecolte.greaterThanOrEqual=" + UPDATED_DATE_RECOLTE);
    }

    @Test
    @Transactional
    void getAllCulturesByDateRecolteIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where dateRecolte is less than or equal to DEFAULT_DATE_RECOLTE
        defaultCultureShouldBeFound("dateRecolte.lessThanOrEqual=" + DEFAULT_DATE_RECOLTE);

        // Get all the cultureList where dateRecolte is less than or equal to SMALLER_DATE_RECOLTE
        defaultCultureShouldNotBeFound("dateRecolte.lessThanOrEqual=" + SMALLER_DATE_RECOLTE);
    }

    @Test
    @Transactional
    void getAllCulturesByDateRecolteIsLessThanSomething() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where dateRecolte is less than DEFAULT_DATE_RECOLTE
        defaultCultureShouldNotBeFound("dateRecolte.lessThan=" + DEFAULT_DATE_RECOLTE);

        // Get all the cultureList where dateRecolte is less than UPDATED_DATE_RECOLTE
        defaultCultureShouldBeFound("dateRecolte.lessThan=" + UPDATED_DATE_RECOLTE);
    }

    @Test
    @Transactional
    void getAllCulturesByDateRecolteIsGreaterThanSomething() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        // Get all the cultureList where dateRecolte is greater than DEFAULT_DATE_RECOLTE
        defaultCultureShouldNotBeFound("dateRecolte.greaterThan=" + DEFAULT_DATE_RECOLTE);

        // Get all the cultureList where dateRecolte is greater than SMALLER_DATE_RECOLTE
        defaultCultureShouldBeFound("dateRecolte.greaterThan=" + SMALLER_DATE_RECOLTE);
    }

    @Test
    @Transactional
    void getAllCulturesByPlantationIsEqualToSomething() throws Exception {
        Plantation plantation;
        if (TestUtil.findAll(em, Plantation.class).isEmpty()) {
            cultureRepository.saveAndFlush(culture);
            plantation = PlantationResourceIT.createEntity(em);
        } else {
            plantation = TestUtil.findAll(em, Plantation.class).get(0);
        }
        em.persist(plantation);
        em.flush();
        culture.setPlantation(plantation);
        cultureRepository.saveAndFlush(culture);
        Long plantationId = plantation.getId();

        // Get all the cultureList where plantation equals to plantationId
        defaultCultureShouldBeFound("plantationId.equals=" + plantationId);

        // Get all the cultureList where plantation equals to (plantationId + 1)
        defaultCultureShouldNotBeFound("plantationId.equals=" + (plantationId + 1));
    }

    @Test
    @Transactional
    void getAllCulturesByPlanteIsEqualToSomething() throws Exception {
        Plante plante;
        if (TestUtil.findAll(em, Plante.class).isEmpty()) {
            cultureRepository.saveAndFlush(culture);
            plante = PlanteResourceIT.createEntity(em);
        } else {
            plante = TestUtil.findAll(em, Plante.class).get(0);
        }
        em.persist(plante);
        em.flush();
        culture.setPlante(plante);
        cultureRepository.saveAndFlush(culture);
        Long planteId = plante.getId();

        // Get all the cultureList where plante equals to planteId
        defaultCultureShouldBeFound("planteId.equals=" + planteId);

        // Get all the cultureList where plante equals to (planteId + 1)
        defaultCultureShouldNotBeFound("planteId.equals=" + (planteId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCultureShouldBeFound(String filter) throws Exception {
        restCultureMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(culture.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].dateDebut").value(hasItem(DEFAULT_DATE_DEBUT.toString())))
            .andExpect(jsonPath("$.[*].dateFin").value(hasItem(DEFAULT_DATE_FIN.toString())))
            .andExpect(jsonPath("$.[*].dateRecolte").value(hasItem(DEFAULT_DATE_RECOLTE.toString())));

        // Check, that the count call also returns 1
        restCultureMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCultureShouldNotBeFound(String filter) throws Exception {
        restCultureMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCultureMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingCulture() throws Exception {
        // Get the culture
        restCultureMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingCulture() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        int databaseSizeBeforeUpdate = cultureRepository.findAll().size();

        // Update the culture
        Culture updatedCulture = cultureRepository.findById(culture.getId()).get();
        // Disconnect from session so that the updates on updatedCulture are not directly saved in db
        em.detach(updatedCulture);
        updatedCulture.nom(UPDATED_NOM).dateDebut(UPDATED_DATE_DEBUT).dateFin(UPDATED_DATE_FIN).dateRecolte(UPDATED_DATE_RECOLTE);

        restCultureMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedCulture.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedCulture))
            )
            .andExpect(status().isOk());

        // Validate the Culture in the database
        List<Culture> cultureList = cultureRepository.findAll();
        assertThat(cultureList).hasSize(databaseSizeBeforeUpdate);
        Culture testCulture = cultureList.get(cultureList.size() - 1);
        assertThat(testCulture.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testCulture.getDateDebut()).isEqualTo(UPDATED_DATE_DEBUT);
        assertThat(testCulture.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testCulture.getDateRecolte()).isEqualTo(UPDATED_DATE_RECOLTE);
    }

    @Test
    @Transactional
    void putNonExistingCulture() throws Exception {
        int databaseSizeBeforeUpdate = cultureRepository.findAll().size();
        culture.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCultureMockMvc
            .perform(
                put(ENTITY_API_URL_ID, culture.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(culture))
            )
            .andExpect(status().isBadRequest());

        // Validate the Culture in the database
        List<Culture> cultureList = cultureRepository.findAll();
        assertThat(cultureList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCulture() throws Exception {
        int databaseSizeBeforeUpdate = cultureRepository.findAll().size();
        culture.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCultureMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(culture))
            )
            .andExpect(status().isBadRequest());

        // Validate the Culture in the database
        List<Culture> cultureList = cultureRepository.findAll();
        assertThat(cultureList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCulture() throws Exception {
        int databaseSizeBeforeUpdate = cultureRepository.findAll().size();
        culture.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCultureMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(culture)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Culture in the database
        List<Culture> cultureList = cultureRepository.findAll();
        assertThat(cultureList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCultureWithPatch() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        int databaseSizeBeforeUpdate = cultureRepository.findAll().size();

        // Update the culture using partial update
        Culture partialUpdatedCulture = new Culture();
        partialUpdatedCulture.setId(culture.getId());

        partialUpdatedCulture.nom(UPDATED_NOM).dateDebut(UPDATED_DATE_DEBUT).dateFin(UPDATED_DATE_FIN).dateRecolte(UPDATED_DATE_RECOLTE);

        restCultureMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCulture.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCulture))
            )
            .andExpect(status().isOk());

        // Validate the Culture in the database
        List<Culture> cultureList = cultureRepository.findAll();
        assertThat(cultureList).hasSize(databaseSizeBeforeUpdate);
        Culture testCulture = cultureList.get(cultureList.size() - 1);
        assertThat(testCulture.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testCulture.getDateDebut()).isEqualTo(UPDATED_DATE_DEBUT);
        assertThat(testCulture.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testCulture.getDateRecolte()).isEqualTo(UPDATED_DATE_RECOLTE);
    }

    @Test
    @Transactional
    void fullUpdateCultureWithPatch() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        int databaseSizeBeforeUpdate = cultureRepository.findAll().size();

        // Update the culture using partial update
        Culture partialUpdatedCulture = new Culture();
        partialUpdatedCulture.setId(culture.getId());

        partialUpdatedCulture.nom(UPDATED_NOM).dateDebut(UPDATED_DATE_DEBUT).dateFin(UPDATED_DATE_FIN).dateRecolte(UPDATED_DATE_RECOLTE);

        restCultureMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCulture.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCulture))
            )
            .andExpect(status().isOk());

        // Validate the Culture in the database
        List<Culture> cultureList = cultureRepository.findAll();
        assertThat(cultureList).hasSize(databaseSizeBeforeUpdate);
        Culture testCulture = cultureList.get(cultureList.size() - 1);
        assertThat(testCulture.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testCulture.getDateDebut()).isEqualTo(UPDATED_DATE_DEBUT);
        assertThat(testCulture.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testCulture.getDateRecolte()).isEqualTo(UPDATED_DATE_RECOLTE);
    }

    @Test
    @Transactional
    void patchNonExistingCulture() throws Exception {
        int databaseSizeBeforeUpdate = cultureRepository.findAll().size();
        culture.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCultureMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, culture.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(culture))
            )
            .andExpect(status().isBadRequest());

        // Validate the Culture in the database
        List<Culture> cultureList = cultureRepository.findAll();
        assertThat(cultureList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCulture() throws Exception {
        int databaseSizeBeforeUpdate = cultureRepository.findAll().size();
        culture.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCultureMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(culture))
            )
            .andExpect(status().isBadRequest());

        // Validate the Culture in the database
        List<Culture> cultureList = cultureRepository.findAll();
        assertThat(cultureList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCulture() throws Exception {
        int databaseSizeBeforeUpdate = cultureRepository.findAll().size();
        culture.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCultureMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(culture)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Culture in the database
        List<Culture> cultureList = cultureRepository.findAll();
        assertThat(cultureList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCulture() throws Exception {
        // Initialize the database
        cultureRepository.saveAndFlush(culture);

        int databaseSizeBeforeDelete = cultureRepository.findAll().size();

        // Delete the culture
        restCultureMockMvc
            .perform(delete(ENTITY_API_URL_ID, culture.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Culture> cultureList = cultureRepository.findAll();
        assertThat(cultureList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
