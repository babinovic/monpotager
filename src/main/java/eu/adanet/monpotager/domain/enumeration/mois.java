package eu.adanet.monpotager.domain.enumeration;

/**
 * The Mois enumeration.
 */
public enum Mois {
    JANVIER,
    FEVRIER,
    MARS,
    AVRIL,
    MAI,
    JUIN,
    JUILLET,
    AOUT,
    SEPTEMBRE,
    OCTOBRE,
    NOVEMBRE,
    DECEMBRE,
}
