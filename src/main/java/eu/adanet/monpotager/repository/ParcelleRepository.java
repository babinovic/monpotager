package eu.adanet.monpotager.repository;

import eu.adanet.monpotager.domain.Parcelle;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Parcelle entity.
 */
@Repository
public interface ParcelleRepository extends JpaRepository<Parcelle, Long> {
    default Optional<Parcelle> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<Parcelle> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<Parcelle> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct parcelle from Parcelle parcelle left join fetch parcelle.potager",
        countQuery = "select count(distinct parcelle) from Parcelle parcelle"
    )
    Page<Parcelle> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct parcelle from Parcelle parcelle left join fetch parcelle.potager")
    List<Parcelle> findAllWithToOneRelationships();

    @Query("select parcelle from Parcelle parcelle left join fetch parcelle.potager where parcelle.id =:id")
    Optional<Parcelle> findOneWithToOneRelationships(@Param("id") Long id);
}
