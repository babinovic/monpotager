import { typePlante } from '@/shared/model/enumerations/type-plante.model';
import { exposition } from '@/shared/model/enumerations/exposition.model';
import { frequenceArrosage } from '@/shared/model/enumerations/frequence-arrosage.model';
import { mois } from '@/shared/model/enumerations/mois.model';
import { categoriePlante } from '@/shared/model/enumerations/categorie-plante.model';
export interface IPlante {
  id?: number;
  nom?: string | null;
  variete?: string | null;
  type?: typePlante | null;
  espacement?: number | null;
  espacementRang?: number | null;
  ensoleillement?: exposition | null;
  arrosage?: frequenceArrosage | null;
  debutPlantation?: mois | null;
  finPlantation?: mois | null;
  debutRecolte?: mois | null;
  finRecolte?: mois | null;
  debutSemis?: mois | null;
  finSemis?: mois | null;
  categorie?: categoriePlante | null;
  image?: string | null;
}

export class Plante implements IPlante {
  constructor(
    public id?: number,
    public nom?: string | null,
    public variete?: string | null,
    public type?: typePlante | null,
    public espacement?: number | null,
    public espacementRang?: number | null,
    public ensoleillement?: exposition | null,
    public arrosage?: frequenceArrosage | null,
    public debutPlantation?: mois | null,
    public finPlantation?: mois | null,
    public debutRecolte?: mois | null,
    public finRecolte?: mois | null,
    public debutSemis?: mois | null,
    public finSemis?: mois | null,
    public categorie?: categoriePlante | null,
    public image?: string | null
  ) {}
}
