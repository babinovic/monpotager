/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import ConseilDetailComponent from '@/entities/conseil/conseil-details.vue';
import ConseilClass from '@/entities/conseil/conseil-details.component';
import ConseilService from '@/entities/conseil/conseil.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Conseil Management Detail Component', () => {
    let wrapper: Wrapper<ConseilClass>;
    let comp: ConseilClass;
    let conseilServiceStub: SinonStubbedInstance<ConseilService>;

    beforeEach(() => {
      conseilServiceStub = sinon.createStubInstance<ConseilService>(ConseilService);

      wrapper = shallowMount<ConseilClass>(ConseilDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { conseilService: () => conseilServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundConseil = { id: 123 };
        conseilServiceStub.find.resolves(foundConseil);

        // WHEN
        comp.retrieveConseil(123);
        await comp.$nextTick();

        // THEN
        expect(comp.conseil).toBe(foundConseil);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundConseil = { id: 123 };
        conseilServiceStub.find.resolves(foundConseil);

        // WHEN
        comp.beforeRouteEnter({ params: { conseilId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.conseil).toBe(foundConseil);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
