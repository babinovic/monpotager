import { Component, Vue, Inject } from 'vue-property-decorator';

import { ICulture } from '@/shared/model/culture.model';
import CultureService from './culture.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class CultureDetails extends Vue {
  @Inject('cultureService') private cultureService: () => CultureService;
  @Inject('alertService') private alertService: () => AlertService;

  public culture: ICulture = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.cultureId) {
        vm.retrieveCulture(to.params.cultureId);
      }
    });
  }

  public retrieveCulture(cultureId) {
    this.cultureService()
      .find(cultureId)
      .then(res => {
        this.culture = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
