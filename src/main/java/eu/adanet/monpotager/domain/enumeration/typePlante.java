package eu.adanet.monpotager.domain.enumeration;

/**
 * The typePlante enumeration.
 */
public enum typePlante {
    LEGUME,
    FRUIT,
    AROMATIQUE,
    FLEURS,
    ARBUSTRE,
}
