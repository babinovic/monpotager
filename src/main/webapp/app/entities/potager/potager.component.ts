import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IPotager } from '@/shared/model/potager.model';

import PotagerService from './potager.service';
import AlertService from '@/shared/alert/alert.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class Potager extends Vue {
  @Inject('potagerService') private potagerService: () => PotagerService;
  @Inject('alertService') private alertService: () => AlertService;

  private removeId: number = null;

  public potagers: IPotager[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllPotagers();
  }

  public clear(): void {
    this.retrieveAllPotagers();
  }

  public retrieveAllPotagers(): void {
    this.isFetching = true;
    this.potagerService()
      .retrieve()
      .then(
        res => {
          this.potagers = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
          this.alertService().showHttpError(this, err.response);
        }
      );
  }

  public handleSyncList(): void {
    this.clear();
  }

  public prepareRemove(instance: IPotager): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removePotager(): void {
    this.potagerService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('monpotagerApp.potager.deleted', { param: this.removeId });
        this.$bvToast.toast(message.toString(), {
          toaster: 'b-toaster-top-center',
          title: 'Info',
          variant: 'danger',
          solid: true,
          autoHideDelay: 5000,
        });
        this.removeId = null;
        this.retrieveAllPotagers();
        this.closeDialog();
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
