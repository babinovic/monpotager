import axios from 'axios';

import buildPaginationQueryOpts from '@/shared/sort/sorts';

import { IPlantation } from '@/shared/model/plantation.model';

const baseApiUrl = 'api/plantations';
const parcelleApiUrl = 'api/plantationsParcelle'

export default class PlantationService {
  public find(id: number): Promise<IPlantation> {
    return new Promise<IPlantation>((resolve, reject) => {
      axios
        .get(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public retrieve(paginationQuery?: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(baseApiUrl + `?${buildPaginationQueryOpts(paginationQuery)}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public retrieveParcelle(parcelleId: any, dateVisualisation: number): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(parcelleApiUrl, {params: {parcelleId: parcelleId, dateVisualisation: dateVisualisation}})
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  parcelleApiUrl

  public delete(id: number): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .delete(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public create(entity: IPlantation): Promise<IPlantation> {
    return new Promise<IPlantation>((resolve, reject) => {
      axios
        .post(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public update(entity: IPlantation): Promise<IPlantation> {
    return new Promise<IPlantation>((resolve, reject) => {
      axios
        .put(`${baseApiUrl}/${entity.id}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public partialUpdate(entity: IPlantation): Promise<IPlantation> {
    return new Promise<IPlantation>((resolve, reject) => {
      axios
        .patch(`${baseApiUrl}/${entity.id}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}
