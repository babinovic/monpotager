import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import { IPotager, Potager } from '@/shared/model/potager.model';
import PotagerService from './potager.service';

const validations: any = {
  potager: {
    nom: {},
    longitude: {},
    latitude: {},
  },
};

@Component({
  validations,
})
export default class PotagerUpdate extends Vue {
  @Inject('potagerService') private potagerService: () => PotagerService;
  @Inject('alertService') private alertService: () => AlertService;

  public potager: IPotager = new Potager();
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.potagerId) {
        vm.retrievePotager(to.params.potagerId);
      }
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.potager.id) {
      this.potagerService()
        .update(this.potager)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('monpotagerApp.potager.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.potagerService()
        .create(this.potager)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('monpotagerApp.potager.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrievePotager(potagerId): void {
    this.potagerService()
      .find(potagerId)
      .then(res => {
        this.potager = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
