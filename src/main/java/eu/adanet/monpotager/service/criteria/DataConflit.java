package eu.adanet.monpotager.service.criteria;

import java.io.Serializable;
import java.time.LocalDate;

public class DataConflit implements Serializable {
    private String nom;

    private LocalDate dateDebut;

    private LocalDate dateFin;


    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalDate getDateDebut() {
        return this.dateDebut;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return this.dateFin;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }    
}
